from otree.api import *

from settings import small_win

from functions import make_field

import random

doc = """
Your app description
"""

def creating_session(subsession):
    """Allocating each player in one of the four treatment.

    ability treatment: if True, RPS as an ability game is emphasized.
    knowledge treatment: if True, the probability in the RPS is shown when choosing the allocation mechanism.
    """
    for p in subsession.get_players():
        p.urn_rev = bool(random.randint(0, 1))
        p.gender_rev = bool(random.randint(0, 1))
        p.colour_rev = bool(random.randint(0, 1))
        
        
class Constants(BaseConstants):
    name_in_url = 'Questionnaire'
    players_per_group = None
    num_rounds = 1
    
    urns = ['Urn Left', 'Urn Right']
    balls = ['Red', 'Black']
    gender = ['Male', 'Female', 'Other', 'Prefer not to say']
    age = ["<25", "25-40", "40-55", ">55", 'Prefer not to say']
    occupation = ["Employed", "Self-employed", "Student", "Retired", "Other"]
    small_win = cu(small_win)
    loss = cu(0)

class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    # Demographics variables
    age = make_field('How old are you?', Constants.age)
    gender_rev = models.BooleanField(initial=True)
    gender = make_field('What is your gender?', Constants.gender)
    employment = make_field("What is your occupation?", Constants.occupation)
    region = models.StringField(label="What is your country of residence?")

    # Ambiguity elicitation variables
    urn_rev = models.BooleanField(initial=True)
    urn = make_field('What urn do you choose?', Constants.urns)
    colour_rev = models.BooleanField(initial=True)
    colour = make_field('What ball colour do you choose?', Constants.balls)
    colour_drawn = models.StringField(initial="Black")
    urn_winner = models.BooleanField(initial=False)
    reasons = models.LongStringField()


# PAGES
class Demographics(Page):
    """Ask for some demographic information about the subject."""
    form_model = 'player'
    form_fields = ['age', 'gender', 'employment', 'region']


class Decisions(Page):
    """The page where player provide their belief about the RPS game and have the possibility to provide reasons for
    their choices. """
    form_model = 'player'
    form_fields = ['reasons']

    def vars_for_template(player):
        """Adapt the question to the mechanism chosen."""
        return dict(
            reasons_label=f"Why did you choose the {player.participant.mechanism} to get your reward?"
        )

    
class Ambiguity(Page):
    """Elicit whether a player is ambiguity averse or not, using Ellsberg's urns."""
    form_model = 'player'
    form_fields = ['urn', 'colour']

    def before_next_page(player, timeout_happened):
        """Compute whether a player won or not.
        
        Winning depends on the urn chosen.
        If it is the first urn, the composition is 50 red balls and 50 black balls.
        If it is the second urn, first we determine the composition and then we draw one ball from the urn."""
        if player.urn == Constants.urns[0]:
            player.colour_drawn = random.choice(Constants.balls)
        else:
            n_red = random.randint(0, 100)
            if random.randint(0, 100) <= n_red:
                player.colour_drawn = Constants.balls[0]
            else:
                player.colour_drawn = Constants.balls[1]
        if player.colour == player.colour_drawn:
            player.payoff += Constants.small_win
            player.urn_winner = True


page_sequence = [Decisions, Ambiguity, Demographics]
