from otree.api import *

from settings import reward, lottery


doc = """
The app where subjects choose a mechanism.
"""

def mechanism_choices(player):
    return [[True, player.participant.criteria], [False, lottery]]

    
class Constants(BaseConstants):
    name_in_url = 'Mechanism'
    players_per_group = None
    num_rounds = 1
    
    reward = cu(reward)


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    mechanism = models.BooleanField(label="Which mechanism do you vote for?")
    



# PAGES
class Mechanism(Page):
    form_model = 'player'
    form_fields = ['mechanism']

    
    @staticmethod
    def before_next_page(player, timeout_happened):
        player.participant.mechanism = player.get_field_display('mechanism')




page_sequence = [Mechanism]
