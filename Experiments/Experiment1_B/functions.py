from otree.api import *


def translate(game, dictionary):
    return [dictionary[s] for s in game]


def translate_rps(rps):
    """Transforms a profile of RPS from strings to integers."""
    translation = {'Rock': 1, 'Paper': 2, 'Scissors': 3}
    return translate(rps, translation)


def translate_lottery(lottery):
    """Transforms a profile of Odd Evens from strings to integers."""
    translation = {'Odd': 1, 'Even': 0}
    return translate(lottery, translation)

def make_field(question, choice, rev=False):
    """Create a question using a StringField and RadioSelect buttons.

    Allow for reversing the first two answers."""
    if rev:
        choice2 = choice.copy()
        choice2[0], choice2[1] = choice2[1], choice2[0]
        return models.StringField(choices=choice2, label=question, widget=widgets.RadioSelect)
    else:
        return models.StringField(choices=choice, label=question, widget=widgets.RadioSelect)