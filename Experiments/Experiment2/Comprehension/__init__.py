from otree.api import *

from settings import INCENTIVE, GROUPS, X_PARTICIPANTS, EFFICIENT, MARKET, BARGAINING, TIMER_TEXT
from datetime import datetime

doc = """
Beliefs and computation understanding.
Risk aversion elicitation.
"""


def get_timeout_seconds(player):
    session = player.session
    return (session.expiry - datetime.now()).total_seconds()


class C(BaseConstants):
    NAME_IN_URL = 'Comprendre_vos_choix'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 2

    INCENTIVE = cu(INCENTIVE)
    HALF = int(X_PARTICIPANTS / 2)
    X_PARTICIPANTS = X_PARTICIPANTS


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    efficient = models.BooleanField(
        label="Selon vous, quelle est la procédure la plus <b>efficace</b> pour grouper les participants ?",
        choices=[[False, 'PROCÉDURE 1'], [True, 'PROCÉDURE 2']],
    )
    fair = models.BooleanField(
        label="Selon vous, quelle est la procédure la plus <b>juste</b> pour grouper les participants ?",
        choices=[[False, 'PROCÉDURE 1'], [True, 'PROCÉDURE 2']],
    )
    color_group = models.BooleanField(
        choices=[[k, v] for (k, v) in GROUPS.items()],  # Randomize the group order?
        label="",
    )
    transfer = models.CurrencyField(
        label="",
        min=0,
        max=C.X_PARTICIPANTS,
    )


# PAGES
class FairEfficient(Page):
    form_model = 'player'
    form_fields = ['efficient', 'fair']

    get_timeout_seconds = get_timeout_seconds
    timer_text = TIMER_TEXT

    @staticmethod
    def is_displayed(player):
        return (player.round_number == 1) & (get_timeout_seconds(player) > 3)


class Understanding(Page):
    form_model = 'player'
    form_fields = ['color_group', 'transfer']

    get_timeout_seconds = get_timeout_seconds
    timer_text = TIMER_TEXT

    def is_displayed(player: Player):
        return (not (player.participant.mechanisms[player.round_number - 1] == EFFICIENT) & (
                    get_timeout_seconds(player) > 3))

    @staticmethod
    def vars_for_template(player):
        p = player
        part = p.participant
        variables = {
            'procedure': part.mechanisms[p.round_number - 1],
            'pages': 2,
            'show_head': True,
            'wta': None,
        }
        if get_timeout_seconds(player) > 3:
            variables['wta'] = cu(part.wta[part.mechanisms[p.round_number - 1]])
        if EFFICIENT in part.mechanisms:
            variables['pages'] = 1
        if (variables['pages'] == 2) & (p.round_number == 2):
            variables['show_head'] = False
        if variables['procedure'] == MARKET:
            variables['market_threshold'] = f"{C.HALF + 1}<sup>ème</sup>"
        return variables

    @staticmethod
    def before_next_page(player, timeout_happened):
        from random import choice
        p = player
        part = p.participant
        paid_procedure_belief = choice(part.mechanisms)
        # paid_procedure_belief = BARGAINING  # Only for testing purposes
        while paid_procedure_belief == EFFICIENT:
            paid_procedure_belief = choice(part.mechanisms)
        paid_question = choice(["color_group", "transfer"])
        # paid_question = 'transfer'  # Only for testing purposes
        if paid_question == "color_group":
            answer_value = GROUPS[p.field_maybe_none(paid_question)]
        else:
            answer_value = p.field_maybe_none(paid_question)
        part.paid_belief = {'procedure': paid_procedure_belief, 'question': paid_question, 'value': answer_value}


page_sequence = [FairEfficient, Understanding]
