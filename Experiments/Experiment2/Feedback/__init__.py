from otree.api import *

from settings import EFFICIENT, MARKET, BARGAINING, TRANSFER, X_PARTICIPANTS, INCENTIVE, TOM_FEE, MECHANISMS, \
    TIMER_TEXT, GREEN, BLUE

import string
import random
from os import environ


from datetime import datetime

doc = """
Final pages of the experiment.
It displays the results.
"""


def creating_session(subsession):
    subsession.session.payoff_computed = False


def get_timeout_seconds(player):
    session = player.session
    return (session.result_time - datetime.now()).total_seconds()


def set_payoffs(group):
    from statistics import multimode, median, mean
    from datetime import datetime
    from math import ceil
    ## Which procedure are in this experiment
    weights = [group.session.config[f"weight{i}"] for i in range(1, 4)]
    session_procedures = [i for (i, v) in zip(MECHANISMS, weights) if (v > 0)]
    ## Which procedure has been chosen by the majority?
    chosen_procedures = [p.participant.mechanism for p in group.get_players() if p.participant.finished]
    chosen_procedure = multimode(chosen_procedures)
    chosen = chosen_procedure[0]
    if len(chosen_procedure) != 1:
        first_time = None
        for p in group.get_players():
            if p.participant.finished:
                if (first_time is None) or (p.participant.time_started_utc < first_time):
                    first_time = p.participant.time_started_utc
                    chosen = p.participant.mechanism
    print(f"chosen = {chosen}")
    unchosen = set(session_procedures).difference({chosen}).pop()
    print(f"unchosen = {unchosen}")
    ## Payment for the chosen procedure (and beliefs)
    if chosen == EFFICIENT:
        for p in group.get_players():
            if p.participant.finished:
                p.payoff = C.TOMORROW_FEE
                if p.participant.color == GREEN:
                    p.payoff += cu(p.participant.number)
                else:
                    p.payoff += cu(TRANSFER)
    elif chosen == BARGAINING:
        median_wta = median([p.participant.wta[BARGAINING] for p in group.get_players() if p.participant.finished])
        mean_wta_received = mean([p.participant.wta[BARGAINING] for p in group.get_players() if
                                  (p.participant.finished and p.participant.wta[BARGAINING] < median_wta)])
        print(median_wta)
        print(f"mean wta: {mean_wta_received}")
        for p in group.get_players():
            if p.participant.finished:
                p.payoff = C.TOMORROW_FEE
                if p.participant.wta[BARGAINING] < median_wta:
                    p.participant.color = BLUE
                    p.payoff += cu(p.participant.wta[BARGAINING])
                else:
                    p.participant.color = GREEN
                    p.payoff += cu(p.participant.number)
                ## Beliefs payment
                if p.participant.paid_belief['procedure'] == BARGAINING:
                    if p.participant.paid_belief['question'] == 'transfer':
                        if abs(p.participant.paid_belief['value'] - cu(mean_wta_received)) <= cu(10):
                            p.payoff += INCENTIVE
                    else:
                        if p.participant.paid_belief['value'] == p.participant.color:
                            p.payoff += INCENTIVE
    else:
        median_wta = median([p.participant.wta[MARKET] for p in group.get_players() if p.participant.finished])
        print(median_wta)
        for p in group.get_players():
            if p.participant.finished:
                p.payoff = C.TOMORROW_FEE
                if p.participant.wta[MARKET] < median_wta:
                    p.participant.color = BLUE
                    p.payoff += cu(ceil(median_wta))
                else:
                    p.participant.color = GREEN
                    p.payoff += cu(p.participant.number)
                # Beliefs payments
                if p.participant.paid_belief['procedure'] == MARKET:
                    if p.participant.paid_belief['question'] == 'transfer':
                        if abs(p.participant.paid_belief['value'] - cu(median_wta)) <= cu(10):
                            p.payoff += INCENTIVE
                    else:
                        if p.participant.paid_belief['value'] == p.participant.color:
                            p.payoff += INCENTIVE
    ## Beliefs payment if unchosen procedure was selected as payment for beliefs
    if unchosen == BARGAINING:
        median_wta = median([p.participant.wta[BARGAINING] for p in group.get_players() if p.participant.finished])
        mean_wta_received = mean([p.participant.wta[BARGAINING] for p in group.get_players() if
                                  (p.participant.finished and p.participant.wta[BARGAINING] < median_wta)])
        print(f"mean wta: {mean_wta_received}")
        for p in group.get_players():
            if p.participant.finished:
                if p.participant.paid_belief['procedure'] == BARGAINING:
                    if p.participant.wta[BARGAINING] < median_wta:
                        color = BLUE
                    else:
                        color = GREEN
                    if p.participant.paid_belief['question'] == 'transfer':
                        if abs(p.participant.paid_belief['value'] - cu(mean_wta_received)) <= cu(10):
                            p.payoff += INCENTIVE
                    else:
                        if p.participant.paid_belief['value'] == color:
                            p.payoff += INCENTIVE
    elif unchosen == MARKET:
        median_wta = median([p.participant.wta[MARKET] for p in group.get_players() if p.participant.finished])
        for p in group.get_players():
            if p.participant.finished:
                if p.participant.paid_belief['procedure'] == MARKET:
                    if p.participant.wta[MARKET] < median_wta:
                        color = BLUE
                    else:
                        color = GREEN
                    if p.participant.paid_belief['question'] == 'transfer':
                        if abs(p.participant.paid_belief['value'] - cu(median_wta)) <= cu(10):
                            p.payoff += INCENTIVE
                    else:
                        if p.participant.paid_belief['value'] == color:
                            p.payoff += INCENTIVE
    ## No beliefs are paid in the EFFICIENT procedure.


class C(BaseConstants):
    NAME_IN_URL = 'Remerciements'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1

    TOMORROW_FEE = cu(TOM_FEE)


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    pass


# PAGES
class Pause(Page):
    @staticmethod
    def vars_for_template(player: Player):
        participant_label = ''
        sitefolder = 'EBouacida'
        if not (not player.participant.label):
            from urllib.request import urlopen
            if get_timeout_seconds(player) > 3: player.participant.finished = True
            participant_label = player.participant.label
            sitestart = 'https://s2ch-experiences.huma-num.fr/enligne'
            gain_security_key = environ.get('OTREE_GAIN_KEY')
            f = urlopen(
                sitestart
                + '/expe/'
                + sitefolder
                + '/connect.php?name='
                + participant_label
                + '&ajax=1&status=setvarsgetlinkcode&FirstFinished=1&key='
                + gain_security_key
            )
            key = f.read().decode('utf-8')
        return {'participant_label': participant_label, 'folder': sitefolder,
                'expirydate': player.session.result_time.strftime('%d/%m/%Y à partir de %Hh%M')}


class Fin(Page):
    form_model = 'player'
    form_fields = []

    get_timeout_seconds = get_timeout_seconds
    timer_text = "Temps restant avant que le paiement n'apparaisse (en minutes) :"

    @staticmethod
    def vars_for_template(player: Player):
        if get_timeout_seconds(player) > 14400:
            if not player.participant.finished:
                player.participant.finished = True
        return {
            'result_time': player.session.result_time.strftime("%d %B %Y à partir de %Hh%M")
        }

    @staticmethod
    def is_displayed(player: Player):
        return get_timeout_seconds(player) > 0


# PAGES
class Results(Page):
    form_model = 'player'
    form_fields = []

    def is_displayed(player: Player):
        if not player.session.payoff_computed:
            player.session.payoff_computed = True
            set_payoffs(player.group)
        return True


page_sequence = [Pause, Fin, Results]
