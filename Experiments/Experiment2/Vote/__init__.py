from otree.api import *

from settings import EFFICIENT, X_PARTICIPANTS, GROUPS, TRANSFER, MARKET, TIMER_TEXT
from datetime import datetime

doc = """
The app where subjects choose a mechanism.
"""


def get_timeout_seconds(player):
    session = player.session
    return (session.expiry - datetime.now()).total_seconds()


class C(BaseConstants):
    NAME_IN_URL = 'Vote'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1

    X_PARTICIPANTS = X_PARTICIPANTS
    HALF = int(X_PARTICIPANTS / 2)
    TRANSFER = cu(TRANSFER)


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    mechanism = models.BooleanField(choices=[[False, 'PROCÉDURE 1'], [True, 'PROCÉDURE 2']],
                                    label="Pour quelle procédure votez-vous ?")


# PAGES
class Mechanism(Page):
    form_model = 'player'
    form_fields = ['mechanism']

    get_timeout_seconds = get_timeout_seconds
    timer_text = TIMER_TEXT

    # @staticmethod
    # def is_displayed(player):
    #     return get_timeout_seconds(player) > 3

    @staticmethod
    def vars_for_template(player):
        part = player.participant
        if get_timeout_seconds(player) > 3:
            variables = {
                'wta1': part.wta[part.mechanisms[0]],
                'wta2': part.wta[part.mechanisms[1]],
            }
            if EFFICIENT in part.mechanisms:
                if part.number > C.HALF:
                    variables['transfer'] = cu(part.number)
                else:
                    variables['transfer'] = cu(C.TRANSFER)
            if MARKET in part.mechanisms:
                variables['market_threshold'] = f"{C.HALF + 1}<sup>ème</sup>"
            return variables
        else:
            return {
                'wta1': 0,
                'wta2': 0,
                'transfer': 0,
                'market_threshold': 0,
            }

    @staticmethod
    def before_next_page(player, timeout_happened):
        if timeout_happened:
            player.mechanism = None
            player.participant.mechanism = None
        else:
            player.participant.mechanism = player.participant.mechanisms[player.mechanism]


page_sequence = [Mechanism]
