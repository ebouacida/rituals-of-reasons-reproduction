from otree.api import *

from settings import FEE, TOM_FEE, INCENTIVE, MAX_WIN, MECHANISMS, MARKET, BARGAINING, EFFICIENT, X_PARTICIPANTS, \
    GROUPS, WEIGHTS, TRANSFER, GREEN, BLUE, TIMER_TEXT
from datetime import datetime, timedelta

import random
import locale

locale.setlocale(
    category=locale.LC_ALL,
    locale=""
)

## Initialiser une date à 0 dans la session puis changer le participant field time si 0 à la date du moment et le résultat au lendemain.

doc = """
Initialization of the treatments.

Welcome page and wta choices in the different procedures.
"""


def is_displayed1(player):
    return player.round_number == 1


def get_timeout_seconds(player):
    session = player.session
    return (session.expiry - datetime.now()).total_seconds()


class C(BaseConstants):
    NAME_IN_URL = 'Procedures'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 2

    # Different level of rewards
    FEE = FEE
    INCENTIVE = cu(INCENTIVE)
    TRANSFER = cu(TRANSFER)
    X_PARTICIPANTS = X_PARTICIPANTS
    HALF = int(X_PARTICIPANTS / 2)
    TOMORROW_FEE = TOM_FEE


def creating_session(subsession):
    import itertools
    # d = datetime.now()
    numbers = [i for i in range(1, C.X_PARTICIPANTS + 1)]
    random.shuffle(numbers)
    shuffled_numbers = itertools.cycle(numbers)
    for p in subsession.get_players():
        part = p.participant
        session = subsession.session
        session.result_time = 0
        session.expiry = 0
        part.finished = False

        # Initialize the participants variables that are exported and create the different treatments
        weights = [subsession.session.config[f"weight{we}"] for we in range(1, len(MECHANISMS) + 1)]
        mechanisms_to_sample = [k for (k, v) in zip(MECHANISMS, weights) if v > 0]
        part.mechanisms = random.sample(mechanisms_to_sample, k=2)
        part.wta = {}
        part.number = next(shuffled_numbers)
        if part.number > C.HALF:
            part.color = GREEN
        else:
            part.color = BLUE


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    """Configure the experiment subjects will participate in."""

    # WTA
    wta = models.CurrencyField(min=0, max=C.X_PARTICIPANTS, label="")


# PAGES
class Welcome(Page):
    is_displayed = is_displayed1

    @staticmethod
    def vars_for_template(player):
        d = datetime.now()
        try:
            end = datetime(d.year, d.month, d.day + 1, 12, 0)
            # end = datetime(d.year, d.month, d.day, 17, 50)
        except ValueError:
            end = datetime(d.year, d.month + 1, 1, 12, 0)
        part = player.participant
        session = player.session
        if session.result_time == 0:
            session.expiry = datetime(d.year, d.month, d.day, 22, 15) # Participants must finish the experiment before 22h15 the day of the experiment.
            # session.expiry = d + timedelta(minutes=15)  # For TESTING purposes only
            session.result_time = end
        return {
            'stage3_earnings': cu(C.INCENTIVE),
            'stage3_earnings_dol': cu(C.INCENTIVE).to_real_world_currency(player.session),
            'conversion': cu(1).to_real_world_currency(player.session),
            'fee': cu(C.FEE).to_real_world_currency(player.session),
            'tomorrow_fee': cu(C.TOMORROW_FEE).to_real_world_currency(player.session),
            'stage2_earnings': cu(C.X_PARTICIPANTS),
            'stage2_earnings_dol': cu(C.X_PARTICIPANTS).to_real_world_currency(player.session),
            'result_time': end.strftime("%d %B %Y à partir de %Hh%M")
        }

    # @staticmethod
    # def before_next_page(player, timeout_happened):

    # print(player.participant.expiry)


class Explanation(Page):
    is_displayed = is_displayed1
    get_timeout_seconds = get_timeout_seconds

    timer_text = TIMER_TEXT

    # def vars_for_template(player):
    #     part = player.participant
    #     return {
    #         'converted_number': cu(part.number).to_real_world_currency(player.session) 
    #     }


class Procedure(Page):
    form_model = 'player'
    get_timeout_seconds = get_timeout_seconds

    timer_text = TIMER_TEXT

    @staticmethod
    def is_displayed(player):
        return get_timeout_seconds(player) > 3

    @staticmethod
    def get_form_fields(player):
        part = player.participant
        if part.mechanisms[player.round_number - 1] == EFFICIENT:
            return []
        else:
            return ['wta']

    @staticmethod
    def vars_for_template(player):
        part = player.participant
        variables = {
            'instructions': f"Procedures/{part.mechanisms[player.round_number - 1]}.html",
            # 'show_ft' : (part.mechanisms[player.round_number - 1] == EFFICIENT),
        }
        if part.mechanisms[player.round_number - 1] == EFFICIENT:
            if part.number > C.HALF:
                variables['transfer'] = cu(part.number)
            else:
                variables['transfer'] = cu(C.TRANSFER)
        elif part.mechanisms[player.round_number - 1] == MARKET:
            variables['market_threshold'] = f"{C.HALF + 1}<sup>ème</sup>"
        return variables

    @staticmethod
    def before_next_page(player, timeout_happened):
        p = player
        part = p.participant
        # if timeout_happened:
        #     p.wta = None
        part.wta[part.mechanisms[p.round_number - 1]] = p.field_maybe_none('wta')


page_sequence = [Welcome, Explanation, Procedure]
