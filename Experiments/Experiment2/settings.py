from os import environ


# 2023 Experiments on LULUs

FEE = 150
TOM_FEE = 50
X_PARTICIPANTS = 100
INCENTIVE = 20
MAX_WIN = FEE + X_PARTICIPANTS + INCENTIVE
MARKET = "Market"
BARGAINING = "Bargaining"
EFFICIENT = "Efficient"
MECHANISMS = [MARKET, BARGAINING, EFFICIENT]
WEIGHTS = [1, 1, 0]  # Change for each experiment. Here it is to have only Market and Lottery procedures.
TRANSFER = 50
GREEN = 'VERT'
BLUE = 'BLEU'
TIMER_TEXT = "Temps restant pour compléter cette expérience (en minutes) :"

GROUPS = {False: GREEN, True: BLUE}  # False: do not suffer from the LULU.

SESSION_CONFIGS = [
    dict(
        name='LULU_MTurk_Test',
        display_name="LULU location, MTurk, Test",
        app_sequence=['Procedures', 'Vote', 'Comprehension', 'Demographics', 'Feedback'],
        mturk_hit_settings=dict(
            keywords='bonus, study, academic',
            title='An experiment from Lancaster University',
            description=f'We (Elias Bouacida and Renaud Foucart) are researchers at Lancaster University Management '
                        f'School (United Kingdom) and University Paris 8 (France).\nYou are expected to take around 5 minutes '
                        f'to complete this experiment. You will receive at least \${FEE} and at most \${MAX_WIN} for '
                        f'participating in this experiment.',
            frame_height=500,
            template='global/mturk_template.html',
            minutes_allotted_per_assignment=60,
            expiration_hours=7 * 24,
            qualification_requirements=[
                {'QualificationTypeId': "31FAIA0PU5CI8VLJ5SQG4PLO96SB4B",
                 'Comparator': "DoesNotExist"},
                {'QualificationTypeId': '3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',
                 'Comparator': "DoesNotExist"},
                {'QualificationTypeId': '3CROANS6V1G2XOLUP1WVH4LDTFDFH2',
                 'Comparator': "DoesNotExist"},
            ],
            grant_qualification_id='3CROANS6V1G2XOLUP1WVH4LDTFDFH2',  # Build a new one for this experiment
        ),
    ),
    dict(
        name='LULU_MTurk',
        display_name="LULU location, MTurk",
        app_sequence=['Procedures', 'Vote', 'Comprehension', 'Demographics', 'Feedback'],
        mturk_hit_settings=dict(
            keywords='bonus, study, academic',
            title='An experiment from Lancaster University',
            description=f'We (Elias Bouacida and Renaud Foucart) are researchers at Lancaster University Management '
                        f'School (United Kingdom) and University Paris 8 (France).\nYou are expected to take around 5 minutes '
                        f'to complete this experiment. You will receive at least \${FEE} and at most \${MAX_WIN} for '
                        f'participating in this experiment.',
            frame_height=500,
            template='global/mturk_template.html',
            minutes_allotted_per_assignment=60,
            expiration_hours=7 * 24,
            qualification_requirements=[
                {'QualificationTypeId': "31FAIA0PU5CI8VLJ5SQG4PLO96SB4B",
                 'Comparator': "DoesNotExist"},
                {'QualificationTypeId': '3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',
                 'Comparator': "DoesNotExist"},
                {'QualificationTypeId': '3CROANS6V1G2XOLUP1WVH4LDTFDFH2',
                 'Comparator': "DoesNotExist"},
                #            {
                #                'QualificationTypeId': "00000000000000000071",
                #                'Comparator': "EqualTo",
                #                'LocaleValues': [{'Country': "GB"}] # To restrict to workers from the UK and NI
                #            },
            ],
            grant_qualification_id='3CROANS6V1G2XOLUP1WVH4LDTFDFH2',
            # to prevent retakesm build a new one for this experiment
        ),
    ),
    dict(
        name='LULU_S2CH',
        display_name="LULU version 1",
        app_sequence=['Procedures', 'Vote', 'Comprehension', 'Demographics', 'Feedback', 'redirecttopayment'],
    ),
]

# if you set a property in SESSION_CONFIG_DEFAULTS, it will be inherited by all configs
# in SESSION_CONFIGS, except those that explicitly override it.
# the session config can be accessed from methods in your apps as self.session.config,
# e.g. self.session.config['participation_fee'] 

SESSION_CONFIG_DEFAULTS = dict(
    num_demo_participants=2,
    real_world_currency_per_point=0.02,
    participation_fee=FEE * 0.02,
    doc="An experiment on choosing between procedures with a vote.",
    weight1=WEIGHTS[0],  # Randomization weights to choose which mechanisms to implement in the session.
    # Should be of the same size as MECHANISMS Please make sure that at least to weights are above 0.
    weight2=WEIGHTS[1],
    weight3=WEIGHTS[2],
)

PARTICIPANT_FIELDS = ['mechanisms', 'wta', 'mechanism', 'number', 'color', 'paid_belief', 'finished']
# mechanisms: The two procedures the participant faced.
# wta: The stated compensation in each procedure
# mechanism: the procedure the participant voted for
# number: The individual number of the participant
# color: Group the participants belongs to
# paid_belief: Which question is used to pay the beliefs and the value associated with it.
# expiry: Temps disponible pour faire l'expérience
# finished: est-ce qu'un participant est arrivé à la dernière page de l'expérience en session 1 (avant le timeout)

SESSION_FIELDS = ['payoff_computed', 'result_time', 'expiry']
# result_time: The time when the results will be published.

# ISO-639 code
# for example: de, fr, ja, ko, zh-hans
LANGUAGE_CODE = 'fr'

# e.g. EUR, GBP, CNY, JPY
REAL_WORLD_CURRENCY_CODE = 'EUR'
USE_POINTS = True

ROOMS = []

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

SECRET_KEY = environ.get('OTREE_SECRET_KEY')

INSTALLED_APPS = ['otree']
