from otree.api import *

from settings import INCENTIVE, TIMER_TEXT

import random
from datetime import datetime

doc = """
Asking questions about the subjects and their valuers.
"""


def get_timeout_seconds(player):
    session = player.session
    return (session.expiry - datetime.now()).total_seconds()


def make_field(question, choice, rev=False):
    """Create a question using a StringField and RadioSelect buttons.

    Allow for reversing the first two answers."""
    if rev:
        choice2 = choice.copy()
        choice2[0], choice2[1] = choice2[1], choice2[0]
        return models.StringField(choices=choice2, label=question, widget=widgets.RadioSelect)
    else:
        return models.StringField(choices=choice, label=question, widget=widgets.RadioSelect)


def make_likert(statement):
    """Create a question using an IntegerField and RadioSelect buttons.  
    """
    return models.IntegerField(
        widget=widgets.RadioSelectHorizontal,
        choices=[-2, -1, 0, 1, 2],
        label=statement,
    )


def creating_session(subsession):
    """Randomizing order for questions.
    """
    for p in subsession.get_players():
        p.gender_rev = bool(random.randint(0, 1))  # Currently useless.


class C(BaseConstants):
    NAME_IN_URL = 'Questions'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1

    GENDER = ['homme', 'femme', 'autre', 'ne souhaite pas répondre']
    AGE = ["<25", "25-39", "40-54", ">54", 'ne souhaite pas répondre']
    OCCUPATION = ["employé", "travailleur indépendant", "étudiant", "retraité", "autre"]
    INCENTIVE = cu(INCENTIVE)
    BONUS = cu(5)


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    # Demographics variables
    age = make_field('Quel est votre âge ?', C.AGE)
    gender_rev = models.BooleanField(initial=True)  # Currently useless
    gender = make_field('Comment vous identifiez-vous ?', C.GENDER)
    employment = make_field("Quel occupation avez-vous ?", C.OCCUPATION)
    region = models.StringField(label="What is your country of residence?")
    religion = models.StringField(label="What is your religion?")
    ethnicity = models.StringField(label="What is your ethnic group?")

    # Opinions questions
    # success = make_likert("It is possible to be successful on your own.")
    success = make_likert(
        "Un grand groupe qui se soutient mutuellement est nécessaire pour réussir, plutôt que d’être seul.")
    money = make_likert("Avoir de l’argent est important pour être heureux.")
    # effort = make_likert("People who put effort working end up better than those who do not.")
    trust = make_likert("En France, on peut faire confiance aux autres.")
    work = make_likert("Le principal moteur du succès dans la vie est l'effort fourni.")
    luck = make_likert("Le principal moteur du succès dans la vie est la chance.")
    society = make_likert(
        "C'est le rôle de la société de protéger les individus contre leurs propres erreurs et mauvaises décisions.")
    merit = make_likert(
        "Une société juste devrait récompenser les travailleurs pour les efforts qu'ils fournissent, et non pour leurs capacités innées.")
    attention = make_likert(
        f"Si vous répondez attentivement aux questions, veuillez sélectionner En désaccord et recevoir un bonus de {C.BONUS}.")

    # success = models.BooleanField(
    #    label = "Do you believe that it is possible to be successful on your own or a large group that supports each other is necessary?",
    #    choices = [[False, "It is possible to be successful on your own."],
    #               [True, "A large group is necessary to be successful."]]
    # )
    #     money = models.IntegerField(
    #         label = "Do you believe that having money is important to be happy?",
    #         choices = [[4, "Indispensable to be happy."],
    #                    [3, "Very important to be happy."],
    #                    [2, "Important to be happy."],
    #                    [1, "Somewhat important  to be happy."],
    #                    [0, "Not important to be happy."]
    #                   ],
    #         widget=widgets.RadioSelect
    #     )
    #     effort = models.IntegerField(
    #         label= "In general, people who put effort working end up much better, better, worst, or much worst than those that do not put an effort?",
    #         choices = [[3, "Much better than those who do not put an effort."],
    #                    [2, "Better than those who do not put an effort"],
    #                    [1, "Worst than those who do not put an effort."],
    #                    [0, "Much worst than those who do not put an effort."]
    #                   ],
    #         widget=widgets.RadioSelect
    #     )

    #     trust = models.BooleanField(
    #         label = "In general, in our country, would you say that one can trust other people or that people cannot be trusted?",
    #         choices = [[False, "You cannot trust others."],
    #                    [True, "You can trust others."]]

    #     )

    # Understanding of the experiment
    understand1 = make_likert("La PROCÉDURE 1 est facile à comprendre.")
    understand2 = make_likert("La PROCÉDURE 2 est facile à comprendre.")


# PAGES
class Demographics(Page):
    """Ask for some demographic information about the subject."""
    form_model = 'player'
    form_fields = ['age', 'gender', 'employment']

    get_timeout_seconds = get_timeout_seconds
    timer_text = TIMER_TEXT

    @staticmethod
    def is_displayed(player):
        return get_timeout_seconds(player) > 3


class Values(Page):
    """Some general value questions."""
    form_model = 'player'
    form_fields = ['work', 'luck', 'society', 'merit', 'understand1', 'understand2', 'success', 'money', 'trust',
                   'attention']

    get_timeout_seconds = get_timeout_seconds
    timer_text = TIMER_TEXT

    @staticmethod
    def is_displayed(player):
        return get_timeout_seconds(player) > 3

    @staticmethod
    def before_next_page(player, timeout_happened):
        player.payoff = (player.attention == -1) * C.BONUS


page_sequence = [Demographics, Values]
