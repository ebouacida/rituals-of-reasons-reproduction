---
subtitle: "README for Replication"
title: "Rituals of Reasons"
author: 
    - name: Elias Bouacida
      email: elias.bouacida@univ-paris8.fr
      orcid: 0000-0001-8656-6678
      affiliation:
          - name: University Paris 8
            city: Saint-Denis
            country: France
    - name: Renaud Foucart
      email: r.foucart@lancaster.ac.uk
      orcid:  0000-0003-0611-6146
      affiliation:
          - name: Lancaster University
            city: Lancaster
            country: United Kingdom
date: last-modified
date-format: long
format: html
---

# Overview

This folder contains the experimental instructions and code for the 16 treatments of the experiment.
`Experiment1_A` contains the code we used to run the first session of Experiment 1.
`Experiment1_B` contains the code we used to run the second session of Experiment 1.
`Experiment1_C` contains the code we used to run the third session of Experiment 1.
`Experiment2` contains the code we used to run Experiment 2.
The code was running on different version of Python and oTree. 
It has last been tested on Python 3.12.7 and otree 5.11.1.

## Treatments

There are 16 treatments in the experiment.

- Four different non-lottery procedures:
  1. Rock, Paper, Scissors where winners get the reward
  2. Rock, Paper, Scissors where losers get the reward. This one was not used in the analysis.
  3. Guessing the Paintings
  4. Time, where there were some variations between the treatments. We therefore did not keep the first time answers.
- For each procedure, subjects could either have control or not on the choices.
- There was also a pilot with a different design which is not part of this repository.

## Procedures

We first describe the explicitly random lottery procedure and then the three non-lottery ones. 
Each procedure consists in ranking subjects based on a sequence of five actions.
All procedures were in the form of  contest in which the top half of the performers received a reward. 

Our lottery procedure is a bet on whether each of the 5 numbers in the next day results of a state lottery, the [DC-5 Lottery](https://www.dclottery.com/games/dc-5), are odd or even. 
We then rank subjects by the number of correct guesses they made. 
As the lottery is run by a third party, our subjects should in theory trust this procedure as being the most transparent one we offer. 
In the treatments with control, subjects make the bets themselves. 
In those without control, we draw a sequence for them.

The following three procedures *RPS*, *Paintings* and *Time* do not exhibit explicit randomness in the allocation. 
The objective of *RPS* and *Paintings* is to respect @elster1989solomonic's rituals of reason: subjects make decisions that can be interpreted as meaningful and are easy to understand, although they have no predictable influence on the outcome. 
In contrast, *Time* is designed to appear arbitrary and meaningless.

In *RPS*, each action is taken from the set {Rock, Paper, Scissors}. 
In the treatment without control, we provide the subjects with a sequence of actions. 
In the treatment with control, we let them choose their actions. 
We play the actions of each subject against those of all other subjects in their session. 
As in the traditional game, Rock wins against Scissors, Scissors against Paper, and Paper against Rock.  
A subject wins if they win more rounds than their opponent.
In case of a tie, the first winner of a round wins the game. 
In the rare event where both players have chosen the same sequence of actions, we consider it as neither a win nor a loss.
We then rank all participants by their number of wins, a tie is half a win. 
The main difference with a traditional RPS game is that all actions are chosen in advance and a subject uses the same actions against all other subjects. 
The objective is to ensure people do not choose RPS for its entertainment value: regardless of the chosen procedure, they get to ``play'' RPS in the same way. 

In *Paintings*, we explain to the subjects that one of the two experimenters, Renaud, has chosen 5 pairs of paintings, each pair by the same artist, and that the second experimenter, Elias, has chosen in each pair his favourite painting. 
To win, subjects must guess (with control) the paintings chosen by Elias. 
We rank participants according to the number of paintings they have guessed correctly. 
In case of tie, we use the first pair of paintings, then the second and so on. 
n the treatment without control, we make the guesses for the subjects. 
Subjects could download a password protected PDF copy of Elias' choices, and we revealed the password alongside the results of the experiments.

We designed the third non-lottery procedure, *Time*, in a way to make it look as unreasonable as possible: an arbitrary, complicated, and intractable algorithm. 
In the treatment without control, we provide subjects with a code corresponding to the last five digits of a time, in hours, minutes, and seconds. 
In the treatment with control, we asked subjects to choose their time, which is then transformed into a five digits code. We rank the codes of all subjects according to the algorithm.


# Experiment

All the experiments were run online using oTree [@Chen2016].

## Pilot

The pilot happened in two sessions on Amazon Mechanical Turk.
All the code for the pilot are in the folder `Experiments\Pilot`.

1. One on April 29, 2020, one session in the morning, with 10 subjects, and one in the afternoon, with 178 subjects.
The two sessions differ only on how the belief question was asked.
The codes of the sessions are in the folders `20200429MorningExperiment` and `20200429AfternoonExperiment` respectively. 
2. One on May 13 and May 14, 2020, one session each day, with 89 subjects in total.
The two sessions are identical, except for the date at which they have been carried.
The code of the two sessions of the experiment is in the folder `2020051314Experiment`.

The pilot is quite different from the rest of the experiments, both in the software used (Python 3.7 and oTree 2.5.8) and the protocol.
The pilot is described in an earlier version of the paper [see @Bouacida2020].

## Experiment 1

Experiment 1 ran in three sessions, which mainly differ on the treatment used. 
It was run on Amazon Mechanical Turk.

1. One on June 22, 2021.
The session is available in the folder `Experiment1_A`.
2. One on July 7, 2021.
The session is available in the folder `Experiment1_B`.
1. One on August 31, 2021.
The session is available in the folder `Experiment1_C`.

### Protocol

The experiment is composed of four parts. 

In the first part, we introduce two procedures to allocate the additional payment: one is a lottery, and the other is not. 
Subjects also choose their actions for the procedure(s) in which they have control (if any). 

In the second part, we ask them to vote on which procedure to use to allocate a reward to half of the members of their group. 
We focus on preferences for allocation problems in which the decision makers are one of the many subjects of the mechanism. 
 We then implement the choice of the majority. 

In the third part, we ask them incentivized questions on their beliefs about their ranks in the procedure, and one incentivized measure of their ambiguity aversion.
 The belief elicitation was a simple binary choice, where we asked participants to pick whether they expected to win or lose in each procedure, and they received a payment of \$0.20 for a correct answer.
 The ambiguity task was a simple choice between two urns, one with known probabilities and the other without. 
 Finally, we ask non-incentivized demographic and feedback questions. We provide screenshots of the complete instructions in Online Appendix G.

In the last part, one or two days later, subjects could check their results and others' using their anonymous identifier.
Screenshots of two results page are available in the folder `Data/Screenshots`.

## Experiment 2

Experiment 2 ran in one session, on August 14, 2023, and was pre-registered at <https://aspredicted.org/3yy5p.pdf>.
It was run using Prolific.

### Protocol

We pre-registered Experiment 2 and ran it on August 14, 2023, using Prolific on a representative sample of 600 USA residents.
The Pre-analysis Plan Report is available in [PAP](../Code/Pap.pdf).
We ran two treatments of this experiment, one with *RPS* and one with *Time* as a non-lottery procedure (with control), offered alongside the lottery without control.

All subjects received a fixed payment of \$1, and half of them received an additional payment of \$2.
The higher payment as compared to Experiment 1 reflects the inflation that occurred during the period.

The main difference with Experiment 1 is that in Experiment 2, we also measure the intensity of preferences. 
To do so, we ask subjects for their preferred procedure and the smallest amount we should pay them to change their choice.
We then implement the choice of a subject chosen at random. 
We could not use the standard voting procedure of Experiment 1, because measuring the intensity of preferences would then imply measuring the perceived probability of being pivotal. 
In Experiment 2, we also sent an email to all subjects with the detail of their payments, as well as their results in the two procedures. 