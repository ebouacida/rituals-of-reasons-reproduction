from otree.api import *

from settings import lottery, rps, arbitrary, paintings
import string
import random

doc = """
Final page of the experiment.
It displays the results.
"""

def id_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def creating_session(subsession):
    for p in subsession.get_players():
        p.completion_code = id_generator()    



class Constants(BaseConstants):
    name_in_url = 'Feedback'
    players_per_group = None
    num_rounds = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass

def custom_export(players):
    # header row
    yield ['session_code', 'participant_code', 'criteria', 'criteria_first', 'lottery_control', 'criteria_control', 'criteria_choices', 'lottery_choices', 'mechanism', 'arrival_code', 'best_mechanism']
    for p in players:
        participant = p.participant
        yield [p.session.code, participant.code, participant.criteria, participant.criteria_first, participant.lottery_control, participant.criteria_control, participant.criteria_choices, participant.lottery_choices, participant.mechanism, participant.arrival_code, participant.best_mechanism]
        
class Player(BasePlayer):
    completion_code = models.StringField()


# PAGES
class Results(Page):
    form_model = 'player'
    form_fields = []
    
    
    @staticmethod
    def vars_for_template(player):
        part = player.participant
        expe_number = 1 + 2 * part.criteria_control + part.lottery_control
        if part.criteria == paintings:
            expe_number = expe_number + 4
        elif part.criteria == arbitrary:
            expe_number = expe_number + 8
        return {"experiment": expe_number, "page": f"https://wp.lancs.ac.uk/lexelresults/sample-page/experiment-{expe_number}"}


page_sequence = [Results]
