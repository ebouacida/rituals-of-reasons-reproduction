from otree.api import *

from settings import reward, x_participants, rps_choices, lottery_choices, rps, paintings, arbitrary, lottery, criteria, fee, small_win, criteria_cum_weights, control_cum_weights
from functions import *

import random
from datetime import datetime
from pytz import timezone

doc = """
Initialization of the treatments.

Welcome page and choices for the criteria and the lottery.
"""



def count_odd_digits(n: str):
    return sum(int(i) % 2 for i in n)

def infer_time(n: int):
    if 0 <= n < 10:
        return 0, n
    elif 0 <= n < 60:
        t = f"{n}"
        return int(t[0]), int(t[1])
    else:
        raise ValueError('The time is out of bound.')
    
class Constants(BaseConstants):
    name_in_url = 'Algorithms'
    players_per_group = None
    num_rounds = 1
    
    # Different level of rewards
    loss = cu(0)
    reward = cu(reward)
    fee = cu(fee)
    small_win = cu(small_win)

    # The different choices
    rps = [[k, v] for (k, v) in rps_choices.items()]
    paintings = [0, 1]
    lottery = [[k, v] for (k, v) in lottery_choices.items()]
    arbitrary_short = [i for i in range(6)]
    arbitrary_long = [i for i in range(10)]
    

def creating_session(subsession):
    dc_tz = timezone("US/Eastern")
    d = datetime.now(tz=dc_tz)
    try:
        subsession.session.lottery_time = datetime(d.year, d.month, d.day+1, 12, 50, tzinfo=dc_tz).strftime("%d %B %Y at %I.%M%p")
        subsession.session.result_time = datetime(d.year, d.month, d.day+1, 15, 00, tzinfo=dc_tz).strftime("%d %B %Y at %I.%M%p")
    except ValueError:
        subsession.session.lottery_time = datetime(d.year, d.month+1, 1, 12, 50, tzinfo=dc_tz).strftime("%d %B %Y at %I.%M%p")
        subsession.session.result_time = datetime(d.year, d.month+1, 1, 15, 00, tzinfo=dc_tz).strftime("%d %B %Y at %I.%M%p")        
    
    for p in subsession.get_players():
        part = p.participant
        if 'criteria' in p.session.config:
            part.criteria = p.session.config['criteria']
        else:
            part.criteria = random.choices(criteria, cum_weights = criteria_cum_weights)[0]
        part.criteria_first = bool(random.randint(0, 1))
        part.criteria_control = random.choices([False, True], cum_weights = control_cum_weights)[0]
        part.lottery_control = random.choice([False, True])
    
     # Initialize the participants variables that are exported
        part.criteria_choices = ""
        part.lottery_choices = ""
        part.mechanism = ""
        part.best_mechanism = ""
        part.arrival_code = ""
        
        
        if not part.lottery_control:
            p.lottery_choice1 = bool(random.randint(0, 1))
            p.lottery_choice2 = bool(random.randint(0, 1))
            p.lottery_choice3 = bool(random.randint(0, 1))
            p.lottery_choice4 = bool(random.randint(0, 1))
            p.lottery_choice5 = bool(random.randint(0, 1))   
            part.lottery_choices = [p.lottery_choice1, p.lottery_choice2, p.lottery_choice3, p.lottery_choice4, p.lottery_choice5]
        if part.criteria == rps:
            p.criteria_choice1 = random.randint(1, 3) 
            p.criteria_choice2 = random.randint(1, 3) 
            p.criteria_choice3 = random.randint(1, 3) 
            p.criteria_choice4 = random.randint(1, 3) 
            p.criteria_choice5 = random.randint(1, 3) 
            part.criteria_choices = [p.criteria_choice1, p.criteria_choice2, p.criteria_choice3, p.criteria_choice4, p.criteria_choice5]
            
        elif part.criteria == paintings:
            part.criteria_control = False
            part.x_participants = 50
            p.criteria_choice1 = random.randint(0, 1) 
            p.criteria_choice2 = random.randint(0, 1) 
            p.criteria_choice3 = random.randint(0, 1) 
            p.criteria_choice4 = random.randint(0, 1) 
            p.criteria_choice5 = random.randint(0, 1)             
            part.criteria_choices = [p.criteria_choice1, p.criteria_choice2, p.criteria_choice3, p.criteria_choice4, p.criteria_choice5]
        else:
            if part.criteria_control:
                part.x_participants = 50
            else:
                part.x_participants = 100
                
class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass

def criteria_choice1_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return Constant.paintings
    return Constants.arbitrary_long

def criteria_choice2_choices(player):
    if  player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return Constant.paintings
    return Constants.arbitrary_short

def criteria_choice3_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return Constant.paintings
    return Constants.arbitrary_long

def criteria_choice4_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return Constant.paintings
    return Constants.arbitrary_short

def criteria_choice5_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return Constant.paintings
    return Constants.arbitrary_long

class Player(BasePlayer):
    # Criteria choices
    criteria_choice1 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice2 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice3 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice4 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice5 = models.IntegerField(widget=widgets.RadioSelect)
    
    hours = models.IntegerField(min=0, max=23, label="Please choose your hour:")
    minutes = models.IntegerField(min=0, max=59, label="Please choose your minutes:")
    seconds = models.IntegerField(min=0, max=59, label="Please choose your seconds:")
    
    # Lottery choices
    lottery_choice1 = models.BooleanField(label='For the first round, which action do you choose?', choices=Constants.lottery)
    lottery_choice2 = models.BooleanField(label='For the second round, which action do you choose?', choices=Constants.lottery)
    lottery_choice3 = models.BooleanField(label='For the third round, which action do you choose?', choices=Constants.lottery)
    lottery_choice4 = models.BooleanField(label='For the fourth round, which action do you choose?', choices=Constants.lottery)
    lottery_choice5 = models.BooleanField(label='For the fifth round, which action do you choose?', choices=Constants.lottery)
    

    
        
# PAGES
class Welcome(Page):
    @staticmethod
    def before_next_page(player, timeout_happened):
        part = player.participant
        dc_tz = timezone("US/Eastern")
        d = datetime.now(tz=dc_tz)
        part.arrival_time = d
        part.arrival_code = d.strftime("%H%M%S")[1:]   
        if part.criteria == arbitrary and not part.criteria_control:
            split_code = [int(j) for j in part.arrival_code]
            player.criteria_choice1 = split_code[0]
            player.criteria_choice2 = split_code[1]
            player.criteria_choice3 = split_code[2]
            player.criteria_choice4 = split_code[3]
            player.criteria_choice5 = split_code[4]
    
    @staticmethod
    def vars_for_template(player):
        return { "stage3_earnings": cu(2 * Constants.small_win) }
            
            
class Page1(Page):
    form_model = 'player'
    
    @staticmethod
    def get_form_fields(player):
        part = player.participant
        if part.criteria_first and part.criteria == arbitrary and part.criteria_control:
            return ["hours", "minutes", "seconds"]
        elif not part.criteria_first and part.lottery_control:
            return [f"lottery_choice{i}" for i in range(1, 6)]
        else:
            return []
    
    @staticmethod
    def vars_for_template(player):
        part = player.participant
        variables = dict(
            criteria_instructions = "Algorithms/Arbitrary.html",
            x_participants = x_participants,
            mechanism = part.criteria, 
        )
        if part.criteria_first:
            if part.criteria == paintings:
                variables["criteria_instructions"] = "Algorithms/Paintings.html"
                for i in range(11, 16):
                    variables[f"painting{i-10}"] = f"Algorithms/{i}_{part.criteria_choices[i-11]}.jpg"
            elif part.criteria == rps:
                variables["criteria_instructions"] = "Algorithms/RPS.html"
                variables["sequence"] = ", ".join(rps_choices[a] for a in part.criteria_choices)
            elif not part.criteria_control:
                variables["sequence"] = part.arrival_time.strftime("%H hours %M minutes and %S seconds")
                variables["even"] = count_odd_digits(part.arrival_code)
        else:
            variables["mechanism"] = lottery
            if part.lottery_control:
                variables["lottery_instructions"] = "Algorithms/Control.html"
            else:
                variables["lottery_instructions"] = "Algorithms/No_Control.html"
                variables["sequence"] = ", ".join(lottery_choices[a] for a in part.lottery_choices)
        return variables
    
    @staticmethod
    def before_next_page(player, timeout_happened):
        part = player.participant
        if part.criteria_first:
            if part.criteria == arbitrary and part.criteria_control:
                player.criteria_choice1 = infer_time(player.hours)[1]
                player.criteria_choice2, player.criteria_choice3 = infer_time(player.minutes)
                player.criteria_choice4, player.criteria_choice5 = infer_time(player.seconds)                
            part.criteria_choices = [player.criteria_choice1, player.criteria_choice2, player.criteria_choice3, player.criteria_choice4, player.criteria_choice5]
        elif part.lottery_control:
            part.lottery_choices = [player.lottery_choice1, player.lottery_choice2, player.lottery_choice3, player.lottery_choice4, player.lottery_choice5]
        
    
class Page2(Page): 
    form_model = 'player'
    
    @staticmethod
    def get_form_fields(player):
        part = player.participant
        if not part.criteria_first and part.criteria == arbitrary and part.criteria_control:
            return ["hours", "minutes", "seconds"]
        elif part.criteria_first and part.lottery_control:
            return [f"lottery_choice{i}" for i in range(1, 6)]
        else:
            return []
        
    @staticmethod
    def vars_for_template(player):
        part = player.participant
        variables = dict(
            criteria_instructions =  "Algorithms/Arbitrary.html",
            x_participants = x_participants,
            mechanism = part.criteria,
        )
        if part.criteria_first:
            variables["mechanism"] = lottery
            if part.lottery_control:
                variables["lottery_instructions"] = "Algorithms/Control.html"
            else:
                variables["lottery_instructions"] = "Algorithms/No_Control.html"
                variables["sequence"] = ", ".join(lottery_choices[a] for a in part.lottery_choices)
        else:
            if part.criteria == paintings:
                variables["criteria_instructions"] = "Algorithms/Paintings.html"
                for i in range(11, 16):
                    variables[f"painting{i-10}"] = f"Algorithms/{i}_{part.criteria_choices[i-11]}.jpg"
            elif part.criteria == rps:
                variables["criteria_instructions"] = "Algorithms/RPS.html"
                variables["sequence"] = ", ".join(rps_choices[a] for a in part.criteria_choices)
            elif not part.criteria_control:
                variables["sequence"] = part.arrival_time.strftime("%H hours %M minutes and %S seconds")
                variables["even"] = count_odd_digits(part.arrival_code)
        return variables

    @staticmethod
    def before_next_page(player, timeout_happened):
        part = player.participant
        if not part.criteria_first:
            if part.criteria == arbitrary and part.criteria_control:
                player.criteria_choice1 = infer_time(player.hours)[1]
                player.criteria_choice2, player.criteria_choice3 = infer_time(player.minutes)
                player.criteria_choice4, player.criteria_choice5 = infer_time(player.seconds)        
            part.criteria_choices = [player.criteria_choice1, player.criteria_choice2, player.criteria_choice3, player.criteria_choice4, player.criteria_choice5]
        else:
            part.lottery_choices = [player.lottery_choice1, player.lottery_choice2, player.lottery_choice3, player.lottery_choice4, player.lottery_choice5]

            
            
page_sequence = [Welcome, Page1, Page2]
