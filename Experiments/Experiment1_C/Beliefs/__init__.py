from otree.api import *

from settings import small_win, rps_choices, lottery_choices, lottery, rps, paintings, arbitrary
from random import shuffle


doc = """
Belief and comparative beliefs elicitations.
"""



def creating_session(subsession):
    for p in subsession.get_players():   
        part = p.participant
        if p.round_number == 1:
            p.criteria = part.criteria_first
        else:
            p.criteria = not part.criteria_first        


def best_mechanism_choices(player):
    c = [player.participant.criteria, lottery]
    shuffle(c)
    return [[False, c[0]], [True, c[1]]]

            
class Constants(BaseConstants):
    name_in_url = 'Beliefs'
    players_per_group = None
    num_rounds = 2
    
    small_win = cu(small_win)


    
class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    belief = models.IntegerField(label="Do you think you will be ranked among the:",  widget=widgets.RadioSelect, 
                                 choices=[
                                     [-1, "Bottom half"],
                                     [1, "Top half"],
                                 ]
                                )
    criteria = models.BooleanField(initial=False)
    best_mechanism = models.BooleanField(label="Do you think your rank will be higher in?")


# PAGES
class Belief(Page):   
    form_model = 'player'
    form_fields = ['belief']
    
    @staticmethod
    def vars_for_template(player: Player):
        part = player.participant
        variables = dict(
            title = lottery,
            choices = ", ".join(lottery_choices[a] for a in part.lottery_choices),
        )
        if player.criteria:
            variables["title"] = part.criteria
            if part.criteria == paintings:
                for i in range(11, 16):
                    variables[f"painting{i-10}"] = f"Algorithms/{i}_{part.criteria_choices[i-11]}.jpg"
            elif part.criteria == rps:
                variables["choices"] = ", ".join(rps_choices[a] for a in part.criteria_choices)  
            else:
                variables["choices"] = "".join(str(a) for a in part.criteria_choices)
        return variables
    
    @staticmethod
    def before_next_page(player: Player, timeout_happened):
        if player.round_number == 1:
            player.criteria = not player.criteria
    

class ComparativeBelief(Page):
    form_model = 'player'
    form_fields = ['best_mechanism']
    
    @staticmethod
    def is_displayed(player: Player):
        return player.round_number == Constants.num_rounds
    
    @staticmethod
    def before_next_page(player: Player, timeout_happened):
        player.participant.best_mechanism = player.get_field_display('best_mechanism')
    
    @staticmethod
    def vars_for_template(player: Player):
        part = player.participant
        variables = dict(
            criteria_link = "global/RPS.html"
        )
        if part.criteria == paintings:
            variables["criteria_link"] = "global/Paintings.html"
        elif part.criteria == arbitrary:
            variables["criteria_link"] = "global/Arrival.html"
        return variables


page_sequence = [Belief, ComparativeBelief]
