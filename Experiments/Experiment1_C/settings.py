from os import environ

fee = 0.8
reward = 1.6
small_win = 0.10
max_win = fee + reward + 2 * small_win
#rps_winner = [True, False]
#control = [True, False]
x_participants = 100
rps = "Rock, Paper, Scissors"
paintings = "Guessing the Paintings"
arbitrary = "Time"
criteria = ["Rock, Paper, Scissors", "Guessing the Paintings", "Time"] # Twice RPS for because loser and winner treatment
criteria_cum_weights = [0, 1, 4]
lottery = "DC-5 Lottery"
control_cum_weights = [2, 3]

lottery_choices =  {True: "Even", False: "Odd"}
rps_choices = {1: "Rock", 2: "Paper", 3: "Scissors"}

SESSION_CONFIGS = [
    dict(
        name = 'MTurk_Test', 
        display_name="Reward Allocation, MTurk, Test",
        app_sequence=['Algorithms', 'Mechanism', 'Beliefs', 'Questionnaire', 'Feedback'], 
        num_demo_participants=1,
        criteria=paintings,
        mturk_hit_settings=dict(
            keywords='bonus, study, academic',
            title='An experiment from Lancaster University',
            description=f'We (Elias Bouacida and Renaud Foucart) are researchers at Lancaster University Management '
                        f'School (United Kingdom).\nYou are expected to take less than 5 minutes '
                        f'to complete this experiment. You will receive at least \${fee} and at most \${max_win} for '
                        f'participating in this experiment.',
            frame_height=500,
            template='global/mturk_template.html',
            minutes_allotted_per_assignment=60,
            expiration_hours=7 * 24,
            qualification_requirements=[
                {'QualificationTypeId': "31FAIA0PU5CI8VLJ5SQG4PLO96SB4B",
                 'Comparator': "DoesNotExist"},
                                {'QualificationTypeId': '3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',
                 'Comparator': "DoesNotExist"},
                #            {
                #                'QualificationTypeId': "00000000000000000071",
                #                'Comparator': "EqualTo",
                #                'LocaleValues': [{'Country': "GB"}] # To restrict to workers from the UK and NI
                #            },
            ],
            grant_qualification_id='3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',  # to prevent retakes
        ),
    ),
    dict(
        name = 'MTurk', 
        display_name="Reward Allocation, MTurk",
        app_sequence=['Algorithms', 'Mechanism', 'Beliefs', 'Questionnaire', 'Feedback'], 
        num_demo_participants=1,
        mturk_hit_settings=dict(
            keywords='bonus, study, academic',
            title='An experiment from Lancaster University',
            description=f'We (Elias Bouacida and Renaud Foucart) are researchers at Lancaster University Management '
                        f'School (United Kingdom).\nYou are expected to take less than 5 minutes '
                        f'to complete this experiment. You will receive at least \${fee} and at most \${max_win} for '
                        f'participating in this experiment.',
            frame_height=500,
            template='global/mturk_template.html',
            minutes_allotted_per_assignment=60,
            expiration_hours=7 * 24,
            qualification_requirements=[
                              {'QualificationTypeId': "31FAIA0PU5CI8VLJ5SQG4PLO96SB4B",
                 'Comparator': "DoesNotExist"},
                                {'QualificationTypeId': '3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',
                 'Comparator': "DoesNotExist"},
                #            {
                #                'QualificationTypeId': "00000000000000000071",
                #                'Comparator': "EqualTo",
                #                'LocaleValues': [{'Country': "GB"}] # To restrict to workers from the UK and NI
                #            },
            ],
            grant_qualification_id='3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',  # to prevent retakes
        ),
    ),
]

# if you set a property in SESSION_CONFIG_DEFAULTS, it will be inherited by all configs
# in SESSION_CONFIGS, except those that explicitly override it.
# the session config can be accessed from methods in your apps as self.session.config,
# e.g. self.session.config['participation_fee'] 

SESSION_CONFIG_DEFAULTS = dict(
    real_world_currency_per_point=1.00, participation_fee=fee, doc="An experiment on choosing how to allocate a reward."
)

PARTICIPANT_FIELDS = ['criteria_first', 'criteria_choices', 'lottery_choices', 'criteria', 'criteria_control', 'lottery_control', 'mechanism', 'arrival_code', 'arrival_time',  'best_mechanism', 'x_participants']
# criteria_first: whether the criteria or the random allocation mechanism is presented first.
# criteria choices: what choices the participant made for the criteria
# lottery_choices: what choices the participant made for the lottery (if applicable)
# criteria: which criteria the participant faced
# criteria_control: whether the criteria of the random allocation mechanism is with/without control
# lottery_control: whether the lottery of the random allocation mechanism is with/without control
# mechanism: the chosen mechanism
# arrival_time: the time of arrival in the experiment.
# arrival_code: the code base on time of arrival used in some treatments and in the tie-breaking rules.
# best_mechanism: The mechanism the player thought he will be ranked the highest.
# x_participants: Expected number of participants in the treatment

SESSION_FIELDS = ['lottery_time', 'result_time']
# lottery_time: the time of the next DC-5 lottery
# result_time: The time when the results will be published.

# ISO-639 code
# for example: de, fr, ja, ko, zh-hans
LANGUAGE_CODE = 'en'

# e.g. EUR, GBP, CNY, JPY
REAL_WORLD_CURRENCY_CODE = 'USD'
USE_POINTS = False

#ROOMS = [
#    dict(
#        name='econ101',
#        display_name='Econ 101 class',
#        participant_label_file='_rooms/econ101.txt',
#    ),
#    dict(name='live_demo', display_name='Room for live demo (no participant labels)'),
#]

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

DEMO_PAGE_INTRO_HTML = """
Here are some oTree games.
"""


SECRET_KEY = environ.get('OTREE_SECRET_KEY')

INSTALLED_APPS = ['otree']
