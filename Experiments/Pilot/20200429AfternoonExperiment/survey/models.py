from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)

import random

doc = '''Defining the data needed for the Fair Randomization Experiment.'''


def shuffle(li):
    """Copy and shuffles a list to generate a random order."""
    new_li = li.copy()
    random.shuffle(new_li)
    return new_li


def make_field(q, c, rev=False):
    """Crate a question using a StringField and RadioSelect buttons."""
    if rev:
        if len(c) == 2:
            return models.StringField(choices=[c[1], c[0]], label=q, widget=widgets.RadioSelect)
        else:
            return models.StringField(choices=[c[1], c[0], c[2], c[3]], label=q, widget=widgets.RadioSelect)
    else:
        return models.StringField(choices=c, label=q, widget=widgets.RadioSelect)


class Constants(BaseConstants):
    name_in_url = 'survey'
    players_per_group = None
    num_rounds = 1

    # Different level of rewards.
    small_win = c(.1)
    loss = c(0)
    reward = c(1.4)

    # Constant variables in the code.
    rps = ['Rock', 'Paper', 'Scissors']
    mechanism = ['Coin Toss', 'Rock, Paper, Scissors']
    urns = ['Urn Left', 'Urn Right']
    balls = ['Red', 'Black']
    gender = ['Male', 'Female', 'Other', 'Prefer not to say']
    age = ["<25", "25-40", "40-55", ">55", 'Prefer not to say']
    occupation = ["Employed", "Self-employed", "Student", "Retired", "Other"]
    n_profiles = 2500


class Subsession(BaseSubsession):
    def creating_session(self):
        """Allocating each player in one of the four treatment.
        
        ability treatment: if True, RPS as an ability game is emphasized.
        knowledge treatment: if True, the probability in the RPS is shown when choosing the allocation mechanism.
        """
        for p in self.get_players():
            p.ability = random.randint(0, 1)
            p.knowledge = random.randint(0, 1)
            p.mechanism_rev = random.randint(0, 1)
            p.urn_rev = random.randint(0, 1)
            p.gender_rev = random.randint(0, 1)
            p.colour_rev = random.randint(0, 1)


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    # Demographics variables
    age = make_field('How old are you?', Constants.age)
    gender_rev = models.BooleanField(initial=True)
    gender = make_field('What is your gender?', Constants.gender, rev=gender_rev)
    employment = make_field("What is your occupation?", Constants.occupation)
    region = models.StringField(label="What is your country of residence?")

    # Ambiguity elicitation variables
    urn_rev = models.BooleanField(initial=True)
    urn = make_field('What urn do you choose?', Constants.urns, rev=urn_rev)
    colour_rev = models.BooleanField(initial=True)
    colour = make_field('What ball colour do you choose?', Constants.balls, rev=colour_rev)
    colour_drawn = models.StringField(initial="Black")
    urn_winner = models.BooleanField(initial=False)

    # RPS choices
    round1 = make_field('For the first round, which action do you choose?', Constants.rps)
    round2 = make_field('For the second round, which action do you choose?', Constants.rps)
    round3 = make_field('For the third round, which action do you choose?', Constants.rps)
    round4 = make_field('For the fourth round, which action do you choose?', Constants.rps)
    round5 = make_field('For the fifth round, which action do you choose?', Constants.rps)
    start_time = models.FloatField(initial=0)
    total_time = models.IntegerField(initial=0)
    probability = models.FloatField(initial=.5)
    mechanism_winner = models.BooleanField(initial=False)
    belief_winner = models.BooleanField(initial=False)

    # Allocation mechanism choice
    mechanism_rev = models.BooleanField(initial=True)
    mechanism = make_field('How do you want to get your reward?', Constants.mechanism, rev=mechanism_rev)

    # Treatments variables
    ability = models.BooleanField(initial=True)
    knowledge = models.BooleanField(initial=True)

    # Variables explaining choices.
    belief = models.IntegerField(min=0, max=2500)
    reasons = models.LongStringField()

    def vars_for_templates(self):
        return dict(fee=self.session.config['participation_fee'])
