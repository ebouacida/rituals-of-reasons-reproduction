from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants

from .proba import translate_rps, translate_ct, probability_score, profile, rps_game, ct_game

import time
import random


class Welcome(Page):
    """The page welcoming participants, with the first instructions and disclaimers."""
    form_model = 'player'
    form_fields = []

    def vars_for_template(self):
        """Showing the participation fee in the templates."""
        return dict(
            self.player.vars_for_templates(),
        )


class RPS(Page):
    """The page where they choose the RPS and Matching Pennies strategies."""
    form_model = 'player'
    form_fields = [f"rps_round{i}" for i in range(1, 6)] + [f"ct_round{i}" for i in range(1, 6)]

    def vars_for_template(self):
        """Get the arrival time on a page."""
        self.player.start_time = time.time()

    def before_next_page(self):
        """Save the RPS and MP strategies and compute the probability of winning and score of the strategies chosen in
        RPS."""
        # Get the time the page is left and compute time spent on the page.
        self.player.total_time = round(time.time() - self.player.start_time)

        # Save the sequence of RPS and MP.
        self.player.participant.vars['rps'] = translate_rps(
            [self.player.rps_round1, self.player.rps_round2, self.player.rps_round3, self.player.rps_round4,
             self.player.rps_round5])
        self.player.participant.vars['ct'] = translate_ct(
            [self.player.ct_round1, self.player.ct_round2, self.player.ct_round3, self.player.ct_round4,
             self.player.ct_round5])

        # Compute the probability of winning
        self.player.probability, self.player.score = probability_score(
            self.player.participant.vars['rps'], self.player.total_time)


class Mechanism(Page):
    """"Choice of the allocation mechanism."""
    form_model = 'player'
    form_fields = ['mechanism']

    def vars_for_template(self):
        """Send the names of the mechanisms to the template."""
        return {'coin': Constants.mechanism[0], 'rps': Constants.mechanism[1]}

    def before_next_page(self):
        """Compute the reward won by the player.
        
        Takes into account the mechanism chosen."""
        if self.player.mechanism == Constants.mechanism[0]:
            self.player.mechanism_winner = ct_game(self.participant.vars['ct'])
        else:
            self.player.mechanism_winner, _ = rps_game(
                self.player.participant.vars['rps'], random.choice(profile), self.player.total_time)

        self.player.payoff += Constants.reward * self.player.mechanism_winner


class Decisions(Page):
    """The page where players may provide their reasons for their choices."""
    form_model = 'player'
    form_fields = ['reasons']

    def vars_for_template(self):
        """Adapt the question to the mechanism chosen."""
        return {'reasons_label': f"Why did you choose the {self.player.mechanism} to get your reward?"}


class Belief1(Page):
    """The page where players provide their belief about the games."""
    form_model = 'player'

    def get_form_fields(self):
        """Build the form_fields of the first belief page."""
        if self.player.rps_belief_first:
            return ['rps_belief']
        else:
            return ['ct_belief']

    def before_next_page(self):
        """When we ask the belief, compute whether the real probability is close enough."""
        if self.player.rps_belief_first:
            self.player.belief1_winner = (abs(self.player.score - self.player.rps_belief) <= Constants.belief_precision)
        else:
            self.player.belief1_winner = (
                    abs(self.player.ct_belief - Constants.correct_ct_guess) <= Constants.belief_precision)

        self.player.payoff += self.player.belief1_winner * Constants.small_win


class Belief2(Page):
    """The page where players provide their belief about the games."""
    form_model = 'player'

    def get_form_fields(self):
        """Build the form_fields of the second belief page."""
        if self.player.rps_belief_first:
            return ['ct_belief']
        else:
            return ['rps_belief']

    def before_next_page(self):
        """When we ask the belief, compute whether the real score is close enough."""
        if self.player.rps_belief_first:
            self.player.belief2_winner = (
                    abs(self.player.ct_belief - Constants.correct_ct_guess) <= Constants.belief_precision)
        else:
            self.player.belief2_winner = (
                        abs(self.player.score - self.player.rps_belief) <= Constants.belief_precision)

        self.player.payoff += self.player.belief2_winner * Constants.small_win


class Demographics(Page):
    """Ask for some demographic information about the subject."""
    form_model = 'player'
    form_fields = ['age', 'gender', 'employment', 'region']


class Feedback(Page):
    """Tell the players how much they earned"""
    form_model = 'player'
    form_fields = []

    def vars_for_template(self):
        """Show the breakdown of the payoff to the players."""
        # Tell which of the Coin Toss or the RPS has been chosen by the subject.
        coin = False
        if self.player.mechanism == Constants.mechanism[0]:
            coin = True

        return dict(
            self.player.vars_for_templates(),
            earning=self.player.participant.payoff_plus_participation_fee(),
            coin=coin,
        )


page_sequence = [Welcome, RPS, Mechanism, Belief1, Belief2, Decisions, Demographics, Feedback]
