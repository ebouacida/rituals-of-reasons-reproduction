from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)

from settings import reward, small_win

import random

doc = '''Our second experiment on the social acceptability of random allocation.
In this experiment, subjects have to choose between a repeated matching penny game and a repeated Rock, Paper, Scissors 
game to get a reward.
In each game, they have to choose 5 actions.'''


def make_field(question, choice, rev=False):
    """Create a question using a StringField and RadioSelect buttons.

    Allow for reversing the first two answers."""
    if rev:
        choice2 = choice.copy()
        choice2[0], choice2[1] = choice2[1], choice2[0]
        return models.StringField(choices=choice2, label=question, widget=widgets.RadioSelect)
    else:
        return models.StringField(choices=choice, label=question, widget=widgets.RadioSelect)


class Constants(BaseConstants):
    name_in_url = 'survey'
    players_per_group = None
    num_rounds = 1

    # Different level of rewards.
    small_win = c(small_win)
    loss = c(0)
    reward = c(reward)

    # Constant variables in the code.
    rps = ['Rock', 'Paper', 'Scissors']
    ct = ['Head', 'Tail']
    mechanism = ['Coin Toss', 'Rock, Paper, Scissors']
    urns = ['Urn Left', 'Urn Right']
    balls = ['Red', 'Black']
    gender = ['Male', 'Female', 'Other', 'Prefer not to say']
    age = ["<25", "25-40", "40-55", ">55", 'Prefer not to say']
    occupation = ["Employed", "Self-employed", "Student", "Retired", "Other"]
    n_profiles = 2500
    correct_ct_guess = 0
    belief_precision = 0.1

    # Links to the instructions and beliefs for Matching Penny and Rock, Paper, Scissors.
    ct_instructions = 'survey/ct_instructions.html'
    rps_instructions = 'survey/rps_instructions.html'
    ct_belief = 'survey/ct_belief.html'
    rps_belief = 'survey/rps_belief.html'


class Subsession(BaseSubsession):
    def creating_session(self):
        """Initializing the values for all players.

        In particular, tell the order of apparition of variables when they are randomized."""
        for p in self.get_players():
            p.mechanism_rev = random.randint(0, 1)
            p.gender_rev = random.randint(0, 1)
            p.rps_first = random.randint(0, 1)
            p.rps_belief_first = random.randint(0, 1)
            p.belief_example1 = round(random.uniform(0, 5), ndigits=2)
            p.belief_example2 = round(random.uniform(-5, 0), ndigits=2)
            p.belief_example3 = round(random.uniform(-5, 5), ndigits=2)


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    """Define all the variables attached to a subject."""
    # Demographics variables
    age = make_field('How old are you?', Constants.age)
    gender_rev = models.BooleanField(initial=True)
    gender = make_field('What is your gender?', Constants.gender, rev=gender_rev)
    employment = make_field("What is your occupation?", Constants.occupation)
    region = models.StringField(label="What is your country of residence?")

    # RPS choices
    rps_round1 = make_field('For the first round, which action do you choose?', Constants.rps)
    rps_round2 = make_field('For the second round, which action do you choose?', Constants.rps)
    rps_round3 = make_field('For the third round, which action do you choose?', Constants.rps)
    rps_round4 = make_field('For the fourth round, which action do you choose?', Constants.rps)
    rps_round5 = make_field('For the fifth round, which action do you choose?', Constants.rps)
    start_time = models.FloatField(initial=0)
    total_time = models.IntegerField(initial=0)
    probability = models.FloatField(initial=.5)
    score = models.FloatField(initial=0, min=-5, max=+5)

    # Coin Toss choices
    ct_round1 = make_field('For the first round, which action do you choose?', Constants.ct)
    ct_round2 = make_field('For the second round, which action do you choose?', Constants.ct)
    ct_round3 = make_field('For the third round, which action do you choose?', Constants.ct)
    ct_round4 = make_field('For the fourth round, which action do you choose?', Constants.ct)
    ct_round5 = make_field('For the fifth round, which action do you choose?', Constants.ct)

    # Allocation mechanism choice
    rps_first = models.BooleanField(initial=False)
    mechanism_rev = models.BooleanField(initial=True)
    mechanism = make_field('How do you want to get your reward?', Constants.mechanism, rev=mechanism_rev)
    mechanism_winner = models.BooleanField(initial=False)

    # Beliefs variables
    rps_belief = models.FloatField(min=-5, max=5, label='On average, how many points do you expect to win?')
    ct_belief = models.FloatField(min=-5, max=5, label='On average, how many points do you expect to win?')
    rps_belief_first = models.BooleanField(initial=False)
    belief1_winner = models.BooleanField(initial=False)
    belief2_winner = models.BooleanField(initial=False)
    belief_example1 = models.FloatField(initial=0)
    belief_example2 = models.FloatField(initial=0)
    belief_example3 = models.FloatField(initial=0)

    # Variables explaining choices.
    reasons = models.LongStringField()

    def vars_for_templates(self):
        return {'fee':self.session.config['participation_fee']}
