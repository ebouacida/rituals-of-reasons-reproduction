from .models import Constants
import csv
from random import randint

profile = []
with open(".//survey//Profiles.csv", newline='') as f:
    file = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
    for row in file:
        profile.append([int(i) for i in row])

assert (len(profile) == Constants.n_profiles), "The number of profiles loaded is not 2,500."


def rps_round(a1, a2):
    """Tell whether the first or second action won the RPS round.
    
    The convention is that 1 = "Rock", 2 = "Paper", and 3 = "Scissors".
    Return 1 if the first action won.
    Return -1 if the second action won.
    Return 0 if it is a tie between the two actions.
    """
    if (a1 == 1) and (a2 == 3):
        return 1
    elif (a2 == 1) and (a1 == 3):
        return -1
    elif a1 > a2:
        return 1
    elif a1 == a2:
        return 0
    elif a1 < a2:
        return -1
    return ValueError(a1, a2, f"One of {a1} or {a2} is not a valid action. It should be between 1 and 3.")


def rps_game(s1, s2, t):
    """Compute who is the winner of the RPS game, according to our rules.
    
    Return 1 if the first player won, 0 otherwise.
    There is no tie in our game.
    If there is a tie in the end, if the integer t 1 is even, player 1 wins.
    The t used in our experiment is the time to select the 5 RPS actions.
    """
    round_results = [rps_round(a1, a2) for a1, a2 in zip(s1, s2)]
    score = sum(round_results)
    if score > 0:
        return 1, score
    elif score:  # Because python consider any non zero float as True, we get here when score < 0
        return 0, score
    while round_results:
        r = round_results.pop(0)  # Pop takes the first element out.
        if r > 0:
            return 1, score
        elif r < 0:
            return 0, score
    # If we reach this if, it means that we have looked at all the rounds, and it was always a tie.
    if t % 2 == 0:
        return 1, score
    return 0, score


def probability_score(s1, t):
    """Compute the probability of winning of the strategy profile s1 against all the profiles in t."""
    probability = 0.
    score = 0.
    for s2 in profile:
        p, s = rps_game(s1, s2, t)
        probability += p
        score += s
    return probability / Constants.n_profiles, score / Constants.n_profiles


def translate(game, dictionary):
    return [dictionary[s] for s in game]


def translate_rps(rps):
    """Transforms a profile of RPS from strings to integers."""
    translation = {'Rock': 1, 'Paper': 2, 'Scissors': 3}
    return translate(rps, translation)


def translate_ct(ct):
    """Transforms a profile of Coin Tosses from strings to integers."""
    translation = {'Head': 1, 'Tail': 0}
    return translate(ct, translation)


def ct_game(ct):
    """Return whether the subject matched the computer head and tails."""
    win = 0
    for s in ct:
        if randint(0, 1) == s:
            win += 1
        else:
            win -= 1
    return win > 0
