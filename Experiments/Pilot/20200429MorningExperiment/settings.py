from os import environ

SESSION_CONFIG_DEFAULTS = dict(
    real_world_currency_per_point=1,
    participation_fee=.7,
    mturk_hit_settings=dict(
        keywords='bonus, study, academic',
        title='Rock, Paper, Scissors',
        description='We (Elias Bouacida and Renaud Foucart) are researchers at Lancaster University Management '
                    'School (United Kingdom).\nYou are expected to take on average 5 minutes and no more than 10 '
                    'minutes to complete this experiment. You will receive at least $0.70 and at most $2.30 for '
                    'participating in this experiment.',
        frame_height=500,
        template='global/mturk_template.html',
        minutes_allotted_per_assignment=120,
        expiration_hours=7 * 24,
        qualification_requirements=[
            {'QualificationTypeId': "31FAIA0PU5CI8VLJ5SQG4PLO96SB4B",
             'Comparator': "DoesNotExist"},
            #            {
            #                'QualificationTypeId': "00000000000000000071",
            #                'Comparator': "EqualTo",
            #                'LocaleValues': [{'Country': "GB"}] # To restrict to workers from the UK and NI
            #            },
        ],
        grant_qualification_id="31FAIA0PU5CI8VLJ5SQG4PLO96SB4B",  # to prevent retakes
    )
)
SESSION_CONFIGS = [{'name': 'fairness', 'num_demo_participants': 1, 'app_sequence': ['survey']}]
LANGUAGE_CODE = 'en'
REAL_WORLD_CURRENCY_CODE = 'USD'
USE_POINTS = False
DEMO_PAGE_INTRO_HTML = ''
ROOMS = [
    dict(
        name='department',
        display_name='Test Inside the Department',
        participant_label_file='_rooms/lab.txt',
        use_secure_urls=True,
    )
]

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
# ADMIN_PASSWORD = ''

SECRET_KEY = 'blahblah'

# if an app is included in SESSION_CONFIGS, you don't need to list it here
INSTALLED_APPS = ['otree']
