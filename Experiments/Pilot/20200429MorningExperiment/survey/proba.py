from .models import Constants
import csv

profile = []
with open(".//survey//Profiles.csv", newline='') as f:
    file = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
    for row in file:
        profile.append([int(i) for i in row])

assert (len(profile) == Constants.n_profiles), "The number of profiles loaded is not 2,500."


def rps_round(a1, a2):
    """Tell whether the first or second action won the RPS round.
    
    The convention is that 1 = "Rock", 2 = "Paper", and 3 = "Scissors".
    Return 1 if the first action won.
    Return -1 if the second action won.
    Return 0 if it is a tie between the two actions.
    """
    assert (a1 in [1, 2, 3]), "The first action is not one of Rock, Paper, Scissors. Please check your input."
    assert (a2 in [1, 2, 3]), "The second action is not one of Rock, Paper, Scissors. Please check your input."
    if (a1 == 1) and (a2 == 3):
        return 1
    elif (a2 == 1) and (a1 == 3):
        return -1
    elif a1 > a2:
        return 1
    elif a1 == a2:
        return 0
    elif a1 < a2:
        return -1


def rps_game(s1, s2, t):
    """Compute who is the winner of the RPS game, according to our rules.
    
    Return 1 if the first player won, 0 otherwise.
    There is no tie in our game.
    If there is a tie in the end, if the integer t 1 is even, player 1 wins.
    The t used in our experiment is the time to select the 5 RPS actions.
    """
    res = []
    for i, s in enumerate(s1):
        res.append(rps_round(s, s2[i]))
    if sum(res) > 0:
        return 1
    elif sum(res) == 0:
        for i, v in enumerate(res):
            if v > 0:
                return 1
            elif v < 0:
                return 0
            elif i == (len(res) - 1):
                if not t & 0x1:
                    return 1
    return 0


def p_win(s1, t):
    """Compute the probability of winning of the strategy profile s1 against all the profiles in t."""
    p = 0.
    for s2 in profile:
        p += rps_game(s1, s2, t)
    return p / Constants.n_profiles


def translate(rps):
    """Transforms a profile of RPS from strings to integers."""
    int_rps = []
    for r in rps:
        if r == 'Rock':
            int_rps.append(1)
        elif r == 'Paper':
            int_rps.append(2)
        elif r == 'Scissors':
            int_rps.append(3)
        else:
            raise ValueError("The elements are not one of Rock, Paper or Scissors")
    return int_rps
