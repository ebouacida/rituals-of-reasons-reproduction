from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants

from .proba import translate, p_win, profile, rps_game

import time
import random


class Welcome(Page):
    """The page welcoming participants, with the first instructions and disclaimers."""
    form_model = 'player'
    form_fields = []

    def vars_for_template(self):
        """Showing the participation fee."""
        return dict(
            self.player.vars_for_templates(),
        )


class RPS(Page):
    """The page where they choose the RPS strategies."""
    form_model = 'player'
    form_fields = ['round1', 'round2', 'round3', 'round4', 'round5']

    def vars_for_template(self):
        """Get the arrival time on a page."""
        self.player.start_time = time.time()

    def before_next_page(self):
        """Compute the probability of winning of the strategies chosen."""
        # Get the time the page is left and compute time spent on the page.
        self.player.total_time = round(time.time() - self.player.start_time)
        # print(self.player.total_time, "s")
        # Save the sequence of RPS.
        self.player.participant.vars['rps'] = translate(
            [self.player.round1, self.player.round2, self.player.round3, self.player.round4, self.player.round5])
        # Compute the probability of winning
        self.player.probability = p_win(self.player.participant.vars['rps'], self.player.total_time)


class Mechanism(Page):
    """"The page where the allocation mechanism is chosen."""
    form_model = 'player'
    form_fields = ['mechanism']

    def vars_for_template(self):
        """Send the probability of winning to the template."""
        return dict(
            p=round(self.player.probability * 100, 2),
            coin=Constants.mechanism[0],
            rps=Constants.mechanism[1],
        )

    def before_next_page(self):
        """Compute the reward won by the player.
        
        Takes into account the mechanism chosen."""
        if self.player.mechanism == Constants.mechanism[0]:
            #print("Using the Coin Toss")
            if random.randint(0, 1):
                self.player.mechanism_winner = True
        else:
            #print("Using RPS")
            if rps_game(self.player.participant.vars['rps'], random.choice(profile), self.player.total_time):
                self.player.mechanism_winner = True

        if self.player.mechanism_winner:
            self.player.payoff += Constants.reward


class Decisions(Page):
    """The page where player provide their belief about the RPS game and have the possibility to provide reasons for
    their choices. """
    form_model = 'player'

    def get_form_fields(self):
        """Build the form_fields.
        
        If the player already knows the probability (i.e. knowledge is True), we do not ask for his belief."""
        if self.player.knowledge:
            return ['reasons']
        else:
            return ['reasons', 'belief']

    def vars_for_template(self):
        """Adapt the question to the mechanism chosen."""
        return dict(
            reasons_label="Why did you choose the {} to get your reward?".format(self.player.mechanism)
        )

    def before_next_page(self):
        """If we asked the belief, compute whether the real probability is close enough.
        
        Follow Schlag and Tremewan's method."""
        if not self.player.knowledge:
            lb = self.player.belief / (Constants.n_profiles + 1)
            ub = (self.player.belief + 1) / Constants.n_profiles
            if lb <= self.player.probability <= ub:
                self.player.payoff += Constants.small_win
                self.player.belief_winner = True


class Demographics(Page):
    """Ask for some demographic information about the subject."""
    form_model = 'player'
    form_fields = ['age', 'gender', 'employment', 'region']


class Ambiguity(Page):
    """Elicit whether a player is ambiguity averse or not, using Ellsberg's urns."""
    form_model = 'player'
    form_fields = ['urn', 'colour']

    def before_next_page(self):
        """Compute whether a player won or not.
        
        Winning depends on the urn chosen.
        If it is the first urn, the composition is 50 red balls and 50 black balls.
        If it is the second urn, first we determine the composition and then we draw one ball from the urn."""
        if self.player.urn == Constants.urns[0]:
            self.player.colour_drawn = random.choice(Constants.balls)
        else:
            n_red = random.randint(0, 100)
            if random.randint(0, 100) <= n_red:
                self.player.colour_drawn = Constants.balls[0]
            else:
                self.player.colour_drawn = Constants.balls[1]
        if self.player.colour == self.player.colour_drawn:
            self.player.payoff += Constants.small_win
            self.player.urn_winner = True


class Feedback(Page):
    """Tell the players how much they earned"""
    form_model = 'player'
    form_fields = []

    def vars_for_template(self):
        """Show the breakdown of the payoff to the players."""
        # Tell which of the Coin Toss or the RPS has been chosen by the subject.
        coin = False
        if self.player.mechanism == Constants.mechanism[0]:
            coin = True

        return dict(
            self.player.vars_for_templates(),
            earning=self.player.participant.payoff_plus_participation_fee(),
            coin=coin,
        )


page_sequence = [Welcome, RPS, Mechanism, Decisions, Demographics, Ambiguity, Feedback]
