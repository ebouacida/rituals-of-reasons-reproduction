from otree.api import *

from settings import lottery, rps, arbitrary, paintings

doc = """
Final page of the experiment.
It displays the results.
"""


class Constants(BaseConstants):
    name_in_url = 'Feedback'
    players_per_group = None
    num_rounds = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass

def custom_export(players):
    # header row
    yield ['session_code', 'participant_code', 'criteria', 'criteria_first', 'control', 'rps_winner', 'criteria_choices', 'lottery_choices', 'mechanism', 'arrival_code', 'paintings_order',  'best_mechanism']
    for p in players:
        participant = p.participant
        yield [p.session.code, participant.code, participant.criteria, participant.criteria_first, participant.control, participant.rps_winner, participant.criteria_choices, participant.lottery_choices, participant.mechanism, participant.arrival_code, participant.paintings_order, participant.best_mechanism]
        
class Player(BasePlayer):
    pass


# PAGES
class Results(Page):
    form_model = 'player'
    form_fields = []
    
    
    @staticmethod
    def vars_for_template(player):
        expe_number = 0
        part = player.participant
        if part.criteria == rps:
            expe_number = (1 - part.rps_winner) * 2 + (2 - part.control)
        elif part.criteria == paintings:
            expe_number = 6 - part.control
        elif part.criteria == arbitrary:
            expe_number = 8 - part.control
        return {"experiment": expe_number, "page": f"https://wp.lancs.ac.uk/lexelresults/sample-page/experiment-{expe_number}"}


page_sequence = [Results]
