from otree.api import *

from settings import reward, x_participants, rps_choices, lottery_choices, rps, paintings, arbitrary, lottery, criteria, fee, small_win
from functions import *

import random
from datetime import datetime
from pytz import timezone

doc = """
Initialization of the treatments.

Welcome page and choices for the criteria and the lottery.
"""



def count_odd_digits(n: str):
    return sum(int(i) % 2 for i in n)
    
class Constants(BaseConstants):
    name_in_url = 'Algorithms'
    players_per_group = None
    num_rounds = 1
    
    # Different level of rewards
    loss = cu(0)
    reward = cu(reward)
    fee = cu(fee)
    small_win = cu(small_win)

    # The different choices
    rps = [[k, v] for (k, v) in rps_choices.items()]
    lottery = [[k, v] for (k, v) in lottery_choices.items()]
    arbitrary_short = [i for i in range(6)]
    arbitrary_long = [i for i in range(10)]
    

def creating_session(subsession):
    dc_tz = timezone("US/Eastern")
    d = datetime.now(tz=dc_tz)
    subsession.session.lottery_time = datetime(d.year, d.month, d.day+1, 12, 50, tzinfo=dc_tz).strftime("%d %B %Y at %I.%M%p")
    subsession.session.result_time = datetime(d.year, d.month, d.day+1, 15, 00, tzinfo=dc_tz).strftime("%d %B %Y at %I.%M%p")
    for p in subsession.get_players():
        part = p.participant
        if 'criteria' in p.session.config:
            part.criteria = p.session.config['criteria']
        else:
            part.criteria = random.choice(criteria)
        part.criteria_first = random.randint(0, 1)
        part.rps_winner = 0
        if part.criteria == rps:
            part.rps_winner = random.randint(0, 1)
        part.control = random.randint(0, 1)
        part.paintings_order = [random.sample([0, 1], k=2) for i in range(1, 6)]
    
     # Initialize the participants variables that are exported
        part.criteria_choices = ""
        part.lottery_choices = ""
        part.mechanism = ""
        part.best_mechanism = ""
        part.arrival_code = ""
        
        
        if not part.control:
            p.lottery_choice1 = bool(random.randint(0, 1))
            p.lottery_choice2 = bool(random.randint(0, 1))
            p.lottery_choice3 = bool(random.randint(0, 1))
            p.lottery_choice4 = bool(random.randint(0, 1))
            p.lottery_choice5 = bool(random.randint(0, 1))
            part.lottery_choices = [p.lottery_choice1, p.lottery_choice2, p.lottery_choice3, p.lottery_choice4, p.lottery_choice5]

            
                
class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass

def criteria_choice1_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return player.participant.paintings_order[0]
    return Constants.arbitrary_long

def criteria_choice2_choices(player):
    if  player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return player.participant.paintings_order[1]
    return Constants.arbitrary_short

def criteria_choice3_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return player.participant.paintings_order[2]
    return Constants.arbitrary_long

def criteria_choice4_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return player.participant.paintings_order[3]
    return Constants.arbitrary_short

def criteria_choice5_choices(player):
    if player.participant.criteria == rps:
        return Constants.rps
    elif player.participant.criteria == paintings:
        return player.participant.paintings_order[4]
    return Constants.arbitrary_long

class Player(BasePlayer):
    # Criteria choices
    criteria_choice1 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice2 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice3 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice4 = models.IntegerField(widget=widgets.RadioSelect)
    criteria_choice5 = models.IntegerField(widget=widgets.RadioSelect)
    
    # Lottery choices
    lottery_choice1 = models.BooleanField(label='For the first round, which action do you choose?', choices=Constants.lottery)
    lottery_choice2 = models.BooleanField(label='For the second round, which action do you choose?', choices=Constants.lottery)
    lottery_choice3 = models.BooleanField(label='For the third round, which action do you choose?', choices=Constants.lottery)
    lottery_choice4 = models.BooleanField(label='For the fourth round, which action do you choose?', choices=Constants.lottery)
    lottery_choice5 = models.BooleanField(label='For the fifth round, which action do you choose?', choices=Constants.lottery)
    

    
        
# PAGES
class Welcome(Page):
    @staticmethod
    def before_next_page(player, timeout_happened):
        part = player.participant
        dc_tz = timezone("US/Eastern")
        d = datetime.now(tz=dc_tz)
        part.arrival_time = d
        part.arrival_code = d.strftime("%H%M%S")[1:]   
        if part.criteria == arbitrary:
            split_code = [int(j) for j in part.arrival_code]
            player.criteria_choice1 = split_code[0]
            player.criteria_choice2 = split_code[1]
            player.criteria_choice3 = split_code[2]
            player.criteria_choice4 = split_code[3]
            player.criteria_choice5 = split_code[4]
    
    @staticmethod
    def vars_for_template(player):
        return { "stage3_earnings": cu(2 * Constants.small_win) }
            
            
class Page1(Page):
    form_model = 'player'
    
    @staticmethod
    def get_form_fields(player):
        part = player.participant
        if part.criteria_first and not part.criteria == arbitrary:
            return [f"criteria_choice{i}" for i in range(1, 6)]
        elif not part.criteria_first and part.control:
            return [f"lottery_choice{i}" for i in range(1, 6)]
        else:
            return []
    
    @staticmethod
    def vars_for_template(player):
        part = player.participant
        variables = dict(
            criteria_instructions = "Algorithms/RPS.html",
            x_participants = x_participants,
            mechanism = part.criteria, 
        )
        if part.criteria_first:
            if part.criteria == paintings:
                variables["criteria_instructions"] = "Algorithms/Paintings.html"
                for i in range(1, 6):
                    for j in range(0, 2):
                        variables[f"painting{i}{j}"] = f"Algorithms/{i}_{part.paintings_order[i-1][j]}.jpg"
            elif part.criteria == arbitrary:
                variables["even"] = count_odd_digits(part.arrival_code)
                variables["criteria_instructions"] = "Algorithms/Arbitrary.html"
                variables["time"] = part.arrival_time.strftime("%H hours %M minutes and %S seconds")
        else:
            variables["mechanism"] = lottery
            if part.control:
                variables["lottery_instructions"] = "Algorithms/Control.html"
            else:
                variables["lottery_instructions"] = "Algorithms/No_Control.html"
                variables["lottery_choices"] = ", ".join(lottery_choices[a] for a in part.lottery_choices)
        return variables
    
    @staticmethod
    def before_next_page(player, timeout_happened):
        part = player.participant
        if part.criteria_first:
            part.criteria_choices = [player.criteria_choice1, player.criteria_choice2, player.criteria_choice3, player.criteria_choice4, player.criteria_choice5]
        elif not part.criteria_first and part.control:
            part.lottery_choices = [player.lottery_choice1, player.lottery_choice2, player.lottery_choice3, player.lottery_choice4, player.lottery_choice5]
            #part.lottery_code = "".join(lottery_choices[a][0] for a in part.lottery_choices)
        
    
class Page2(Page): 
    form_model = 'player'
    
    @staticmethod
    def get_form_fields(player):
        part = player.participant
        if not part.criteria_first and not part.criteria == arbitrary:
            return [f"criteria_choice{i}" for i in range(1, 6)]
        elif part.criteria_first and part.control:
            return [f"lottery_choice{i}" for i in range(1, 6)]
        else:
            return []
        
    @staticmethod
    def vars_for_template(player):
        part = player.participant
        variables = dict(
            criteria_instructions = "Algorithms/RPS.html",
            x_participants = x_participants,
            mechanism = part.criteria,
        )
        if part.criteria_first:
            variables["mechanism"] = lottery
            if part.control:
                variables["lottery_instructions"] = "Algorithms/Control.html"
            else:
                variables["lottery_instructions"] = "Algorithms/No_Control.html"
                variables["lottery_choices"] = ", ".join(lottery_choices[a] for a in part.lottery_choices)
        else:
            if part.criteria == paintings:
                variables["criteria_instructions"] = "Algorithms/Paintings.html"
                for i in range(1, 6):
                    for j in range(0, 2):
                        variables[f"painting{i}{j}"] = f"Algorithms/{i}_{part.paintings_order[i-1][j]}.jpg"
            elif part.criteria == arbitrary:
                variables["even"] = count_odd_digits(part.arrival_code)
                variables["criteria_instructions"] = "Algorithms/Arbitrary.html"
                variables["time"] = part.arrival_time.strftime("%H hours %M minutes and %S seconds")
        return variables

    @staticmethod
    def before_next_page(player, timeout_happened):
        part = player.participant
        if not part.criteria_first:
            part.criteria_choices = [player.criteria_choice1, player.criteria_choice2, player.criteria_choice3, player.criteria_choice4, player.criteria_choice5]
        elif part.criteria_first and part.control:
            part.lottery_choices = [player.lottery_choice1, player.lottery_choice2, player.lottery_choice3, player.lottery_choice4, player.lottery_choice5]
            #part.lottery_code = "".join(lottery_choices[a][0] for a in part.lottery_choices)

            
            
page_sequence = [Welcome, Page1, Page2]
