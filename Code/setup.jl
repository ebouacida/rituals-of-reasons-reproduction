# Starting the environment
cd(@__DIR__()); import Pkg; Pkg.activate(".")

# import Pkg
# Pkg.activate(".")

# Importing the necessary libraries for the treatment
import CSV
using DataFrames
using Dates


# Loading the constants
include("Constants.jl")


# Loading the functions
include("CommonFunctions.jl")
include("PaymentFunctions.jl")