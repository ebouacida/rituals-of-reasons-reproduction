# %% [markdown]
#---
# title: "Experiment 1, session 1, Payments"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     echo: false
#     warning: false
#     cache: refresh
# format: html
# jupyter:
#   kernelspec:
#     display_name: Julia 1.6.1
#     language: julia
#     name: julia-1.6
# ---

# WARNING: This script does not run, as the original non-anonymized data is not disclosed.

# %%
import CSV
using DataFrames
using Dates
using Statistics
using Random: seed!, rand
seed!(0)


# %%
include("Constants.jl")


# %%
include("PaymentFunctions.jl")
include("CommonFunctions.jl")


# %%
all_data = CSV.read(joinpath("..", "Data", "OriginalData", "all_apps_wide_2021-06-22.csv"), DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);

# %% [markdown]
# Removing useless columns

# %%
# Remove some useless columns
select!(all_data, Not(r"player_role$"))
select!(all_data, Not(r"group*"))
select!(all_data, Not(r"payoff*"))
select!(all_data, Not(r"mechanism$"));

# %% [markdown]
# Loading the custom data

# %%
custom_data = CSV.read(joinpath("..", "Data", "OriginalData", "Feedback_2021-06-22.csv"), DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);

# %% [markdown]
# Merging the custom and all data

# %%
data = innerjoin(all_data, custom_data, on = [:participant_code,:session_code]); 

# %% [markdown]
# Merging turker id in the same column

# %%
@assert sum(ismissing.(data[!, :participant_mturk_worker_id]) .& .!ismissing.(data[!, :participant_label])) .== sum(.!ismissing.(data[!, :participant_label])) "Some participants have labels and mturk worker id"
@assert sum(.!ismissing.(data[!, :participant_mturk_worker_id]) .& ismissing.(data[!, :participant_label])) .== sum(.!ismissing.(data[!, :participant_mturk_worker_id])) "Some participants have mturk worker id and labels"
data[ismissing.(data[!, :participant_mturk_worker_id]), :participant_mturk_worker_id] = data[ismissing.(data[!, :participant_mturk_worker_id]), :participant_label];
select!(data, Not(:participant_label));

# %% [markdown]
# Keeping only players who reached the end of the experiment

# %%
data = data[.!ismissing.(data[!, :participant_current_page_name]).& (data[!, :participant_current_page_name] .== "Results"), :];

# %% [markdown]
# Simplifying the names of the columns

# %%
apps = ["Algorithms", "Beliefs", "Mechanism", "Feedback", "Questionnaire"]
models = ["player", "subsession"]
for col = names(data)
    for app = apps, model = models
        m = match(Regex("$app") * r"_(?<round>\d)_" * Regex("$model") * r"_(?<column>\w+)", string(col))
        if !(m === nothing)
            #println(m)
            if (app != "Beliefs") & (m[:column] == "round_number")
                select!(data, Not(col))
            elseif (app == "Beliefs") 
                if (m[:column] == "criteria")
                    select!(data, Not(col))
                elseif m[:column] == "belief"
                    rename!(data, col => Symbol(m[:column] * "_" * m[:round]))
                end
            else
                rename!(data, col => Symbol(m[:column]))
            end
        end
    end
end

# %% [markdown]
# Transform columns into their correct types

# %%
transform!(data, :criteria_first => (x -> Bool.(x)) => :criteria_first);
dformat = DateFormat("y-m-d H:M:S.s")
data[!, :participant_time_started_utc] = (x -> DateTime.(x[1:end-3], dformat)).(data[!, :participant_time_started_utc])
data[!, :criteria_choices] = criteriachoices.(data[!, :criteria_choices]);
transform!(data, :arrival_code => ByRow(x -> arrivalcode(x)) => :arrival_code)
for i = 1:5
    transform!(data, :arrival_code => ByRow(x -> parse(Int, string(x)[i])) => Symbol("arrival_code$i"))
end

# %%
# Tranform the lottery choices into their proper Bool representation
transform!(data, :lottery_choices => ByRow(x -> Int.(split(strip(x, [']', '[']), ", ") .== repeat(["True"], 5))) => :lottery_choices)


# %%
data[!, :mechanism] .= .!(data[!, :mechanism] .== "DC-5 Lottery");

# %% [markdown]
# Computing the beliefs

# %%
data[!, :criteria_belief] = zeros(Int, size(data, 1))
data[!, :lottery_belief] = zeros(Int, size(data, 1))
data[data[!, :criteria_first], :criteria_belief] = data[data[!, :criteria_first], :belief_1]
data[.!data[!, :criteria_first], :criteria_belief] = data[.!data[!, :criteria_first], :belief_2]
data[data[!, :criteria_first], :lottery_belief] = data[data[!, :criteria_first], :belief_2]
data[.!data[!, :criteria_first], :lottery_belief] = data[.!data[!, :criteria_first], :belief_1]
select!(data, Not([:belief_1, :belief_2]))

# %% [markdown]
# Computing the payments

# %%
include("PaymentFunctions.jl")


# %%
data[!, :prediction_payed] = rand([:lottery, :criteria, :best_mechanism], size(data, 1))
data[!, :best_mechanism_winner] = missings(Bool, size(data, 1))
data[!, :payment] = data[!, :urn_winner] .* low_reward .+ participation_fee[2021]

# %%
for name = ["lottery", "criteria"]
    data[!, Symbol("$(name)_winner")] = missings(Bool, size(data, 1))
    data[!, Symbol("$(name)_ranks")] = zeros(Int, size(data, 1))
    data[!, Symbol("$(name)_score")] = missings(Int, size(data, 1))
    data[!, Symbol("$(name)_belief_winner")] = missings(Bool, size(data, 1))
end

# %% 
# Now working only on the split data.
sepdata = groupby(data, [:criteria, :control, :rps_winner])
for key = keys(sepdata)
    expe_number = 0
    println("Criteria: ", key[:criteria]," Winner/loser: ", Bool(key[:rps_winner]), ", Control: ", Bool(key[:control]))
    winner(sepdata[key], x -> lotteryrank(x, dc5), "lottery")
    criteria_chosen = (mean(sepdata[key][!, :mechanism]) > 0.5)
    if criteria_chosen
        chosenmechanism = key[:criteria]
    else
        chosenmechanism = "DC-5 Lottery"
    end
    println("Mechanism chosen to attribute the reward: ", chosenmechanism)
    if key[:criteria] == "Arrival Time"
        winner(sepdata[key], arrivaltimeranks, "criteria")
        expe_number = 8 - key[:control]
    elseif key[:criteria] == "Rock, Paper, Scissors"
        winner(sepdata[key], totalrpsranks, "criteria", Bool(key[:rps_winner]))
        expe_number = (1 - key[:rps_winner]) * 2 + (2 - key[:control])
    elseif key[:criteria] == "Guessing the Paintings"
        winner(sepdata[key], x -> paintingrank(x, elias), "criteria")   
        expe_number = 6 - key[:control]
    end
    comparativebeliefwinner(sepdata[key])
    beliefwinner(sepdata[key], "criteria")
    beliefwinner(sepdata[key], "lottery")
    for row = eachrow(sepdata[key])
        if criteria_chosen
            reward_mechanism = "criteria"
        else
            reward_mechanism = "lottery"
        end
        row[:payment] += high_reward[2021] * row[Symbol("$(reward_mechanism)_winner")] +
            low_reward * (row[:criteria_belief_winner] * (row[:prediction_payed] .== :criteria) + 
            (row[:prediction_payed] .== :best_mechanism) * row[:best_mechanism_winner] + (row[:prediction_payed] .== :lottery) * row[:lottery_belief_winner])         
      end
end
data[!, :payment] = round.(data[!, :payment], digits = 2);


# %%
payments = data[!, [:participant_mturk_worker_id, :payment, :rps_winner, :criteria]]
payments |> CSV.write(joinpath("..", "Data", "Payments", "TheoreticalPayments.csv"))



