import boto3
import pandas as pd
import botocore
import os.path as os

MTURK_SANDBOX = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'
MTURK = 'https://mturk-requester.us-east-1.amazonaws.com'


## Loading login informations 
"""
Login information for the mturk session are saved in an .aws folder in the user folder.
The folder contains two files config and credentials.
The credentials contains the aws keys needed to access the account.
"""
session = boto3.Session(profile_name="elias")

mturk = session.client('mturk',
   endpoint_url = MTURK,
)


def grant_qualification(work_id, recent_hit = 1, new_quali = 1):
    if recent_hit > 0 and new_quali == 1:
        mturk.associate_qualification_with_worker(WorkerId = work_id, 
                                       QualificationTypeId = '3TFHWR10D0LVX2EZF5NNVSZ3Y8N3E6',
                                       IntegerValue = 1)
        
def need_qualification(quali1, quali2):
    if not quali1 == 1 and not quali2 == 1:
        return 1
    else:
        return 0

#df = pd.read_csv(os.join("..", "Data", "Input", "User_1616140_workers.csv"))

#df.apply(lambda x: grant_qualification(x["Worker ID"], x["Number of HITs approved or rejected - Last 30 days"]), axis = 1)