# %% [markdown]
#---
# title: "Experiment 1, paying subjects"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     eval: false
# format: html
# jupyter: python3
# ---

# WARNING: This script does not run, as the original non-anonymized data is not disclosed.

# %%
import boto3
import pandas as pd
import re
import datetime
import os

# %% [markdown]
"""
Login information for the mturk session are saved in an .aws folder in the user folder.
The folder contains two files config and credentials.
The credentials contains the aws keys needed to access the account.
"""
# %%
MTURK_SANDBOX = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'
MTURK = 'https://mturk-requester.us-east-1.amazonaws.com'
session = boto3.Session(profile_name="elias")
mturk = session.client('mturk',
   endpoint_url = MTURK,
)

# %%
# Getting the list of workers
res = mturk.list_hits(MaxResults = 100)
hits = res["HITs"]
n = res["NumResults"]
nt = res["NextToken"]
while n == 100:
    m = mturk.list_hits(MaxResults = 100, NextToken = nt)
    n = m["NumResults"]
    nt = m["NextToken"]
    hits = hits + m["HITs"]
    


# %%
startday  = datetime.datetime(2021, 7, 7, 12, 0, 0, tzinfo=tzlocal)


# %%
hitids = []
for r in hits:
    try:
        if r["CreationTime"] > startday:
            hitids.append(r["HITId"])
    except TypeError:
        print("Error")


# %%
tzlocal = datetime.timezone(datetime.timedelta(hours =1))



# %%
now = datetime.datetime.now(datetime.timezone.utc)
print(now)


# %%
#=for hit in hitids:
#    mturk.update_expiration_for_hit(HITId = hit, ExpireAt = now)


# %%
exec(open("getturkers.py").read())


# %%
df.to_csv(os.path.join("..", "..", "Data", "Input", "MTurkersExperiment2.csv"))


# Accepting and Rejecting HITs

# %%
exec(open("paymturk.py").read())


# %%
df = pd.read_csv(os.path.join("..", "..", "Data", "Payments", "AssignmentsAcceptationExperiment2.csv"));


# %%
len(df)


# %%
df.apply(lambda x: approve_hit(x["AssignmentId"]), axis = 1)

# %% [markdown]
## Bonuses

# %%
df.apply(lambda x: bonus(x["AssignmentId"], x["WorkerId"], x["Bonus"]), axis = 1)

# %% [markdown]
## Rejections

# %%
rejections = pd.read_csv(os.path.join("..", "..", "Data", "Payments", "AssignmentRejectionExperiment2.csv"))


# %%
rejections.apply(lambda x: reject_hit(x["AssignmentId"], x["Comment"]), axis = 1)

# %% [markdown]
# Check if all assignments have been accepted / rejectedexec(open("getturkers.py").read())

# %%
exec(open("getturkers.py").read())


# %%
df.apply(lambda x: reject_hit(x["AssignmentId"], "Finished the work only once, despite appearing to have tried it several times. Only one is paid."), axis = 1)


# %%
exec(open("getturkers.py").read())

# %% [markdown]
# Grant Qualifications

# %%
exec(open("GrantQualification.py").read())


# %%
qualification = pd.read_csv(os.path.join("..", "..", "Data", "Payments","QualificationExperiment2.csv"))


# %%
qualification.apply(lambda x: grant_qualification(x["WorkerId"]), axis = 1)



