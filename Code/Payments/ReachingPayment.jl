import CSV
using Dates
using Random: rand

include(joinpath("..", "Constants.jl"))
include(joinpath("..", "Functions.jl"))

all_data = CSV.read("..\\..\\Data\\OriginalData\\all_apps_wide_2021-06-22.csv", DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);

# Remove some useless columns (those finishing by player_role and id_in_group)
select!(all_data, Not(r"player_role$"))
select!(all_data, Not(r"group*"))
select!(all_data, Not(r"payoff*")) # Must check that it does not ruin the payments
select!(all_data, Not(r"mechanism$"))


custom_data = CSV.read("..\\..\\Data\\OriginalData\\Feedback_2021-06-22.csv", DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);

data = innerjoin(all_data, custom_data, on = [:participant_code,:session_code]); 

# Merging turker id in the same dataframe
@assert sum(ismissing.(data[!, :participant_mturk_worker_id]) .& .!ismissing.(data[!, :participant_label])) .== sum(.!ismissing.(data[!, :participant_label])) "Some participants have labels and mturk worker id"
@assert sum(.!ismissing.(data[!, :participant_mturk_worker_id]) .& ismissing.(data[!, :participant_label])) .== sum(.!ismissing.(data[!, :participant_mturk_worker_id])) "Some participants have mturk worker id and labels"

data[ismissing.(data[!, :participant_mturk_worker_id]), :participant_mturk_worker_id] = data[ismissing.(data[!, :participant_mturk_worker_id]), :participant_label];

select!(data, Not(:participant_label));

### Keeping only players who reached the end of the experiment
data = data[.!ismissing.(data[!, :participant_current_page_name]).& (data[!, :participant_current_page_name] .== "Results"), :];

# Make one of the column that should be a boolean a boolean
transform!(data, :criteria_first => (x -> Bool.(x)) => :criteria_first)


apps = ["Algorithms", "Beliefs", "Mechanism", "Feedback", "Questionnaire"]
models = ["player", "subsession"]
for col = names(data)
    for app = apps, model = models
        m = match(Regex("$app") * r"_(?<round>\d)_" * Regex("$model") * r"_(?<column>\w+)", string(col))
        if !(m === nothing)
            #println(m)
            if (app != "Beliefs") & (m[:column] == "round_number")
                select!(data, Not(col))
            elseif (app == "Beliefs") 
                if (m[:column] == "criteria")
                    select!(data, Not(col))
                elseif m[:column] == "belief"
                    rename!(data, col => Symbol(m[:column] * "_" * m[:round]))
                end
            else
                rename!(data, col => Symbol(m[:column]))
            end
        end
    end
end

#data[.!(data[!, :criteria] .== "Rock, Paper, Scissors"), :rps_winner] .= 0 # Should be useless in the new version of the experiment

# Removing non starting participants
#data = data[.!ismissing.(data[!, :participant_time_started_utc]), :]
dformat = DateFormat("y-m-d H:M:S.s")
data[!, :participant_time_started_utc] = (x -> DateTime.(x[1:end-3], dformat)).(data[!, :participant_time_started_utc])

# Parsing the choices
data[!, :criteria_choices] = criteriachoices.(data[!, :criteria_choices]);

transform!(data, :arrival_code => ByRow(x -> arrivalcode(x)) => :arrival_code)
for i = 1:5
    transform!(data, :arrival_code => ByRow(x -> parse(Int, string(x)[i])) => Symbol("arrival_code$i"))
end

# Tranform the lottery choices into their proper Bool representation
transform!(data, :lottery_choices => ByRow(x -> Int.(split(strip(x, [']', '[']), ", ") .== repeat(["True"], 5))) => :lottery_choices)

## Assigning the beliefs to the criteria or the lottery.
data[!, :criteria_belief] = zeros(Int, size(data, 1))
data[!, :lottery_belief] = zeros(Int, size(data, 1))

data[data[!, :criteria_first], :criteria_belief] = data[data[!, :criteria_first], :belief_1]
data[.!data[!, :criteria_first], :criteria_belief] = data[.!data[!, :criteria_first], :belief_2]
data[data[!, :criteria_first], :lottery_belief] = data[data[!, :criteria_first], :belief_2]
data[.!data[!, :criteria_first], :lottery_belief] = data[.!data[!, :criteria_first], :belief_1]
select!(data, Not([:belief_1, :belief_2]))

data[!, :payment] = data[!, :urn_winner] .* low_reward .+ participation_fee

# Now working only on the split data.

sepdata = groupby(data, [:criteria, :control, :rps_winner])


include("SavingData.jl")

# Start the analysis by treatment. Each (key, value) pair is a different treatment and is therefore treated separately.

splitdata = Dict{DataFrames.GroupKey, DataFrame}()

for key = keys(sepdata)
    expe_number = 0
    println("Criteria: ", key[:criteria]," Winner/loser: ", Bool(key[:rps_winner]), ", Control: ", Bool(key[:control]))
    df = DataFrame(sepdata[key])
    df  = winner(df, x -> lotteryrank(x, dc5), "lottery")
    chosenmechanism = majoritymechanism(df)
    println("Mechanism chosen to attribute the reward: ", chosenmechanism)
    if key[:criteria] == "Arrival Time"
        df = winner(df, arrivaltimeranks, long_short_criteria[key[:criteria]])
        expe_number = 8 - key[:control]
    elseif key[:criteria] == "Rock, Paper, Scissors"
        df = winner(df, totalrpsranks, long_short_criteria[key[:criteria]], Bool(key[:rps_winner]))
        expe_number = (1 - key[:rps_winner]) * 2 + (2 - key[:control])
    elseif key[:criteria] == "Guessing the Paintings"
        df = winner(df, x -> paintingrank(x, elias), long_short_criteria[key[:criteria]])   
        expe_number = 6 - key[:control]
    end
    
    df = comparativebeliefwinner(df, long_short_criteria[key[:criteria]])
    df = beliefwinner(df, long_short_criteria[key[:criteria]])
    df = beliefwinner(df, "lottery")
    df[!, :prediction_payed] = rand([:lottery, :criteria, :best_mechanism], size(df, 1))
    df[!, :payment] .+= high_reward * df[!, Symbol("$(long_short_criteria[chosenmechanism])_winner")] +
        low_reward * (df[!, :criteria_belief_winner] .* (df[!, :prediction_payed] .== :criteria) + (df[!, :prediction_payed] .== :best_mechanism) .* df[!, :best_mechanism_winner] + (df[!, :prediction_payed] .== :lottery) .* df[!, :lottery_belief_winner])    
    splitdata[key] = df
    datasaving(df, key, expe_number)
end

#############
### Linking the actions with the payments

workers = CSV.read("..\\Data\\Payments\\MTurkers.csv", DataFrame, normalizenames=true, drop = [1], pool = false); # Ignore the first column which is purely an index.
workers[ismissing.(workers[!, :CompletionCode]), :CompletionCode] .= "";

dformatworkers = DateFormat("y-m-d H:M:S")
workers[!, :SubmitTime] = (x -> DateTime.(x[1:end-6], dformatworkers)).(workers[!, :SubmitTime]);

## Looking for workers with more than one task
pbmaticworkers = workers[nonunique(workers[!, [:WorkerId]]), :WorkerId]
allworkers = unique(data[!, :participant_mturk_worker_id]);

paymentdata = DataFrame()
for (k, df) = splitdata
    append!(paymentdata, df[!, [:participant_mturk_assignment_id, :payment, :participant_mturk_worker_id]])
end

rename!(paymentdata, :participant_mturk_assignment_id => :AssignmentId, :participant_mturk_worker_id => :WorkerId)
paymentdata[!, :payment] .= round.(paymentdata[!, :payment] .- participation_fee, digits = 2)

paymentdata[ismissing.(paymentdata[!, :AssignmentId]), :AssignmentId] .= "";


paymentdata_simple = DataFrame()
for row in eachrow(paymentdata)
    if !in(row[:WorkerId], pbmaticworkers)
       push!(paymentdata_simple, row)
    end
end



paymentdata1 = innerjoin(paymentdata[.!ismissing.(paymentdata[!, :AssignmentId]), :], workers, on = [:WorkerId, :AssignmentId])
paymentdata_simple = innerjoin(select(paymentdata_simple, Not(:AssignmentId)), workers, on = [:WorkerId]);

pbmaticworkers = setdiff(allworkers, unique(paymentdata_simple[!, :WorkerId]))
workers_hard = DataFrame()
paymentdata_hard = DataFrame()

for row in eachrow(paymentdata)
    if in(row[:WorkerId], pbmaticworkers)
        push!(paymentdata_hard, row)
    end
end    
for row in eachrow(workers)
    if in(row[:WorkerId], pbmaticworkers)
        push!(workers_hard, row)
    end
end

paymentdata_hard[!, :CompletionCode] .= "";
paymentdata_hard[!, :HITId] .= "";
paymentdata_hard[!, :AssignmentId] .= "";
paymentdata_hard[!, :SubmitTime] = zeros(DateTime, size(paymentdata_hard, 1));



for i = 1:size(paymentdata_hard, 1)
    used_assignment_ids = paymentdata_simple[paymentdata_simple[!, :WorkerId] .== paymentdata_hard[i, :WorkerId], :AssignmentId]
    subworkers = workers_hard[(workers_hard[!, :WorkerId] .== paymentdata_hard[i, :WorkerId]), :]
    println(subworkers[!, :AssignmentId])
    (s = size(subworkers, 1)) |> println
    j = 0
    while (j < (s - 1))
        sort!(subworkers, :SubmitTime)
        k = 1
        while in(subworkers[j+k, :AssignmentId], used_assignment_ids)
            k += 1
        end
        paymentdata_hard[i+j, :HITId] = subworkers[j+k, :HITId]
        paymentdata_hard[i+j, :AssignmentId] = subworkers[j+k, :AssignmentId]
        paymentdata_hard[i+j, :CompletionCode] = subworkers[j+k, :CompletionCode]
        paymentdata_hard[i+j, :SubmitTime] = subworkers[j+k, :SubmitTime]     
        push!(used_assignment_ids, subworkers[j+k, :AssignmentId])
        j+=1
    end
            
end
append!(paymentdata_simple, paymentdata_hard)

paymentdata_simple |> CSV.write(joinpath("..", "Data", "Payments", "PaymentsMturks.csv"))
