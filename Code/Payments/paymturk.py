import boto3
import pandas as pd
import botocore

MTURK_SANDBOX = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'
MTURK = 'https://mturk-requester.us-east-1.amazonaws.com'

"""
Login information for the mturk session are saved in an .aws folder in the user folder.
The folder contains two files config and credentials.
The credentials contains the aws keys needed to access the account.
"""
session = boto3.Session(profile_name="elias")

mturk = session.client('mturk',
   endpoint_url = MTURK,
)

print("We have $" + mturk.get_account_balance()['AvailableBalance'] + " in our MTURK account.")




#df = pd.read_csv("../Data/Output/PaymentsMturks.csv")
#df = df[df["AssignmentId"].notna()]

#df = df.dropna()

#all_hits = [hit['HITId'] for hit in list_hit]
#all_assignments = [mturk.list_assignments_for_hit(HITId = hit) for hit in all_hits]
#assign = []
#for ass in all_assignments:
#    print(type(ass))
#    if ass['NumResults'] > 0:
#        for a in ass:
#            print(type(a))
#            print(a)
#            assign.append(a['AssignmentId'])
        

def approve_hit(assign_id):
    try:
        mturk.approve_assignment(AssignmentId = assign_id, RequesterFeedback="Thank you for participating in the experiment from Lancaster University.")
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == 'EntityAlreadyExists':
            print("User already exists")
        else:
            print("Unexpected error: %s" % e)
    
def bonus(assign_id, work_id, value):
    if value > 0:
        try:
            mturk.send_bonus(WorkerId = work_id, BonusAmount = f"{value}", AssignmentId = assign_id, Reason = "Bonus payment for the experiment from Lancaster University you participated in on August 31.", UniqueRequestToken=f"{work_id}_{assign_id}")
        except botocore.exceptions.ClientError as e:
            print("Error: %s" % e)

def reject_hit(assign_id, comment):
    try:
        mturk.reject_assignment(AssignmentId = assign_id, RequesterFeedback=comment)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "RequestError":
            print("Assignment does not exist")
            
            
#df.apply(lambda x: approve_hit(x["AssignmentId"]), axis = 1)
#df.apply(lambda x: bonus(x["AssignmentId"], x["WorkerId"], x["payment"]), axis = 1)
#df.apply(lambda x: reject_hit(x["AssignmentId"], x["Comment"]), axis = 1)
