using DataFrames
using PrettyTables
using Pipe: @pipe

## Careful some constants are defined in the file Constants.jl

#######
### Saving the data for display to subjects


# Formatter functions

"""
    print_yesno(v, i::Int, j::Int, k::Vector{Int} = []) 

Transform a Bool into yes and no.
"""
function print_yesno(v, i::Int, j::Int, k::Vector{Int} = []) 
    if j in k
        if v == 0
            return "No"
        else
            return "Yes"
        end
    else 
        return v
    end
end

"""
    print_belief(v, i::Int, j::Int, k::Vector{Int} = []) 

Transform predictions into literals.
"""
function print_belief(v, i::Int, j::Int, k::Vector{Int} = []) 
    if j in k
        if v == 1
            return "Top half"
        else
            return "Bottom half"
        end
    else 
        return v
    end
end

"""
    print_vector(v, i::Int, j::Int, k::Vector{Int} = [], dlm = "") 

Transform a vector into a string, separating elements using `dlm`.
"""
function print_vector(v, i::Int, j::Int, k::Vector{Int} = [], dlm = "") 
    if j in k
        return join(v, dlm)
    else
        return v
    end
end

"""
    print_dict(v, i::Int, j::Int, k::Vector{Int} = [], dic::Dict = RPS_inv)

Format a vector using a dictionary to translate and then join.
"""
function print_dict(v, i::Int, j::Int, k::Vector{Int} = [], dic::Dict = RPS_inv)
    if j in k
        return join(collect(dic[l] for l = v), ", ")
    else
        return v
    end
end

"""
    print_payed(v, i::Int, j::Int, k::Vector{Int} = [], criteria::String = "rps") 

Format the paid belief
"""
function print_payed(v, i::Int, j::Int, k::Vector{Int} = [], criteria::String = "rps") 
    if j in k
        if v == :best_mechanism
            return "Preferred Mechanism"
        elseif v == :lottery
            return "Lottery"
        elseif v == :criteria
            if criteria == "rps"
                return "RPS"
            else
                return titlecase("$criteria")
            end
        end
    else
        return v
    end
end

"""
    print_mech(v, i::Int, j::Int, k::Vector{Int} = [], criteria::String = "rps") 

Format the mechanisms
"""
function print_mechanism(v, i::Int, j::Int, k::Vector{Int} = [], criteria::AbstractString = "Rock, Paper, Scissors") 
    if j in k
        if v == "DC-5 Lottery"
            return criteria
        else
            return "DC-5 Lottery"
        end
    else
        return v
    end
end

#################
### Saving function

"""
    datasaving(df::AbstractDataFrame, key::Tuple{String, Bool}, i::Int = 1)

Export the result of each subject into different html tables.
"""
function datasaving(df::AbstractDataFrame, criteria::AbstractString="Rock, Paper, Scissors", i::Int = 1)
    criteria_short = long_short_criteria_experiment2[criteria]

    printcols =  [:payment,  :participant_criteria_choices, :participant_lottery_choices, :participant_mechanism,  :lottery_ranks, :criteria_ranks, :criteria_belief, :lottery_belief, :participant_best_mechanism, :prediction_payed]
    dfprint =  df[!, printcols]
    
    newnames = Tuple{Symbol, Symbol}[
        (:participant_lottery_choices, :lottery), 
        (:participant_best_mechanism, :preferred_mechanism), 
        (:participant_mechanism, :chosen_mechanism),
        (:participant_criteria_choices, Symbol("$criteria_short")),
        (:criteria_belief, Symbol("$(criteria_short)_prediction")),
        (:lottery_belief, :lottery_prediction),
    ]
    for (old, new) = newnames
        printcols[printcols .== old] .= new
    end    
    
    rps_pos = findall(:rps .== printcols)   
    lottery_pos = findall(:lottery .== printcols)
    arrival_pos = findall(:time .== printcols)
    painting_pos = findall(:painting .== printcols)
   # mech_pos = findall(:chosen_mechanism .== printcols)
    
    printcols = [replace(titlecase(replace(string(s), "_" => " ")), "Rps" => "RPS") for s = printcols]
    
    belief_pos = findall(occursin.("Prediction", printcols))
    payed_pos = findall(occursin.("Prediction Payed", printcols))
    setdiff!(belief_pos, payed_pos)
    
    f2 = (ft_printf("\$%2.2f", [1]))
    f3 = (v, i, j) -> print_belief(v, i, j, belief_pos)
    f4 = (v, i, j) -> print_dict(v, i, j, rps_pos, RPS_inv)
    f5 = (v, i, j) -> print_dict(v, i, j, lottery_pos, lottery)
    f6 = (v, i, j) -> print_vector(v, i, j, arrival_pos)
    f7 = (v, i, j) -> print_vector(v, i, j, painting_pos, ", ")
    f8 = (v, i, j) -> print_payed(v, i, j, payed_pos, criteria_short)
    #f9 = (v, i, j) -> print_mechanism(v, i, j, mech_pos, key[:participant_criteria]) ## May not be needed.
    
    
    open(joinpath("..", "Data", "Payments","Experiment2023ResultsExperiment$i.html"), "w") do f
        pretty_table(f, dfprint, 
            title ="Experiment $i", 
            header = printcols,
            title_alignment = :c, 
            backend = Val(:html),
            formatters = (f2, f3, f4, f5, f6, f7, f8),
            tf = tf_html_minimalist, 
            alignment = :c,
            row_names = df[!, :participant_code],
            row_name_alignment = :l,
            row_name_column_title = "Participant Code",
            standalone = false,
        )
    end
end

"""
    print_dict(v, dic::Dict = RPS_inv)

Format a vector using a dictionary to translate and then join.
"""
print_dict(v, dic::Dict = RPS_inv) = join(collect(dic[l] for l = v), ", ")

"""
    print_vector(v, i::Int, j::Int, k::Vector{Int} = [], dlm = ", ") 

Transform a vector into a string, separating elements using `dlm`.
"""
print_vector(v, dlm = ", ") = join(collect(l for l in v), dlm)

"""
    toCSV_saving(df::DataFrame, criteria::AbstractString="Rock, Paper, Scissors", i::Int=1, g::Int=1)

Export the result of each treatment into a CSV table.
"""
function toCSV_saving(df::DataFrame, criteria::AbstractString="Rock, Paper, Scissors", i::Int=1, g::Int=1)
    criteria_short = long_short_criteria_experiment2[criteria]
    if criteria_short == "rps"
        criteria_transformation = (x -> print_dict(x, RPS_inv))
    else # Should also work with paintings, but not tested yet
         criteria_transformation = (x -> print_vector(x))
    end
    df[!, [:criteria_belief, :lottery_belief]] .= @pipe df[!, [:criteria_belief, :lottery_belief]] |> ifelse.(_,  "Top half", "Bottom half")

    res = @pipe df |> select(_,
        :participant_code => :code,
        :participant_number => :number,
        :payment,
        :participant_lottery_choices => ByRow(x -> print_dict(x, lottery))=> :lottery_sequence,
        :participant_criteria_choices => ByRow(x -> criteria_transformation(x)) => "$(criteria_short)_sequence",
        :participant_mechanism => :your_choice, 
        :wta => :your_threshold,
        :lottery_ranks, 
        :criteria_ranks => "$(criteria_short)_ranks",
        :criteria_belief =>  "$(criteria_short)",
        :lottery_belief => :lottery,
        :participant_best_mechanism => :mechanism_preferred,
    ) 

    res |> CSV.write(joinpath("..", "Data", "Payments","2023ResultsExperiment$(i)Group$g.csv"))
    return res
end