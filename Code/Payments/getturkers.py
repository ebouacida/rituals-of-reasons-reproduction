import boto3
import pandas as pd
import re

MTURK_SANDBOX = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'
MTURK = 'https://mturk-requester.us-east-1.amazonaws.com'

## Loading logging informations
"""
Login information for the mturk session are saved in an .aws folder in the user folder.
The folder contains two files config and credentials.
The credentials contains the aws keys needed to access the account.
"""
session = boto3.Session(profile_name="elias")

mturk = session.client('mturk',
   endpoint_url = MTURK,
)

print("I have $" + mturk.get_account_balance()['AvailableBalance'] + " in my MTURK account")


hits = mturk.list_hits(MaxResults = 100)
all_hits = [h['HITId'] for h in hits['HITs']]
next_page = hits['NextToken']
while len(next_page) > 0:
    hits = mturk.list_hits(MaxResults = 100, NextToken=next_page)
    all_hits.extend([h['HITId'] for h in hits['HITs']])
    if 'NextToken' in hits:
        next_page = hits['NextToken']
    else:
        next_page = ''

        
    
df = pd.DataFrame(
    {'AssignmentId': [],
      'WorkerId': [],
      'HITId': [],
      'CompletionCode': [],
      'SubmitTime': [],}
     )

p = re.compile('completion_code":"(?P<completion>\w+)*')


for hit in all_hits:
    assignments = mturk.list_assignments_for_hit(HITId = hit, MaxResults = 100, AssignmentStatuses = ['Submitted'])
    if assignments['NumResults'] > 0:
        print(f"Number of results: {assignments['NumResults']}")
    for ass in assignments['Assignments']:
        m = p.search(ass['Answer'])
        df.loc[len(df.index)] = [ass['AssignmentId'],ass['WorkerId'], ass['HITId'], m.group('completion'), ass['SubmitTime'] ]
    
#df.to_csv("..//Data//Input//MTurkersRemaining.csv")
