# Starting the environment
import Pkg
Pkg.activate("Code")

# Importing the necessary libraries for the treatment
import CSV
using DataFrames
using Dates


# Loading the constants
include("Constants.jl")


# Loading the functions
include("CommonFunctions.jl")
include("PaymentFunctions.jl")

# Running the different cleaning data
include("Experiment1_ACleaningData.jl")
include("Experiment1_BCleaningData.jl")
include("Experiment1_CCleaningData.jl")
include("Experiment2CleaningData.jl")
