# %% [markdown]
#---
# title: "Experiment 1, session 3, cleaning the data"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     eval: false
# format: html
# jupyter:
#   kernelspec:
#     display_name: Julia 1.9.2
#     language: julia
#     name: julia-1.9
# ---

# %% [markdown]
# WARNING: The script does not run as the non-anonymized data is not available anymore.

# %%
import CSV
using DataFrames
using Dates
using Statistics
using Random: seed!, rand
seed!(1000)


# %%
include("Constants.jl")


# %%
include("PaymentFunctions.jl")
include("CommonFunctions.jl")


# %%
all_data = CSV.read(joinpath("..", "Data", "OriginalData", "all_apps_wide_2021-08-31.csv"), DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);

# %%
# Remove some useless columns
select!(all_data, Not(r"player_role$"))
select!(all_data, Not(r"group*"))
select!(all_data, Not(r"payoff*"))
select!(all_data, Not(r"mechanism$"));



# %%
# Loading the custom data
custom_data = CSV.read(joinpath("..", "Data", "OriginalData", "Feedback_2021-08-31.csv"), DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);



# %%
# Merging the custom and all data
data = innerjoin(all_data, custom_data, on = [:participant_code,:session_code]); 

# %% [markdown]
"""
Merging turker id in the same column

Separating players who reached the end of the experiment and the others
"""

# %%
nonfinished_data = data[ismissing.(data[!, :participant_current_page_name]) .| .!(data[!, :participant_current_page_name] .== "Results"), :];

# %%
data = data[.!ismissing.(data[!, :participant_current_page_name]).& (data[!, :participant_current_page_name] .== "Results"), :];

# %%
# Simplifying the names of the columns
apps = ["Algorithms", "Beliefs", "Mechanism", "Feedback", "Questionnaire"]
models = ["player", "subsession"]
for col = names(data)
    for app = apps, model = models
        m = match(Regex("$app") * r"_(?<round>\d)_" * Regex("$model") * r"_(?<column>\w+)", string(col))
        if !(m === nothing)
            #println(m)
            if (app != "Beliefs") & (m[:column] == "round_number")
                select!(data, Not(col))
            elseif (app == "Beliefs") 
                if (m[:column] == "criteria")
                    select!(data, Not(col))
                elseif m[:column] == "belief"
                    rename!(data, col => Symbol(m[:column] * "_" * m[:round]))
                end
            else
                rename!(data, col => Symbol(m[:column]))
            end
        end
    end
end




# %%
# Transform columns into their correct types
transform!(data, :criteria_first => (x -> Bool.(x)) => :criteria_first);
dformat = DateFormat("y-m-d H:M:S.s")
data[!, :participant_time_started_utc] = (x -> DateTime.(x[1:end-3], dformat)).(data[!, :participant_time_started_utc])
data[!, :criteria_choices] = criteriachoices.(data[!, :criteria_choices]);
transform!(data, :arrival_code => ByRow(x -> arrivalcode(x)) => :arrival_code)
for i = 1:5
    transform!(data, :arrival_code => ByRow(x -> parse(Int, string(x)[i])) => Symbol("arrival_code$i"))
end

# %%
# Tranform the lottery choices into their proper Bool representation
transform!(data, :lottery_choices => ByRow(x -> Int.(split(strip(x, [']', '[']), ", ") .== repeat(["True"], 5))) => :lottery_choices)


# %%
data[!, :mechanism] .= .!(data[!, :mechanism] .== "DC-5 Lottery");

# %% [markdown]
# Matching the belief with their respective mechanism

# %%
## Assigning the beliefs to the criteria or the lottery.
data[!, :criteria_belief] = falses(size(data, 1))
data[!, :lottery_belief] = falses(size(data, 1))
data[data[!, :criteria_first], :criteria_belief] = (data[data[!, :criteria_first], :belief_1] .== 1)
data[.!data[!, :criteria_first], :criteria_belief] = (data[.!data[!, :criteria_first], :belief_2] .== 1)
data[data[!, :criteria_first], :lottery_belief] = (data[data[!, :criteria_first], :belief_2] .== 1)
data[.!data[!, :criteria_first], :lottery_belief] = (data[.!data[!, :criteria_first], :belief_1] .== 1)
select!(data, Not([:belief_1, :belief_2]));

# %% [markdown]
# Computing the payments

# %%
include(joinpath("Payments", "SavingData.jl"))


# %%
data[!, :prediction_payed] = rand([:lottery, :criteria, :best_mechanism], size(data, 1))
data[!, :best_mechanism_winner] = missings(Bool, size(data, 1))
data[!, :payment] = data[!, :urn_winner] .* low_reward .+ participation_fee[2021]
for name = ["lottery", "criteria"]
    data[!, Symbol("$(name)_winner")] = missings(Bool, size(data, 1))
    data[!, Symbol("$(name)_ranks")] = zeros(Int, size(data, 1))
    data[!, Symbol("$(name)_score")] = missings(Int, size(data, 1))
    data[!, Symbol("$(name)_belief_winner")] = missings(Bool, size(data, 1))
end

# %% [markdown]
# Now working only on the split data.

# %%
sepdata = groupby(data, [:criteria, :criteria_control, :lottery_control])
for key = keys(sepdata)
    println("Criteria: ", key[:criteria], ", Control: ", Bool(key[:criteria_control]), ", Lottery Control:", Bool(key[:lottery_control]))
    winner(sepdata[key], x -> lotteryrank(x, dc5_3), "lottery")
    criteria_chosen = (mean(sepdata[key][!, :mechanism]) > 0.5)
    if criteria_chosen
        chosenmechanism = key[:criteria]
    else
        chosenmechanism = "DC-5 Lottery"
    end
    println("Mechanism chosen to attribute the reward: ", chosenmechanism)
    expe_number = 1 + key[:lottery_control] + 2*key[:criteria_control]
    if key[:criteria] == "Time"
        winner(sepdata[key], arrivaltimeranks, "criteria")
        expe_number += 8
    elseif key[:criteria] == "Rock, Paper, Scissors"
        winner(sepdata[key], totalrpsranks, "criteria")
    elseif key[:criteria] == "Guessing the Paintings"
        winner(sepdata[key], x -> paintingrank(x, elias3), "criteria")   
        expe_number += 4
    end
    
    comparativebeliefwinner(sepdata[key])
    beliefwinner(sepdata[key], "criteria")
    beliefwinner(sepdata[key], "lottery")
    for row = eachrow(sepdata[key])
        if criteria_chosen
            reward_mechanism = "criteria"
        else
            reward_mechanism = "lottery"
        end
        row[:payment] += high_reward[2021] * row[Symbol("$(reward_mechanism)_winner")] +
            low_reward * (row[:criteria_belief_winner] * (row[:prediction_payed] .== :criteria) + 
            (row[:prediction_payed] .== :best_mechanism) * row[:best_mechanism_winner] + (row[:prediction_payed] .== :lottery) * row[:lottery_belief_winner])         
      end
    if criteria_chosen
        datasaving(sort(sepdata[key], [:criteria_ranks, order(:payment, rev=true)]), key, expe_number)
    else
        datasaving(sort(sepdata[key], [:lottery_ranks, order(:payment, rev=true)]), key, expe_number)
    end

end
data[!, :payment] = round.(data[!, :payment], digits = 2);


# %%
mean(sepdata[3][!, :mechanism])


# %%
@assert length(unique(data[!, :participant_label])) == size(data, 1) "There are some nonunique workers"

# %% [markdown]
"""
# Loading workers

Load workers and match them with their performance in the experiment.
"""

# %%
workers = CSV.read(joinpath("..", "Data", "Payments", "MTurkersExperiment3.csv"), DataFrame, normalizenames=true, drop = [1], pool = false); # Ignore the first column which is purely an index.

# %%
# Transform the time in the right format
dformatworkers = DateFormat("y-m-d H:M:S")
workers[!, :SubmitTime] = (x -> DateTime.(x[1:end-6], dformatworkers)).(workers[!, :SubmitTime]);




# %%
# The number of worker which have not provided a completion code
sum(ismissing.(workers[!, :CompletionCode]))




# %%
# Restrict the data to the variables of interest for payments
payment = data[!, [:participant_label, :payment, :completion_code]]
rename!(payment, :participant_label => :WorkerId)
payment[!, :AssignmentId] = missings(String, size(payment, 1));
payment[!, :HITId] = missings(String, size(payment, 1));

# %% [markdown]
"""
Match the workers from the otree data to the workers id retrieved from MTurk.  
First match workers that are only present once in the workers data.
Then for those who are here more than once, look at the completion codes and match the worker to the first submitted data with the right completion code.
Finally, if the completion code is missing, match the first non missing completion code.
"""

# %%
for df = groupby(workers, :WorkerId)
    subpayment = payment[payment[!, :WorkerId] .== df[1, :WorkerId], :]
    if isempty(subpayment)
        continue
    elseif (size(df, 1) == 1)
        payment[payment[!, :WorkerId] .== df[1, :WorkerId], :AssignmentId] .= df[1, :AssignmentId]
        payment[payment[!, :WorkerId] .== df[1, :WorkerId], :HITId] .= df[1, :HITId]         
    else
        subdf = df[.!ismissing.(df[!, :CompletionCode]) .& (df[!, :CompletionCode] .== subpayment[1, :completion_code]), :]   
        if isempty(subdf)
            subdf = df[.!ismissing.(df[!, :CompletionCode]), :] 
            mintime = minimum(subdf[!, :SubmitTime])
            payment[payment[!, :WorkerId] .== subdf[1, :WorkerId], :AssignmentId] .= subdf[subdf[!, :SubmitTime] .== mintime, :AssignmentId]
            payment[payment[!, :WorkerId] .== subdf[1, :WorkerId], :HITId] .= subdf[subdf[!, :SubmitTime] .== mintime, :HITId]  
        else
            mintime = minimum(subdf[!, :SubmitTime])
            payment[payment[!, :WorkerId] .== subdf[1, :WorkerId], :AssignmentId] .= subdf[subdf[!, :SubmitTime] .== mintime, :AssignmentId]
            payment[payment[!, :WorkerId] .== subdf[1, :WorkerId], :HITId] .= subdf[subdf[!, :SubmitTime] .== mintime, :HITId]   
        end
    end
end


# %%
payment[ismissing.(payment[!, :AssignmentId]), :]

# %% [markdown]
# Three workers are in our experimental data, but are not found in the workers from MTurk

# %%
missingpaidworkers = payment[ismissing.(payment[!, :AssignmentId]), :WorkerId]
paidworkers = payment[.!ismissing.(payment[!, :AssignmentId]), :WorkerId];
nonpaidworkers = setdiff(workers[!, :WorkerId], paidworkers)
for worker = missingpaidworkers
    if in(worker, nonpaidworkers)
        println(payment[payment[!, :WorkerId] .== worker, :])
        println(workers[workers[!, :WorkerId] .== worker, :])
    end    
end

# %% [markdown]
# Workers that can be paid

# %%
topay = payment[.!ismissing.(payment[!, :AssignmentId]), :];

# %%
topay[!, :Bonus] .= round.(topay[!, :payment] .- 0.8, digits = 2);


# %%
topay |> CSV.write(joinpath("..", "Data", "Payments", "AssignmentsAcceptationExperiment3.csv"))

# %% [markdown]
# Transform the missing type in `:CompletionCode` to empty string, so that the code to find unpaid assignments works

# %%
workers[ismissing.(workers[!, :CompletionCode]), :CompletionCode] .= "";
workers[!, :CompletionCode] = String.(workers[!, :CompletionCode])


# %%
toreject = DataFrame()
for row = eachrow(workers)
    if !(in(row[:WorkerId], topay[!, :WorkerId]) & in(row[:AssignmentId], topay[!, :AssignmentId]))
        push!(toreject, row)
    end
end


# %%
toreject[!, :Comment] .= "Wrong completion or individual completion code found multiple times."


# %%
toreject |> CSV.write(joinpath("..", "Data", "Payments", "AssignmentRejectionExperiment3.csv"))


# %%
toreject

# %% [markdown]
# Granting Qualifications

# %%
DataFrame(WorkerId = unique(workers[!, :WorkerId]))  |> CSV.write(joinpath("..", "Data", "Payments", "QualificationExperiment3.csv"))



