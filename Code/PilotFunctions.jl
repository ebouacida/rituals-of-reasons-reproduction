using Statistics: mean
using HypothesisTests: FisherExactTest
using DataFrames: AbstractDataFrame

const n_profiles = 2500
const rpstoint = Dict("Rock" =>1, "Paper" => 2, "Scissors" => 3)
const rpstostring = Dict(1 => "Rock", 2 => "Paper", 3 => "Scissors")

"""
    translate(x::Union{String, Int}, d::Dict)

Use the dictionary `d` to transform a string or an Int value into another.
"""
translate(x::Union{String, Int}, d::Dict) = d[x]

"""
    translaterpstoint(x::String)

Use the dictionary `rpstoint` to transform the string RPS values into Int.
"""
translaterpstoint(x::String) = translate(x, rpstoint)

"""
    translaterpstostring(x::Int) 

Use the dictionary `rpstostring` to transform the string RPS values into Int.
"""
translaterpstostring(x::Int) = translate(x, rpstostring)

"""
    treatment(df::AbstractDataFrame, npage::Int)

Treat the time data to transform columns in rows.
Compute whether a subject has finished or not.
"""
function treatment(df::AbstractDataFrame, npage::Int)
    finished = false
    if sum(df[!, :page_index] .== npage) > 0
        finished = true
    end
    totaltime = sum(df[!, :seconds_on_page])
    result = DataFrame(finished = finished, time_in_experiment = totaltime)
    for i = 1:npage
        if !isempty(df[df[!, :page_index] .== i, :])
            result[!, Symbol("time_page_$i")] = df[df[!, :page_index] .== i, :seconds_on_page]
        else
            result[!, Symbol("time_page_$i")] .= missing
        end
    end
    return result
end


"""
    FisherTest(x::Union{BitArray, Array{Bool}}, y::Union{BitArray, Array{Bool}})

Simplification of the Fisher Test for binary outcomes.
"""
FisherTest(x::Union{BitArray, Array{Bool}}, y::Union{BitArray, Array{Bool}}) = FisherExactTest(sum(x), sum(y), sum(.!x), sum(.!y))
FisherTest(x::DataFrame, y::DataFrame, axiom::Symbol) = FisherTest(x[!, axiom], y[!, axiom])

"""
    treatmenteffect(data::AbstractDataFrame, treatment::Symbol, outcome::Symbol = :mechanism)

Looking at the effect of the variable `treatment` on the `outcome`  on the data `data`.
The `outcome` defaults to mechanism, as it is the most common variable we are looking for outcomes on the data.
Return also the p-value of the Fisher Test of equality between proportions.
"""
function treatmenteffect(data::AbstractDataFrame, treatment::Symbol, outcome::Symbol = :mechanism; digits::Int = 3)
    println("Looking at the effect of the variable $treatment on the outcome $outcome.")
    println("$treatment ", round(mean(data[data[!, treatment], outcome]), digits = 2))
    println("Not $treatment: ", round(mean(data[.!data[!, treatment], outcome]), digits = 2))    
    t = FisherTest(data[data[!, treatment], :], data[.!data[!, treatment], :], outcome)
    if round(pvalue(t), digits = digits) > 0
        println("P-value of the Fisher Exact Test of equality of proportions: ", round(pvalue(t), digits = digits))
    else
        println("P-value of the Fisher Exact Test of equality of proportions: <", round(10. ^-digits, digits = digits +1 ))
    end
end  

"""
    treatmenteffect(data::AbstractDataFrame, treatment::Symbol, threshold::Number, outcome::Symbol = :mechanism)

Looking at the effect of the variable `treatment` on the outcome  `outcome` on the data `data` when it is above or below the `threshold`.
The `outcome` defaults to mechanism, as it is the most common variable we are looking for outcomes on the data.
Return also the p-value of the Fisher Test of equality between proportions.
"""
function treatmenteffect(data::AbstractDataFrame, treatment::Symbol, threshold::Number, outcome::Symbol = :mechanism; digits::Int = 3)
    println("Looking at the effect of the variable $treatment being above or below the threshold $threshold on the outcome $outcome.")
    println("$treatment: ", round(mean(data[data[!, treatment] .> threshold, outcome]), digits = 2))
    println("Not $treatment: ", round(mean(data[data[!, treatment] .< threshold, outcome]), digits = 2))    
    t = FisherTest(data[data[!, treatment] .> threshold, :], data[data[!, treatment] .< threshold, :], outcome)
    if round(pvalue(t), digits = digits) > 0
        println("P-value of the Fisher Exact Test of equality of proportions: ", round(pvalue(t), digits = digits))
    else
        println("P-value of the Fisher Exact Test of equality of proportions: <", round(10. ^-digits, digits = digits + 1 ))
    end
end  


##############################
### Regression decoration

"""
    pstar(s::String, pval::Float64)

Add decoration to the regression coefficients (the *).
"""
function pstar(s::String, pval::Float64)
    if pval<0.0
        error("p value needs to be nonnegative.")
    elseif (pval > 0.1)
        return "$s"
    elseif (pval < 0.001)
        return "$s****"
    elseif (pval < 0.01)
        return "$s***"
    elseif (pval < 0.05)
        return "$s**"
    elseif (pval <= 0.1)
        return "$s*"
    end
end


allvar = ["knowledge", "ability", "rps_believed_really_better", "RPSbetter", "beliefRPSbetter", "mechanism"]
regressors = Dict(j => titlecase(j) for j in allvar)
regressors["rps_believed_really_better"] = "\$RPS \\succeq CT\$"
regressors["RPSbetter"] = "RPS really better"
regressors["beliefRPSbetter"] = "RPS believed better"
regressors["rps_minus_ct"] = "\$RPS - CT\$"
regressors["probability_driving_choice"] = "Probability"

"""
    transform_labels(s::String)

Transform the labels of the regressors in the regression, using the dictionary `d`.
If the regressor in not in `d`, it will not change it.
"""
function transform_labels(s::String, d::Dict{String, String} = regressors)
    lab = split(s, " & ")
    return join([haskey(d, i) ? d[i] : i for i in lab], " \\& ")
end