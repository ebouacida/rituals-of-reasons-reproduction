using DataFrames
using Statistics
using StatsBase




#=
"""
    majoritymechanism(df::AbstractDataFrame)

Return the mechanism selected by most subjects in the experiment.
"""
function majoritymechanism(df::AbstractDataFrame)
    c = countmap(df[!, :mechanism])
    ke = collect(keys(c))
    if length(ke) == 0
        return "No mechanism selected"
    elseif length(ke) == 1
        return ke[1]
    end
    if c[ke[1]] > c[ke[2]]
        return ke[1]
    elseif c[ke[1]] < c[ke[2]]
        return ke[2]
    else
        return df[df[!, :participant_time_started_utc] .== minimum(df[!, :participant_time_started_utc]), :mechanism][1]
    end
end
=#

###########
### Function common for all algorithms

"""
    matchscore(row::DataFrameRow, paintings::Union{Vector{Int}, BitVector}, colname::String)

Compute the score of a given combination of numbers against another one.
"""
function matchscore(row::DataFrameRow, paintings::Union{Vector{Int}, BitVector}, colname::String)
    res = 0
    for i = eachindex(paintings)
        res += (row[Symbol("$colname$i")] == paintings[i])
    end
    return res
end


#####################
## Functions for RPS ranking

"""
    winner(a1::Int, a2::Int)

Compute the score of action `a1` against action `a2`.
"""
function winner(a1::Int, a2::Int)
    if (a1 == 1) & (a2 == 3)
        return 1
    elseif (a2 == 1) & (a1 == 3)
        return -1
    elseif a1 > a2
        return 1
    elseif a1 == a2
        return 0
    elseif a1 < a2
        return -1
    else
        return DomainError(a1, "$a1 or $a2 do not have value 1, 2 or 3. Cannot compute a winner.")
    end
end

"""
    rpsscorescore(s1::AbstractArray{T}, s2::AbstractArray{T}) where T <: Int

Compute the score of the strategy profile `s1` against the strategy profile `s2`.
"""
function rpsscore(s1::AbstractArray{T}, s2::AbstractArray{T}) where T <: Int
    @assert size(s1) == size(s2) "The different strategies do not have the same dimension, and cannot be pitted against each other."
    res = zeros(Int, size(s1))
    for i = eachindex(s1)
        res[i] = winner(s1[i], s2[i])
    end
    return res
end

"""
    numberwin(s1::AbstractArray{T}, M::Vector{Vector{T}}) where T <: Int

Compute the number of wins of the strategy `s1` against all strategies in `M`.

`M` may or may not include `s1`, it is not an issue. 
Same strategies are counted as 0 anyway.
"""
function numberwin(s1::AbstractArray{T}, M::Vector{Vector{T}}) where T <: Int
    l = size(M, 2)
    p = 0 
    for i = eachindex(M)
        s = rpsscore(s1, M[i])
        if sum(s) > 0
            p += 1
        elseif sum(s) < 0
            p -= 1
        else
            for i = eachindex(s)
                if s[i] > 0
                    p += 1
                    break
                elseif s[i] < 0
                    p -= 1
                    break
                end
            end
        end
    end
    return p
end

"""
    rpsranks(df::AbstractDataFrame)

Compute the ranks for the RPS strategies of all players in `df`.
"""
function rpsranks(df::AbstractDataFrame, notfirst::Bool = true)
    col_code = :participant_code
    col_arrival_code = :arrival_code
    col_criteria_choices = :criteria_choices
    if !in("participant_code", names(df))
        col_code = :code
        col_arrival_code = :arrival_code
        col_criteria_choices = :criteria_choices
    end
    res = df[:, [col_code,  col_criteria_choices, :criteria_ranks, :criteria_score]]
    for i = 1:size(res, 1)
        res[i, :criteria_score] = numberwin(res[i, col_criteria_choices], Vector(res[!, col_criteria_choices]))
    end
    sort!(res, :criteria_score, rev = true)
    i = 1
    res[i, :criteria_ranks] = i
    while i < size(res[!, :criteria_ranks], 1)
        i += 1
        if res[i, :criteria_score] == res[i - 1, :criteria_score]
            res[i, :criteria_ranks] = res[i - 1, :criteria_ranks]
        else
            res[i, :criteria_ranks] = i
        end
    end 
    for row = eachrow(res)
        df[(df[!, col_code] .== row[col_code]), :criteria_ranks] .+= row[:criteria_ranks] - notfirst
        if !notfirst
            df[(df[!, col_code] .== row[col_code]), :criteria_score] .= row[:criteria_score]
        end
    end
    return df
end

"""
    totalrpsranks(df::AbstractDataFrame)

Compute the ranks for all RPS strategies, and break the ties using the RPS breaking ties.
Does not break ties between the same strategies.
"""
function totalrpsranks(df::AbstractDataFrame)
    rpsranks(df, false)
    previousranks = zeros(Int, size(df, 1))
    currentranks = unique(df[!, :criteria_ranks])
    while !(previousranks == currentranks)
        subdfs = groupby(df, :criteria_ranks)
        for subdf = subdfs
            if size(subdf, 1) > 1
                rpsranks(subdf)
            end
        end
        previousranks = currentranks
        currentranks = unique(df[!, :criteria_ranks])
    end
    return df
end

############################
### Functions for Arbitrary algorithm ranks

"""
    arrivaltimescore(x::String)

Compute the score for the arrival time.
"""
arrivaltimescore(x::String) = sum([iseven(parse(Int, i)) for i = x])

arrivaltimescore(x::Int) = sum([iseven(parse(Int, i)) for i = string(x)])



"""
    arrivaltimeranks(df::AbstractDataFrame, tiebreaking::Bool = false)

Rank all the subjects in the arbitrary treatment.
If `tiebreaking` is `true`, it is used only as tiebreaking for other treatments.
"""
function arrivaltimeranks(df::AbstractDataFrame, tiebreaking::Bool = false)
    col_code = :participant_code
    col_arrival_code = :arrival_code
    if !in("participant_code", names(df))
        col_code = :code
        col_arrival_code = :arrival_code
    end
    if tiebreaking
        name = "arrival"
    else
        name = "criteria"
    end
    ke = [Symbol("arrival_code$i") for i=1:5]
    append!(ke, [col_code, col_arrival_code, Symbol("$(name)_ranks")])
    res = df[:, ke]      
    res[!, :even] =  map(arrivaltimescore, res[!, col_arrival_code])
    sort!(res, :even) # No reverse here because what we count is the number of odds.
    i = 1
    res[i, Symbol("$(name)_ranks")] = i
    while i < size(res[!, Symbol("$(name)_ranks")], 1)
        i += 1
        if res[i, :even] == res[i - 1, :even]
            res[i, Symbol("$(name)_ranks")] = res[i - 1, Symbol("$(name)_ranks")]
        else
            res[i, Symbol("$(name)_ranks")] = i
        end
    end 
    i = 1
    while i < 6
        for subdf = groupby(res, Symbol("$(name)_ranks"))
            if size(subdf, 1) == 1
                continue
            end
            c = countmap(subdf[!, Symbol("arrival_code$i")])
            addrank = 0
            while !isempty(c)
                m = minimum(values(c))
                j  = 0
                for (key, v) = c
                    if v == m
                        subdf[(subdf[!, Symbol("arrival_code$i")] .== key), Symbol("$(name)_ranks")] .+= addrank    
                        j += 1
                        delete!(c, key)
                    end
                end
                addrank += m * j
            end                
        end
        i += 1
    end
    rankname = name
    if !tiebreaking & (median(res[!, :even]) < 2.5) # Creating the reverse ranking for the loser treatment
        sort!(res, Symbol("$(name)_ranks"), rev=true)
        res[!, :reverse_ranks] = zeros(Int, size(res, 1))
        i = 1
        res[i, :reverse_ranks] = i
        while i < size(df, 1)
            i += 1
            if res[i, Symbol("$(name)_ranks")] == res[i-1, Symbol("$(name)_ranks")]
                res[i, :reverse_ranks] = res[i-1, :reverse_ranks]
            else
                res[i, :reverse_ranks] = i
            end
        end
        rankname = "reverse"
    end
    for row = eachrow(res)
        df[df[!, col_code] .== row[col_code], Symbol("$(name)_ranks")] .= row[Symbol("$(rankname)_ranks")]
    end 
    return df
end


#############################
### Function for DC-5 lottery ranks


"""
    lotteryrank(df::AbstractDataFrame, dc5::BitVector = falses(5))

Compute the ranking of the according to the lottery guesses.
"""
function lotteryrank(df::AbstractDataFrame, dc5::BitVector = falses(5))
    ke = [Symbol("lottery_choice$i") for i=1:5]
    col_code = :participant_code
    if !in("participant_code", names(df))
        col_code = :code
    end
    res = DataFrame(df[!, append!([col_code, :lottery_score, :lottery_ranks], ke)])
    for row = eachrow(res)
        row[:lottery_score] = matchscore(row, dc5, "lottery_choice")
    end
    sort!(res, :lottery_score, rev = true)
    i = 1
    res[i, :lottery_ranks] = i
    while i < size(res, 1)
        i += 1
        if res[i, :lottery_score] == res[i-1, :lottery_score]
            res[i, :lottery_ranks] = res[i-1, :lottery_ranks]
        else
            res[i, :lottery_ranks] = i
        end
    end
    i = 1
    while i < 6
        subdfs = groupby(res, :lottery_ranks)
        for subdf = subdfs
            if size(subdf, 1) > 1
                correctguess = sum(subdf[!, Symbol("lottery_choice$i")] .== dc5[i])
                subdf[.!(subdf[!, Symbol("lottery_choice$i")] .== dc5[i]), :lottery_ranks] .+= correctguess
            end
        end
        i += 1
    end
    for row = eachrow(res)
        df[df[!, col_code] .== row[col_code], :lottery_score] .= row[:lottery_score]
        df[df[!, col_code] .== row[col_code], :lottery_ranks] .= row[:lottery_ranks]
    end 
    return df
end
        
############################
### Ranking the paintings

"""
    paintingrank(df::AbstractDataFrame, paintings::Vector{Int} = zeros(Int, 5))

Compute the ranking of the according to the painting guesses.
"""
function paintingrank(df::AbstractDataFrame, paintings::Vector{Int} = zeros(Int, 5))
    ke = [Symbol("criteria_choice$i") for i=1:5]
    res = DataFrame(df[!, append!([:participant_code, :criteria_choices, :criteria_ranks, :criteria_score], ke)])
    for row = eachrow(res)
        row[:criteria_score] = matchscore(row, paintings, "criteria_choice") ## Need to be changed.
    end
    sort!(res, :criteria_score, rev = true)
    i = 1
    res[i, :criteria_ranks] = i
    while i < size(res, 1)
        i += 1
        if res[i, :criteria_score] == res[i-1, :criteria_score]
            res[i, :criteria_ranks] = res[i-1, :criteria_ranks]
        else
            res[i, :criteria_ranks] = i
        end
    end
    i = 1
    while i < 6
        subdfs = groupby(res, :criteria_ranks)
        for subdf = subdfs
            if size(subdf, 1) > 1
                correctguess = sum(subdf[!, Symbol("criteria_choice$i")] .== paintings[i])
                subdf[.!(subdf[!, Symbol("criteria_choice$i")] .== paintings[i]), :criteria_ranks] .+= correctguess
            end
        end
        i += 1
    end
    for row = eachrow(res)
        df[df[!, :participant_code] .== row[:participant_code], :criteria_score] .= row[:criteria_score]
        df[df[!, :participant_code] .== row[:participant_code], :criteria_ranks] .= row[:criteria_ranks]
    end     
    return df
end

#########
## Computing the algorithms winners

"""
    winner(df::AbstractDataFrame, algorithm, name::String="lottery", winnertreatment::Bool = true)

Compute who is the winner acccording to each `algorithm`.
Use the `arrival_time_ranks` to break tie if necessary.

# Arguments

- `df` the original data
- `algorithm` the algorithm used to rank people.
- `name` the name we want to use in the data for the `algorithm`. Usually constrained by the algorithm.
- `winnertreatment` whether the ranking is reversed or not to determine the winners.
"""
function winner(df::AbstractDataFrame, algorithm, name::String="lottery", winnertreatment::Bool = true)
    col_code = :participant_code
    col_arrival_code = :arrival_code
    if !in("participant_code", names(df))
        col_code = :code
        col_arrival_code = :arrival_code
    end
    df = algorithm(df)
    if !winnertreatment # Creating the reverse ranking for the loser treatment
        tempdf = DataFrame(copy(df[!, [col_code, Symbol("$(name)_ranks")]]))
        sort!(tempdf, Symbol("$(name)_ranks"), rev=true)
        tempdf[!, :reverse_ranks] = zeros(Int, size(tempdf, 1))
        i = 1
        tempdf[i, :reverse_ranks] = i
        while i < size(df, 1)
            i += 1
            if tempdf[i, Symbol("$(name)_ranks")] == tempdf[i-1, Symbol("$(name)_ranks")]
                tempdf[i, :reverse_ranks] = tempdf[i-1, :reverse_ranks]
            else
                tempdf[i, :reverse_ranks] = i
            end
        end
        for row = eachrow(tempdf)
            df[df[!, col_code] .== row[col_code], Symbol("$(name)_ranks")] .= row[Symbol("$(name)_ranks")]
        end
    end
    n_winner = round(Int, size(df, 1) / 2, RoundUp)
    df[df[!, Symbol("$(name)_ranks")] .> n_winner , Symbol("$(name)_winner")] .= false
    # Computing the maximal rank of the winners (in case of large indifference classes)
    max_rank_winners = n_winner
    while sum(df[!, Symbol("$(name)_ranks")] .< max_rank_winners) > n_winner
        max_rank_winners -= 1
    end
    df[df[!, Symbol("$(name)_ranks")] .< max_rank_winners , Symbol("$(name)_winner")] .= true
    remaining = n_winner - sum(df[.!ismissing.(df[!, Symbol("$(name)_winner")]), Symbol("$(name)_winner")])
    println("Remaining winners to attribute: $remaining")
    println("Remaining subjects whose winning status has not been characterized: ", sum(ismissing.(df[!, Symbol("$(name)_winner")])))
    
    # Assessing whether tie-breaking is needed or not.
    if sum(ismissing.(df[!, Symbol("$(name)_winner")])) == remaining
        df[ismissing.(df[!, Symbol("$(name)_winner")]), Symbol("$(name)_winner")] .= true
    else
        println("Entering tie-breaking in for mechanism $name")
        ke = [Symbol("arrival_code$i") for i=1:5]
        append!(ke, [col_code,  col_arrival_code])
        subdf = copy(df[ismissing.(df[!, Symbol("$(name)_winner")]), ke])
        subdf[!, :arrival_ranks] = zeros(Int, size(subdf, 1))
        subdf = arrivaltimeranks(subdf, true)
        winners = subdf[subdf[!, :arrival_ranks] .<= remaining, col_code]
        for win = winners
            df[df[!, col_code] .== win, Symbol("$(name)_winner")] .= true
        end
        df[ismissing.(df[!, Symbol("$(name)_winner")]), Symbol("$(name)_winner")] .= false
    end
    if sum(df[!, Symbol("$(name)_winner")]) > n_winner
        println("There are more winner than normally should be the case. It may be a normal behavior.")
    end
    @assert sum(ismissing.(df[!, Symbol("$(name)_winner")])) == 0 "Some subjects have uncharacterized winning status for mechanisms $name, in the $winnertreatment case"
    return df
end


#### 
# Rewarding the beliefs

"""
    beliefwinner(df::AbstractDataFrame, criteria::String)

Compute for each subject whether their belief were correct.

# Arguments

- `criteria` is one of lottery or criteria
- `algorithm` is one of rps, arrival, painting or lottery
"""
function beliefwinner(df::AbstractDataFrame, criteria::String)
    middle = round.(Int, size(df, 1) /2, RoundUp)
    df[(df[!, Symbol("$(criteria)_ranks")] .>= middle) .& .!df[!, Symbol("$(criteria)_belief")], Symbol("$(criteria)_belief_winner")] .= true
    df[(df[!, Symbol("$(criteria)_ranks")] .> middle) .& df[!, Symbol("$(criteria)_belief")], Symbol("$(criteria)_belief_winner")] .= false
    df[(df[!, Symbol("$(criteria)_ranks")] .<= middle) .& df[!, Symbol("$(criteria)_belief")], Symbol("$(criteria)_belief_winner")] .= true
    df[(df[!, Symbol("$(criteria)_ranks")] .< middle) .& .!df[!, Symbol("$(criteria)_belief")], Symbol("$(criteria)_belief_winner")] .= false
    return df
end

"""
    comparativebeliefwinner(df::AbstractDataFrame)

Rewards the comparative beliefs
"""
function comparativebeliefwinner(df::AbstractDataFrame)
    df[(df[!, :criteria_ranks] .<= df[!, :lottery_ranks]) .& .!(df[!, :best_mechanism] .== "DC-5 Lottery"), :best_mechanism_winner] .= true
    df[(df[!, :criteria_ranks] .>= df[!, :lottery_ranks]) .& (df[!, :best_mechanism] .== "DC-5 Lottery"), :best_mechanism_winner] .= true
    df[(df[!, :criteria_ranks] .< df[!, :lottery_ranks]) .& (df[!, :best_mechanism] .== "DC-5 Lottery"), :best_mechanism_winner] .= false
    df[(df[!, :criteria_ranks] .> df[!, :lottery_ranks]) .& .!(df[!, :best_mechanism] .== "DC-5 Lottery"), :best_mechanism_winner] .= false  
    return df
end


"""
    create_groups(df::AbstractDataFrame)

Create groups with only one subject of a given number for the payment in the experiment.
"""
function create_groups(df::AbstractDataFrame, number_row::Symbol = :number)
    df.group = zeros(Int, size(df, 1))
    for row = eachrow(df)
        group = 1
        while in(group, df[df[!, number_row] .== row[number_row], :group])
            group +=1
        end
        row.group = group
    end
    return df
end