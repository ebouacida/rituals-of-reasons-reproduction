from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import ssl
import getpass
import pandas as pd
import os.path

def create_message(amount, code, receiver_email, expe=1, sender_email="elias.bouacida@univ-paris8.fr"):
    message = MIMEMultipart("alternative")
    message["Subject"] = "Experiment payment from Prolific"
    message["From"] = sender_email
    message["To"] = receiver_email
    
    # Create the plain-text and HTML version of your message
    text = f"""\
    Dear Participant,
    On August 14, you participated in an experiment on Prolific conducted by Renaud Foucart and me.
    Your payment in this experiment is ${amount}.
    If you have not already, you should receive your payment soon.
    You can see the results of everyone on the website https://eliasbcd.github.io/experiment/experiment{expe}.html.
    Your code is {code}.
    Thank you very much!
    Elias Bouacida
    """
    html = f"""\
    <html>
      <body>
        <p>Dear Participant,<br/>
            On August 14, you participated in an experiment on Prolific conducted by Renaud Foucart and me.
            Your payment in this experiment is ${amount}.
            If you have not already, you should receive your payment soon.
            You can see the results of everyone on this <a href="https://eliasbcd.github.io/experiment/experiment{expe}.html">website</a>.
            Your code is {code}.
    Thank you very much!</p>
    <p>Elias Bouacida
        </p>
      </body>
    </html>
    """
    
    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)
    return message.as_string()

smtp_server = "smtp.univ-paris8.fr"
port = 587
sender_email = "elias.bouacida@univ-paris8.fr"
password = getpass.getpass(prompt="Type your password and press enter: ")

data = pd.read_csv(os.path.join("..", "Data", "Payments", "2023ProlificExperiment_Mails.csv"))

# Create a secure SSL context
context = ssl.create_default_context()

# Try to log in to server and send email
try:
    # Try to log in to server and send email
    server = SMTP(smtp_server,port)
    server.ehlo() # Can be omitted
    server.starttls(context=context) # Secure the connection
    server.ehlo() # Can be omitted
    server.login(sender_email, password)
    for i, row in data.iterrows():
        receiver_mail = f"""{row["participant_label"]}@email.prolific.co"""
        server.sendmail(
            sender_email, receiver_mail, create_message(row["payment"], row["participant_code"], receiver_mail, expe=row["expe"])
        )
except Exception as e:
    # Print any error messages to stdout
    print(e)
finally:
    server.quit() 
  