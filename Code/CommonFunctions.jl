using Statistics: mean
using HypothesisTests: FisherExactTest
using DataFrames: AbstractDataFrame


"""
    translate(vec::Vector, dic::Dict)

Translate the element of the vector using the `dic`.
"""
translate(vec::Vector, dic::Dict) = (x -> dic[x]).(vec)

"""
    arrivalcode(x::Int)

Transform the arrival code into the right format, in case it is not.
"""
function arrivalcode(x::Int)
    res = string(x)
    while length(res) <5
        res = "0" * res
    end
    return res
end

"""
    translate(x::Union{String, Int}, d::Dict)

Use the dictionary `d` to transform a string or an Int value into another.
"""
translate(x::Union{String, Int}, d::Dict) = d[x]

"""
    translaterpstoint(x::String)

Use the dictionary `rpstoint` to transform the string RPS values into Int.
"""
translaterpstoint(x::String) = translate(x, rpstoint)

"""
    translaterpstostring(x::Int) 

Use the dictionary `rpstostring` to transform the string RPS values into Int.
"""
translaterpstostring(x::Int) = translate(x, rpstostring)

"""
    missingtime(x::Int)

Transforms -1 in time spent into missing, as -1 means the value is missing.
"""
function missingtime(x::Int)
    if x == -1
        return missing
    end
    return x
end


"""
    criteriachoices(str::String)

Transform the criteria choices into numbers.

Needed because Julia treat the vector as a string.
"""
criteriachoices(str::AbstractString) = parse.(Int, split(strip(str, ['[', ']']), ','))


"""
    treatment(df::AbstractDataFrame, npage::Int)

Treat the time data to transform columns in rows.
Compute whether a subject has finished or not.
"""
function treatment(df::AbstractDataFrame; lastpage::Int=0, pages::Vector{Int} = collect(0:lastpage))
    finished = false
    lastpage = maximum(pages)
    if sum(df[!, :page_index] .== lastpage) > 0
        finished = true
    end
    if finished
        totaltime = (df[df[!, :page_index] .== lastpage, :epoch_time_completed] - df[df[!, :page_index] .== 0, :epoch_time_completed])
    else
        totaltime = -1
    end
    result = DataFrame(finished = finished, time_in_experiment = totaltime)
    for i = eachindex(pages[1:(end-1)]) 
        if !isempty(df[df[!, :page_index] .== pages[i+1], :]) .& !isempty(df[df[!, :page_index] .== pages[i], :])
            #@show (df[df[!, :page_index] .== i, :epoch_time_completed] .- df[df[!, :page_index] .== (i-1), :epoch_time_completed])
            result[!, Symbol("time_page_$i")] .= (df[df[!, :page_index] .== pages[i+1], :epoch_time_completed] .- df[df[!, :page_index] .== pages[i], :epoch_time_completed])
        else
            result[!, Symbol("time_page_$i")] .= -1
        end
    end
    return result
end


"""
    FisherTest(x::Union{BitArray, Array{Bool}}, y::Union{BitArray, Array{Bool}})

Simplification of the Fisher Test for binary outcomes.
"""
FisherTest(x::Union{BitArray, Array{Bool}}, y::Union{BitArray, Array{Bool}}) = FisherExactTest(sum(x), sum(y), sum(.!x), sum(.!y))
FisherTest(x::DataFrame, y::DataFrame, axiom::Symbol) = FisherTest(x[!, axiom], y[!, axiom])

"""

    percentage(x::Any, digit::Int = 0)

Report the result in percentages rather than with the mean, with `digit` decimal numbers.
"""
percentage(x::Any, digit::Int = 0) = round(mean(x) * 100, digits = digit) 

"""
    treatmenteffect(data::AbstractDataFrame, treatment::Symbol, outcome::Symbol = :mechanism)

Looking at the effect of the variable `treatment` on the `outcome`  on the data `data`.
The `outcome` defaults to mechanism, as it is the most common variable we are looking for outcomes on the data.
Return also the p-value of the Fisher Test of equality between proportions.
"""
function treatmenteffect(data::AbstractDataFrame, treatment::Symbol, outcome::Symbol = :mechanism; digits::Int = 3)
    println("Looking at the effect of the variable $treatment on the outcome $outcome.")
    println("$treatment ", percentage(data[data[!, treatment], outcome], 2))
    println("Not $treatment: ", percentage(data[.!data[!, treatment], outcome], 2))    
    t = FisherTest(data[data[!, treatment], :], data[.!data[!, treatment], :], outcome)
    if round(pvalue(t), digits = digits) > 0
        println("P-value of the Fisher Exact Test of equality of proportions: ", round(pvalue(t), digits = digits))
    else
        println("P-value of the Fisher Exact Test of equality of proportions: <", round(10. ^-digits, digits = digits +1 ))
    end
end  



"""
    treatmenteffect(data::AbstractDataFrame, treatment::Symbol, threshold::Number, outcome::Symbol = :mechanism)

Looking at the effect of the variable `treatment` on the outcome  `outcome` on the data `data` when it is above or below the `threshold`.
The `outcome` defaults to mechanism, as it is the most common variable we are looking for outcomes on the data.
Return also the p-value of the Fisher Test of equality between proportions.
"""
function treatmenteffect(data::AbstractDataFrame, treatment::Symbol, threshold::Number, outcome::Symbol = :mechanism; digits::Int = 3)
    println("Looking at the effect of the variable $treatment being above or below the threshold $threshold on the outcome $outcome.")
    println("$treatment: ", percentage(data[data[!, treatment] .> threshold, outcome], 2))
    println("Not $treatment: ", percentage(data[data[!, treatment] .< threshold, outcome], 2))    
    t = FisherTest(data[data[!, treatment] .> threshold, :], data[data[!, treatment] .< threshold, :], outcome)
    if round(pvalue(t), digits = digits) > 0
        println("P-value of the Fisher Exact Test of equality of proportions: ", round(pvalue(t), digits = digits))
    else
        println("P-value of the Fisher Exact Test of equality of proportions: <", round(10. ^-digits, digits = digits + 1 ))
    end
end  


##############################
### Regression decoration

"""
    pstar(s::String, pval::Float64)

Add decoration to the regression coefficients (the *).
"""
function pstar(s::String, pval::Float64; aer::Bool = false)
    if aer
        return "$s"
    elseif pval<0.0
        error("p value needs to be nonnegative.")
    elseif (pval > 0.1)
        return "$s"
    elseif (pval < 0.001)
        return "$s****"
    elseif (pval < 0.01)
        return "$s***"
    elseif (pval < 0.05)
        return "$s**"
    elseif (pval <= 0.1)
        return "$s*"
    end
end



"""
    transform_labels(s::String)

Transform the labels of the regressors in the regression, using the dictionary `d`.
If the regressor in not in `d`, it will not change it.
"""
function transform_labels(s::String, escape = true, d::Dict{String, String} = regressors)
    lab = split(s, " & ")
    if escape
        return join([haskey(d, i) ? d[i] : i for i in lab], " \\& ")
    else
        return join([haskey(d, i) ? d[i] : i for i in lab], " & ")
    end
end

#######################
### Formatting functions 

"""
    percent_format(digit = 0)

Format a number in pretty table as percentage, with `digit` decimals.

`applicable(round, v)` check that the value in the table is roundable (i.e., is a number)
"""
function percent_format(digit = 0)
    return (v, i, j) -> begin
        if applicable(round, v)
            if digit == 0
                return "$(round(Int, 100 * v))%"
            else
                return "$(round(100 * v, digits = digit))%"
            end
        else
            return v
        end
    end    
end

"""
    pvalue_format()

Formatting p-values in pretty table.
"""
function pvalue_format()
    return (v, i, j) -> begin
        if v < 0.001
            return "<0.001"
        else
            return round(v, digits=3)
        end
    end
end