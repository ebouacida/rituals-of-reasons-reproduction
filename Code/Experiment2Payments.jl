# %% [markdown]
#---
# title: "Experiment 2, Computing Payments"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     eval: false
# format: html
# jupyter: julia-1.10
# ---

# %% [markdown]
# WARNING: The script does not run as the non-anonymized data is not available anymore.

# %%
import CSV
using DataFrames
using Dates
using Statistics
const conv_rate_USD_GBP = 0.784007
const fee_payed = 1.66 / conv_rate_USD_GBP

# %%
include("Constants.jl")

# %%
include("PaymentFunctions.jl")
include("CommonFunctions.jl")


# %%
data = CSV.read(joinpath("..", "Data", "OriginalData", "all_apps_wide_2023-08-15.csv"), DataFrame, normalizenames=true, truestrings = ["true", "True", "1"], falsestrings = ["false", "False", "0"], pool = false);

# %%
# Remove some useless columns
select!(data, Not(r"player_role$"))
select!(data, Not(r"group*"))
select!(data, Not(r"payoff*"));
#select!(all_data, Not(r"mechanism$"));

# %% [markdown]
"""
Loading the custom data

Merging the custom and all data

Separating players who reached the end of the experiment and the others
"""

# %%
nonfinished_data = data[ismissing.(data[!, :participant_current_page_name]) .| .!(data[!, :participant_current_page_name] .== "Results"), :];


# %%
data = data[.!ismissing.(data[!, :participant_current_page_name]) .& (data[!, :participant_current_page_name] .== "Results"), :];

# %% [markdown]
# Simplifying the names of the columns

# %%
apps = ["Algorithms", "Beliefs", "Mechanism", "Feedback", "Questionnaire"]
models = ["player", "subsession"]
for col = names(data)
    for app = apps, model = models
        m = match(Regex("$app") * r"_(?<round>\d)_" * Regex("$model") * r"_(?<column>\w+)", string(col))
        if !(m === nothing)
            #println(m)
            if (app != "Beliefs") & (m[:column] == "round_number")
                select!(data, Not(col))
            elseif (app == "Beliefs") 
                if (m[:column] == "criteria")
                    select!(data, Not(col))
                elseif m[:column] == "belief"
                    rename!(data, col => Symbol(m[:column] * "_" * m[:round]))
                end
            else
                rename!(data, col => Symbol(m[:column]))
            end
        end
    end
end

# %% [markdown]
# Transform columns into their correct types

# %%
rename!(data, :participant_criteria_first => :participant_criteria_first)
transform!(data, :participant_criteria_first => (x -> Bool.(x)) => :participant_criteria_first);
dformat = DateFormat("y-m-d H:M:S.s")
data[!, :participant_time_started_utc] = (x -> DateTime.(x[1:end-3], dformat)).(data[!, :participant_time_started_utc])
data[!, :participant_criteria_choices] = criteriachoices.(data[!, :participant_criteria_choices]);
transform!(data, :participant_arrival_code => ByRow(x -> arrivalcode(x)) => :participant_arrival_code)
for i = 1:5
    transform!(data, :participant_arrival_code => ByRow(x -> parse(Int, string(x)[i])) => Symbol("arrival_code$i"))
end

# %%
# Tranform the lottery choices into their proper Bool representation
transform!(data, :participant_lottery_choices => ByRow(x -> Int.(split(strip(x, [']', '[']), ", ") .== repeat(["True"], 5))) => :participant_lottery_choices);

# %% [markdown]
# Matching the belief with their respective mechanism

# %%
## Assigning the beliefs to the criteria or the lottery.
data[!, :criteria_belief] = falses(size(data, 1))
data[!, :lottery_belief] = falses(size(data, 1))
data[data[!, :participant_criteria_first], :criteria_belief] = (data[data[!, :participant_criteria_first], :belief_1] .== 1)
data[.!data[!, :participant_criteria_first], :criteria_belief] = (data[.!data[!, :participant_criteria_first], :belief_2] .== 1)
data[data[!, :participant_criteria_first], :lottery_belief] = (data[data[!, :participant_criteria_first], :belief_2] .== 1)
data[.!data[!, :participant_criteria_first], :lottery_belief] = (data[.!data[!, :participant_criteria_first], :belief_1] .== 1);

# %% [markdown]
# Computing the payments

# %%
include(joinpath("Payments", "SavingData.jl"))


# %%
data[!, [:participant_number, :participant_mechanism, :participant_criteria, :wta]]


# %%
selected_belief_question = dc4_2023 % 3 + 1
data.prediction_payed .= :best_mechanism
if selected_belief_question == 1
    data[data[!, :participant_criteria_first], :prediction_payed] .= :criteria
    data[.!data[!, :participant_criteria_first], :prediction_payed] .= :lottery
elseif selected_belief_question == 2
    data[.!data[!, :participant_criteria_first], :prediction_payed] .= :criteria
    data[data[!, :participant_criteria_first], :prediction_payed] .= :lottery  
end
data[!, :best_mechanism_winner] = missings(Bool, size(data, 1))
data[!, :payment] .= participation_fee[2023]
data[!, :bonus] .= 0.
for name = ["lottery", "criteria"]
    data[!, Symbol("$(name)_winner")] = missings(Bool, size(data, 1))
    data[!, Symbol("$(name)_ranks")] = zeros(Int, size(data, 1))
    data[!, Symbol("$(name)_score")] = missings(Int, size(data, 1))
    data[!, Symbol("$(name)_belief_winner")] = missings(Bool, size(data, 1))
end


# %%
# Separating the participants in different groups by participant_number.
transform!(groupby(data, :participant_criteria), create_groups); 

# %% [markdown]
# Computing the payments.

# %%
# Separating the data by criteria type and group.
sepdata = groupby(data, [:participant_criteria, :group])
for key = keys(sepdata)
    println("Criteria: ", key[:participant_criteria], ", Group: ", key[:group])
    winner(sepdata[key], x -> lotteryrank(x, dc5_2023), "lottery")
    participant_used_number = dc2_2023
    chosenmechanism = sepdata[key][sepdata[key][!, :participant_number] .== participant_used_number, :participant_mechanism]
    while isempty(chosenmechanism)
        if participant_used_number == 99
            participant_used_number = 0
        else
            participant_used_number += 1
        end
        chosenmechanism = (sepdata[key][sepdata[key][!, :participant_number] .== participant_used_number, :participant_mechanism])
    end
    chosenmechanism = chosenmechanism[1]
    chosen_wta = (sepdata[key][sepdata[key][!, :participant_number] .== participant_used_number, :wta])
    if  parse(Int, string(dc3_2023)[1]) > 4 # If the number drawn is 5 or above, the second question is used for payment.
        drawn_threshold > wta # Have to add that to the winner
        sepdata[key][sepdata[key][!, :participant_number] .== participant_used_number, :payment] += drawn_threshold
        if chosenmechanism == "DC-5 Lottery"
            chosenmechanism = sepdata[key][sepdata[key][!, :participant_number] == participant_used_number, :participant_mechanism][1]
        else
            chosenmechanism = "DC-5 Lottery"
        end
    end         
    println("Mechanism chosen to attribute the reward: ", chosenmechanism)
    expe_number = 1
    if key[:participant_criteria] == "Time"
        winner(sepdata[key], arrivaltimeranks, "criteria")
        expe_number = 2
    elseif key[:participant_criteria] == "Rock, Paper, Scissors"
        winner(sepdata[key], totalrpsranks, "criteria")
    elseif key[:participant_criteria] == "Guessing the Paintings"
        winner(sepdata[key], x -> paintingrank(x, elias3), "criteria")   
    end
    
    comparativebeliefwinner(sepdata[key])
    beliefwinner(sepdata[key], "criteria")
    beliefwinner(sepdata[key], "lottery")
    for row = eachrow(sepdata[key])
        if chosenmechanism == "DC-5 Lottery"
            reward_mechanism = "lottery"
        else
            reward_mechanism = "criteria"
        end
        row[:payment] += high_reward[2023] * row[Symbol("$(reward_mechanism)_winner")] +
            low_reward * (row[:criteria_belief_winner] * (row[:prediction_payed] .== :criteria) + 
            (row[:prediction_payed] .== :best_mechanism) * row[:best_mechanism_winner] + (row[:prediction_payed] .== :lottery) * row[:lottery_belief_winner])       
    end

    ## Computing only the bonus payment compared to what has already been payed on Prolific.
    sepdata[key].bonus .= round.((sepdata[key].payment .- fee_payed) .* conv_rate_USD_GBP, digits = 2)
    sepdata[key][sepdata[key].bonus .< 0, :bonus] .= 0.
    sepdata[key].payment .= round.(sepdata[key].payment, digits = 2)

    
    if chosenmechanism == "DC-5 Lottery"
        sorted_data = sort(sepdata[key], [:lottery_ranks, order(:payment, rev=true)])
    else
        sorted_data = sort(sepdata[key], [:criteria_ranks, order(:payment, rev=true)])
    end

    
        # datasaving(sorted_data, key, expe_number)
    toCSV_saving(sorted_data, key[:participant_criteria], expe_number, key[:group]) 
    sepdata[key].expe .= expe_number
    
end


# %%
# Saving the data for the emails and the website
data[data.bonus .> 0., [:participant_label, :bonus]] |> CSV.write(joinpath("..", "Data", "Payments", "2023ProlificExperiment_Amounts.csv"), writeheader=false)
data[!, [:participant_label, :payment, :expe, :participant_code]] |> CSV.write(joinpath("..", "Data", "Payments", "2023ProlificExperiment_Mails.csv"));


# %%
@assert length(unique(data[!, :participant_label])) == size(data, 1) "There are some nonunique workers"

# %% [markdown]
"""
## Looking at the Edge cases

Treating the data for participants who are not approved directly from the process above. Data Downloaded the 16/08/23 at 4pm Paris time.
"""

# %%
await_review = CSV.read(joinpath("..", "Data", "OriginalData", "ProlificAwaitReview.csv"), DataFrame)
timed_out = CSV.read(joinpath("..", "Data", "OriginalData", "ProlificTimedOut.csv"), DataFrame) 
mistakenly_returned = CSV.read(joinpath("..", "Data", "OriginalData", "ProlificReturned.csv"), DataFrame);


# %%
for part = await_review.participant_label
    println(in(part, data.participant_label))
end


# %%
for part = timed_out.participant_label
    println(in(part, data.participant_label))
end


# %%
for part = mistakenly_returned.participant_label
    println(in(part, data.participant_label))
end


