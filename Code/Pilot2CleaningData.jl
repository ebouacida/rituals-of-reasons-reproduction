# %% [markdown]
#---
# title: "Pilot 2, cleaning the data"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     echo: true
#     warning: false
# format: html
# jupyter:
#   kernelspec:
#     display_name: Julia 1.10.5
#     language: julia
#     name: julia-1.10
# ---

# %% [markdown]
# Clean the raw data file, and lays out the assumption behind the cleaning.

# %%
import CSV
using DataFrames

# %%
# Functions
include("PilotFunctions.jl");




# %%
# Constants
const n_pages = 8;

# %% [markdown]
"""
# Reading the Data

## Time Spent

Read the table where the time spent on each page is kept.
Then apply the function `treatment` to each subject.
"""

# %%
timespent = CSV.read(joinpath("..", "Data", "OriginalData", "TimeSpent (accessed 2020-05-14).csv"), DataFrame, normalizenames = true)
timedata = combine(groupby(timespent, :participant_code), x->treatment(x, n_pages - 1));

# %% [markdown]
"""
## Raw Data

Read the raw data (after the anonymization)
"""

# %%
rawdata = CSV.read(joinpath("..", "Data", "OriginalData", "Pilot2RawData.csv"), DataFrame, normalizenames=true, stringtype=String);

# %%
# Aggregate Time and Raw Data
data = innerjoin(rawdata, timedata, on = :participant_code);

# %% [markdown]
"""
# Cleaning the Data

## Finishing the Experiments

Removed subjects who have not finished the experiment

Check that the raw data and time date coincide on who finished the experiment.

Then keep only subjects who have finished the experiment, and remove the finished column as a consequence.
"""

# %%
@assert (data[!, :participant_index_in_pages] .== n_pages) == data[!, :finished] "The method used to determine who has finished is not correct"
data = data[data[!, :finished], :]
select!(data, Not([:finished])); 

# %% [markdown]
"""
## Selecting a subset of columns

We keep on the columns from the following `relevantcolumns` variable.
"""

# %%
const relevantcolumns = [:participant_code, :participant_payoff_plus_participation_fee, :participant_payoff, :session_code, :survey_1_player_age, :survey_1_player_gender_rev, :survey_1_player_gender,
    :survey_1_player_employment, :survey_1_player_region, :survey_1_player_rps_round1, :survey_1_player_rps_round2, :survey_1_player_rps_round3,
    :survey_1_player_rps_round4, :survey_1_player_rps_round5, :survey_1_player_total_time, :survey_1_player_probability, :survey_1_player_score, 
    :survey_1_player_ct_round1, :survey_1_player_ct_round2, :survey_1_player_ct_round3, :survey_1_player_ct_round4, :survey_1_player_ct_round5, 
    :survey_1_player_rps_first, :survey_1_player_mechanism_rev, :survey_1_player_mechanism, :survey_1_player_mechanism_winner, :survey_1_player_rps_belief, 
    :survey_1_player_ct_belief, :survey_1_player_rps_belief_first, :survey_1_player_belief1_winner, :survey_1_player_belief2_winner,
    :survey_1_player_belief_example1, :survey_1_player_belief_example2, :survey_1_player_belief_example3, :survey_1_player_reasons,
    :time_in_experiment, :time_page_1, :time_page_2, :time_page_3, :time_page_4, :time_page_5, :time_page_6, :time_page_7];


# %%
mean(data[!, :participant_payoff_plus_participation_fee])


# %%
irrelevantcolumns = [col for col = names(data) if !in(col, relevantcolumns)]
select!(data, relevantcolumns)
irrelevantcolumns

# %% [markdown]
## Striping the column names from `survey_1_player_`

# %%
for col = names(data)
    m = match(r"survey_1_player_(?<column>\w+)", string(col))
    if !(m === nothing)
        rename!(data, col => Symbol(m[:column]))
    end
end

# %% [markdown]
"""
## Transform Columns Types

Transform some string columns with only two possibilities into boolean columns.
"""

# %%
booleancolumns = [:gender_rev, :mechanism_winner, :belief1_winner, :belief2_winner, :mechanism_rev, :rps_first, :rps_belief_first]
for col = booleancolumns
    data[!, col] = (data[!, col] .== 1)
end

# %% [markdown]
"""
Transform the mechanism column into a boolean. 
Value of 1 if RPS, 0 if Coin Toss.
"""

# %%
data[!, :mechanism] = (data[!, :mechanism] .== "Rock, Paper, Scissors");

# %% [markdown]
# Transform the `gender` variable: Male is 0, Female is 1.

# %%
@assert sum(occursin.("ale", data[!, :gender])) == nrow(data) "Not everyone has answered the Gender question with Male or Female"

# %% [markdown]
# Everyone has answered to the Gender question with either Male or Female.

# %%
data[!, :female] = (data[!, :gender] .== "Female");
data[!, :male] = (data[!, :gender] .== "Male");
select!(data, Not(:gender));

# %% [markdown]
"""
Transform the coin toss choices into boolean (1 for Head, 0 for Tail).
Transform the different RPS rounds choice into numbers. 
The translation is Rock = 1, Paper = 2, Scissors = 3
"""

# %%
for i = 1:5
    data[!, Symbol("ct_round$i")] = (data[!, Symbol("ct_round$i")] .== "Head")
    data[!, Symbol("rps_round$(i)")] = translaterpstoint.(data[!, Symbol("rps_round$i")])
end

# %%
# Create a boolean telling whether RPS was better than Coin Toss or not.
data[!, :RPSbetter] = (data[!, :probability] .> 0.5);

# %%
# Compute the observed probability of winning the mechanism of each subject.
data[!, :probability_winning] .= 0.5
data[data[!, :mechanism], :probability_winning] .= data[data[!, :mechanism], :probability];

# %% [markdown]
"""
Dummy that give when subjects have a higher belief of winning with RPS than with CT.
Then the value of the difference between their beliefs.
"""

# %%
data[!, :rps_better_ct_belief] = (data[!, :rps_belief] .> data[!, :ct_belief]);


# %%
data[!, :rps_minus_ct_belief] = (data[!, :rps_belief] .- data[!, :ct_belief]);


# %%
data[!, :rps_overconfidence] .= 0.
data[!, :rps_overconfidence] = data[!, :rps_belief] .- data[!, :score];
data[!, :rps_overconfident] .= (data[!, :rps_overconfidence] .> 0);

# %%
# Unify the definition of a country and remove the previous column `:region` that encoded the same data.
data[!, :country] .= "USA";
for (i, region) = enumerate(data[!, :region])
    m = match(r"(?<country>US|State|california|georgia|arizona|america|ca$|la|95823)"i, region)
    if m === nothing
        m2 = match(r"(?<country>india)"i, region)
        if m2 === nothing
            data[i, :country] = titlecase(data[i, :region], strict = true)
        else
            data[i, :country] = "India"
        end
    end
end
select!(data, Not(:region));

# %%
## Hourly Wage
data[!, :wage] = data[!, :participant_payoff_plus_participation_fee] ./ data[!, :time_in_experiment] .* 3600;

# %% [markdown]
"""
## Comments Encoding

We have encoded the comments according to three dummy variables `probability_driving_choice`, `preference_driving_choice`, `doubt_interpretation` and a string of comment `additional_notes`.

- `probability_driving_choices` means that we read in the comment that higher probabilities of winning are what drives the choices of a mechanism over another (even if the belief/understanding and subsequent choice may not reflect that).
- `preference_driving_choice` means that we read in the comment a intrinsic preference for one or the other mechanism.
- `doubt_interpretation` means that we are not sure of the interpretation of the comment, so that our interpretation should be taken with a grain of salt.
In the data, it is originally encoded as `?`, we replace that by a dummy of value 1 when we have a doubt.
- `additional_notes` are additional remarks we have on the comments. In particular, if a preference is expressed for one or the other of RPS / CT, we mention it.
"""

# %%
comments = CSV.read(joinpath("..", "Data", "OriginalData", "Pilot2Comments.csv"), DataFrame, normalizenames = true, stringtype=String,
    select = [:participant_code, :probability_driving_choice, :preference_driving_choice, :doubt_interpretation, :additional_notes]);
comments[!, :doubt_interpretation] = (comments[!, :doubt_interpretation] .== "?")
comments[ismissing.(comments[!, :doubt_interpretation]), :doubt_interpretation] .= false;
comments[!, :probability_driving_choice] .= (comments[!, :probability_driving_choice] .== 1)
comments[!, :preference_driving_choice] .= (comments[!, :preference_driving_choice] .== 1);




# %%
# Join together the data and our comments.
data = innerjoin(data, comments, on = :participant_code);

# %% [markdown]
"""
Take the reasons to transform anything that looks like an absence of comment into an empty comment. 
Remove the previous column `:reasons` that contained the comments.
This filter is very imperfect.
"""

# %%
data[!, :comments] .= ""
for (i, comment) = enumerate(data[!, :reasons])
    m = match(r"^(?<comment>no comment|na|none|nothing)"i, comment)
    if m === nothing
        data[i, :comments] = data[i, :reasons]
    end
end
select!(data, Not(:reasons));

# %%
# Saving the Cleaned Data
data |> CSV.write(joinpath("..", "Data", "Input", "Pilot2CleanedData.csv"), delim = ',')

