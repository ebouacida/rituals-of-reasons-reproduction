# DC-5 Draw of the 23/06 Midday 4 8 1 7 4
# DC-5 Draw of the 08/07 Midday 4 3 6 8 5
# DC-5 Draw of the 01/09 Midday 9 8 4 6 3
# DC-5 Draw of the 16/08 Midday 7 8 8 5 4


const dc5 = BitVector([true, true, false, false, true]) 
const dc5_2 = BitVector([true, false, true, true, false])
const dc5_3 = BitVector([false, true, true, true, false])
const dc5_2023 = BitVector([false, true, true, false, true])
const dc2_2023 = 49
const dc4_2023 = 9666
const dc3_2023 = 328
const elias = [1, 0, 1, 0, 0];
const elias2 = [1, 0, 0, 0, 0];
const elias3 = [0, 0, 1, 0, 0]

const participation_fee = Dict(
    2021 => 0.8,
    2023 => 1.
)
const high_reward = Dict(
    2021 => 1.6,
    2023 => 2.
)
const low_reward = 0.2

const RPS = Dict{String, Int}(
"Rock" => 1,
"Paper" => 2,
"Scissors" => 3
)

const RPS_inv = Dict{Int, String}(
    1 => "Rock",
    2 => "Paper",
    3 => "Scissors"
)

const belief = Dict{Int, String}(
    -1 => "Bottom half",
    1 => "Top half",
    0 => "Neither"
)

const lottery = Dict{Bool, String}(
    true => "Even",
    false => "Odd",
)

const long_short_criteria = Dict{String, String}(
    "Guessing the Paintings" => "painting",
    "Arrival Time" => "arrival",
    "DC-5 Lottery" => "lottery",
    "Rock, Paper, Scissors" => "rps",
)

const long_short_criteria_experiment2 = Dict{String, String}(
    "Guessing the Paintings" => "painting",
    "Time" => "time",
    "DC-5 Lottery" => "lottery",
    "Rock, Paper, Scissors" => "rps",
)

