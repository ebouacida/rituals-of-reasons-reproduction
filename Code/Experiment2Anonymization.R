# Anonymization of the data

library(readr)
library(dplyr)

# read the data
data <- read_csv(
    file.path("Data", "OriginalData", "all_apps_wide_2023-08-15.csv")
    )
demographics <- read_csv(
    file.path("Data", "OriginalData", "prolific_export_64c7dd8a74bb548c50b4e381.csv"), 
    name_repair = make.names
    )

# Match with the demographic data
data <-  inner_join(data, demographics,
    join_by(participant.label == Participant.id)
    )

#' Check that the participants are unique
testit::assert("Participants in the experiment have unique Prolific ID",
    length(unique(data$participant.label)) == nrow(data)
    )

# Remove participant Prolific ID (participant.label) and Mturk IDs (which should be empty here anyway)
cleaned_data <- data %>% 
    select(!c(participant.label, Submission.id, Status)) %>%
    select(!contains("mturk"))

# Check only MTurk/prolific related columns have been removed.
setdiff(names(data), names(cleaned_data)) %>%
    print

# Save the cleaned data.
cleaned_data %>%
    write_csv(file.path("Data", "OriginalData", "ProlificRawData.csv"))