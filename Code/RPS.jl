import CSV
using DataFrames
using Statistics
using Random


#=
1 = Rock
2 = Paper
3 = Scissors
=#




"""
    winner(a1::Int, a2::Int)

Compute the score of action `a1` against action `a2`.
"""
function winner(a1::Int, a2::Int)
    if (a1 == 1) & (a2 == 3)
        return 1
    elseif (a2 == 1) & (a1 == 3)
        return -1
    elseif a1 > a2
        return 1
    elseif a1 == a2
        return 0
    elseif a1 < a2
        return -1
    else
        return DomainError(a1, "$a1 or $a2 do not have value 1, 2 or 3. Cannot compute a winner.")
    end
end

"""
    score(s1::AbstractArray{T}, s2::AbstractArray{T}) where T <: Int

Compute the score of the strategy profile `s1` against the strategy profile `s2`.
"""
function score(s1::AbstractArray{T}, s2::AbstractArray{T}) where T <: Int
    @assert size(s1) == size(s2) "The different strategies do not have the same dimension, and cannot be pitted against each other."
    res = zeros(Int, size(s1))
    for i = eachindex(s1)
        res[i] = winner(s1[i], s2[i])
    end
    return res
end


"""
    probawin(s1::T, M::Vector{T}) where T <: Int

Compute the probability of winning of the strategy `s1` against all strategies in `M`.
"""
function probawin(s1::T, M::Vector{T}) where T <: Int
    l = length(M)
    p = 0
    d = 0
    for s2 = M
        s = winner(s1, s2)
        if s > 0
            p += 1
        elseif s == 0
            d += 1/2
        end
    end
    return p/l, d/l
end


"""
    probawin(s1::AbstractArray{T}, M::Matrix{T}) where T <: Int

Compute the probability of winning of the strategy `s1` against all strategies in `M`.
"""
function probawin(s1::AbstractArray{T}, M::Matrix{T}, breaktie::Bool) where T <: Int
    l = size(M, 2)
    p = 0
    d = 0
    for s2 = eachcol(M)
        s = score(s1, s2)
        if sum(s) > 0
            p += 1
        elseif sum(s) == 0
            if breaktie
                for i = eachindex(s)
                    if s[i] > 0
                        p+= 1
                        break
                    elseif s[i] < 0
                        break
                    elseif i == length(s)
                        d += 1/2
                    end
                end
            else
                d += 1/2
            end
        end
    end
    return p/l, d/l
end

"""
    session(p1::Vector{T}, p2::Vector{T}) where T <: Int

Pit two profiles of one action against each other.
"""
function session(p1::Vector{T}, p2::Vector{T}) where T <: Int
    result = DataFrame(S = [0.], D = [0.])
    for s1 = p1
        res = probawin(s1, p2)
        push!(result, res)
    end
    delete!(result, 1)
    if p1 == p2# Correction if we are using a matrix against itself, to remove the strategy pitted against itself
        l = length(p1)
        result .*= l /(l-1)
        result[!, :D] .-= 1/2/(l-1)
    end
    result[!, :SD] .= result[!, :D] .+ result[!, :S]
    return result
end  

"""
    session(p1::Matrix{T}, p2::Matrix{T}) where T <: Int

Pit two different collections of profiles of 5 actions against each other.
"""
function session(p1::Matrix{T}, p2::Matrix{T}, breaktie::Bool = true) where T <: Int
    result = DataFrame(S = [0.], D = [0.])
    for s1 = eachcol(p1)
        res = probawin(s1, p2, breaktie)
        push!(result, res)
    end
    delete!(result, 1)
    if p1 == p2 # Correction if we are using a matrix against itself, to remove the strategy pitted against itself
        l = size(p1, 2)
        result .*= l /(l-1)
        result.D .-= 1/2/(l-1)
    end
    result[:, :SD] = result[!, :D] + result[!, :S]
    return result
end

"""
    session(M::Matrix{T}) where T <: Int

Compute the probabilities of winning of all the strategy profiles in `M`.
"""
session(M::Matrix{T}, breaktie::Bool=true)  where T <: Int = session(M, M, breaktie)

"""
    fillstrat(df::AbstractDataFrame, n::Int)

Take the data from `df` and transform it into only one Matrix combining all strategies.
"""
function fillstrat(df::AbstractDataFrame, n::Int)
    allstrat = Matrix{Int}(undef, n, Int(size(df, 1)/n*2))
    c = 0
    for df2 in groupby(df, :game_id)
        c += 1
        allstrat[:, c] = df2[!, :player_one_throw]
        c += 1
        allstrat[:, c] = df2[!, :player_two_throw] 
    end
    return allstrat
end

"""
    randomprofile(n::Int, nsim::Int)

Generate a profile of players playing the optimal equilibrium strategy of RPS.
"""
function randomprofile(n::Int, nsim::Int)
    res = zeros(Int, n, nsim)
    actions = collect(1:3)
    for i = 1:nsim
        s = rand(actions, n)
        res[:, i] = s
    end
    return res
end


#####################
### Reading the data

"""
    clean(df::AbstractDataFrame, n::Int)

For a given game, make sure that there is no player not playing and that there are enough round in the game.
"""
function clean(df::AbstractDataFrame, n::Int)
    res = DataFrame()
    for d in groupby(df, :game_round_id)
        push!(res, d[1, :])
    end
    if (size(res, 1) >= n)
        if (sum(res[1:n, :player_one_throw] .== 0) == 0) & (sum(res[1:n, :player_two_throw] .== 0) == 0)        
            return res[1:n, :]
        end
    end
    return DataFrame()
end

"""
    createdata(file::AbstractString, n::Int)

Read and clean the data by removing everyone who did not play at least one round and games that were too short. We keep only the first play in a given round, if there was a draw.
"""
function createdata(file::AbstractString, n::Int)
    rps = CSV.read(file, DataFrame)
    res = DataFrame()
    for df = groupby(rps, :game_id)
        append!(res, clean(df, n))
    end
    return res
end