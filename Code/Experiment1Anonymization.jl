# %% [markdown]
#---
# title: "Experiment 1, Anonymization"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     eval: false
# format: html
# jupyter:
#   kernelspec:
#     display_name: Julia 1.9.2
#     language: julia
#     name: julia-1.9
# ---

# %% [markdown]
"""
Removes the identifiers of the MTurkers. Once the files are removed, this is a definitive step and it is not possible to link back subjects with their MTurk WorkerIDs.

WARNING: The script does not run as the non-anonymized data is not available anymore.
"""
# %%
import CSV
using DataFrames


# %%
rawdata = CSV.read(joinpath("..", "Data", "OriginalData", "all_apps_wide_2021-06-22.csv"), DataFrame, normalizenames=true, stringtype=String);

# %% [markdown]
# We remove then all empty data from the dataframes, by removing all the missing `participant_mturk_worker_id` or `participant_label`.

# %%
rawdata[ismissing.(rawdata[!, :participant_mturk_worker_id]), :participant_mturk_worker_id] = rawdata[ismissing.(rawdata[!, :participant_mturk_worker_id]), :participant_label]
rawdata = rawdata[.!ismissing.(rawdata[!, :participant_mturk_worker_id]), :];

# %% [markdown]
# There is one information we want to keep from the workersID, whether they participated twice or not. To do so, we provide a unique ID for each mturker.

# %%
allturkers = unique(rawdata[!, :participant_mturk_worker_id])
rawdata[!, :unique_id] = zeros(Int, size(rawdata, 1))
for i = eachindex(allturkers)
    rawdata[rawdata[!, :participant_mturk_worker_id] .== allturkers[i], :unique_id] .= i
end
@assert sum(ismissing.(rawdata[!, :unique_id])) == 0 "Some participants did not receive a unique identifier"

# %%
# Removes the columns with references to mturkers ID.
select!(rawdata, Not(r"mturk"))
select!(rawdata, Not(:participant_label));


# %%
rawdata |> CSV.write(joinpath("..", "Data", "OriginalData", "Experiment1_ARawData.csv"))

# %% [markdown]
# Second Experiment

# %%
expe2rawdata = CSV.read(joipath("..", "Data", "OriginalData", "all_apps_wide_2021-07-07.csv"), DataFrame, normalizenames=true);

# %% [markdown]
# We remove then all empty data from the dataframes, by removing all the missing  `participant_label` (in this second experiment, this is where Workers'ID are stored.

# %%
expe2rawdata = expe2rawdata[.!ismissing.(expe2rawdata[!, :participant_label]), :];

# %% [markdown]
# There is one information we want to keep from the workersID, whether they participated twice or not. To do so, we provide a unique ID for each mturker.

# %%
allturkers = unique(expe2rawdata[!, :participant_label])
expe2rawdata[!, :unique_id] = zeros(Int, size(expe2rawdata, 1))
for i = eachindex(allturkers)
    expe2rawdata[expe2rawdata[!, :participant_label] .== allturkers[i], :unique_id] .= i
end
@assert sum(ismissing.(expe2rawdata[!, :unique_id])) == 0 "Some participants did not receive a unique identifier"

# %%
# Removes the columns with references to mturkers ID.
select!(expe2rawdata, Not(r"mturk"))
select!(expe2rawdata, Not(:participant_label));


# %%
expe2rawdata |> CSV.write(joinpath("..", "Data", "OriginalData", "Experiment1_BRawData.csv"))

# %% [markdown]
# Third Experiment

# %%
expe3rawdata = CSV.read(joinpath("..", "Data", "OriginalData"; "all_apps_wide_2021-08-31.csv"), DataFrame, normalizenames=true);

# %% [markdown]
# We remove then all empty data from the dataframes, by removing all the missing  `participant_label` (in this second experiment, this is where Workers'ID are stored.

# %%
expe3rawdata = expe3rawdata[.!ismissing.(expe3rawdata[!, :participant_label]), :];

# %% [markdown]
# There is one information we want to keep from the workersID, whether they participated twice or not. To do so, we provide a unique ID for each mturker.

# %%
allturkers = unique(expe3rawdata[!, :participant_label])
expe3rawdata[!, :unique_id] = zeros(Int, size(expe3rawdata, 1))
for i = eachindex(allturkers)
    expe3rawdata[expe3rawdata[!, :participant_label] .== allturkers[i], :unique_id] .= i
end
@assert sum(ismissing.(expe3rawdata[!, :unique_id])) == 0 "Some participants did not receive a unique identifier"

# %%
# Removes the columns with references to mturkers ID.
select!(expe3rawdata, Not(r"mturk"))
select!(expe3rawdata, Not(:participant_label));


# %%
expe3rawdata |> CSV.write(joinpath("..", "Data", "OriginalData", "Experiment1_CRawData.csv"))



