# %% [markdown]
#---
# title: "Pilot Anonymization"
# author: 
#     - name: Elias Bouacida
#       email: elias.bouacida@univ-paris8.fr
#       orcid: 0000-0001-8656-6678
#       affiliation:
#           - name: University Paris 8
#             city: Saint-Denis
#             country: France
#     - name: Renaud Foucart
#       email: r.foucart@lancaster.ac.uk
#       orcid:  0000-0003-0611-6146
#       affiliation:
#           - name: Lancaster University
#             city: Lancaster
#             country: United Kingdom
# date: last-modified
# date-format: long
# execute:
#     echo: true
#     warning: false
# format: html
# jupyter:
#   kernelspec:
#     display_name: Julia 1.6.7
#     language: julia
#     name: julia-1.6
# ---

# %% [markdown]
# Take the original data output from oTree and remove MTurk identifiers. This file is kept only for transparency, as the original data has been removed and it is impossible to run it again.

# %%
import CSV
using DataFrames


# %%
expe1rawdata = CSV.read(joinpath("..", "Data", "OriginalData", "all_apps_wide_2020-04-30.csv"), normalizenames=true);


# %%
expe2rawdata=CSV.read(joinpath("..", "Data", "OriginalData", "all_apps_wide_2020-05-14.csv"), normalizenames=true);

# %%
# We remove then all empty data from the dataframes, by removing all the missing `participant_mturk_worker_id`.
expe1rawdata = expe1rawdata[.!ismissing.(expe1rawdata[!, :participant_mturk_worker_id]), :]
expe2rawdata = expe2rawdata[.!ismissing.(expe2rawdata[!, :participant_mturk_worker_id]), :];



# %%
# We check that no Mturker has taken the experiment twice (even between the different sessions.
@assert unique(vcat(expe1rawdata[!, :participant_mturk_worker_id], expe2rawdata[!, :participant_mturk_worker_id])) == vcat(expe1rawdata[!, :participant_mturk_worker_id], expe2rawdata[!, :participant_mturk_worker_id]) "Some subjects have done the experiment twice."

# %% [markdown]
"""
We remove all columns that contains a reference to MTurk.
First, we find all of them, in `columns_to_delete`, then we use a Regex to remove them.
We don't need the first step, it is only to show which ones are deleted.
"""

# %%
columns_to_delete = [col for col=names(expe1rawdata) if occursin("mturk", "$col")]


# %%
select!(expe1rawdata, Not(columns_to_delete)) |> CSV.write(joinpath("..", "Data", "OriginalData", "Pilot1RawData.csv"))


# %%
columns_to_delete = [col for col=names(expe2rawdata) if occursin("mturk", "$col")]


# %%
select!(expe2rawdata, Not(columns_to_delete)) |>  CSV.write(joinpath("..", "Data", "OriginalData", "Pilot2RawData.csv"))



