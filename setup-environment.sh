#!/bin/bash

# Exit on error
set -e

# Define directory paths
PROJECT_DIR="$HOME/project"
R_LIBS_USER="$PROJECT_DIR/R/library"
JULIA_DEPOT_PATH="$PROJECT_DIR/julia"
VIRTUAL_ENV="$PROJECT_DIR/venv"

# Create necessary directories
mkdir -p "$R_LIBS_USER"
mkdir -p "$JULIA_DEPOT_PATH"
mkdir -p "$VIRTUAL_ENV"

# Install system dependencies (requires sudo)
echo "Installing system dependencies..."
sudo apt-get update
sudo apt-get install -y --no-install-recommends \
    curl \
    wget \
    libssl-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    libfontconfig1-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libfreetype6-dev \
    libpng-dev \
    libtiff5-dev \
    libjpeg-dev \
    build-essential \
    zlib1g-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libgdbm-dev \
    libbz2-dev \
    liblzma-dev \
    libffi-dev \
    gfortran \
    libpcre2-dev \
    libcairo2-dev \
    libxt-dev \
    libblas-dev \
    liblapack-dev \
    libicu-dev \
    libtcl8.6 \
    libtk8.6 \
    xorg-dev \
    default-jdk \
    texinfo \
    texlive \
    texlive-fonts-extra

# Install R 4.4.1 from source
echo "Installing R 4.4.1..."
wget https://cran.r-project.org/src/base/R-4/R-4.4.1.tar.gz
tar xzf R-4.4.1.tar.gz
cd R-4.4.1
./configure --prefix=/opt/R/4.4.1 \
    --enable-R-shlib \
    --enable-memory-profiling \
    --with-blas \
    --with-lapack \
    --with-readline \
    --with-tcltk
make
sudo make install
cd ..
rm -rf R-4.4.1 R-4.4.1.tar.gz

# Create symbolic links
sudo ln -sf /opt/R/4.4.1/bin/R /usr/local/bin/R
sudo ln -sf /opt/R/4.4.1/bin/Rscript /usr/local/bin/Rscript

# Install Python 3.12.7 from source
echo "Installing Python 3.12.7..."
wget https://www.python.org/ftp/python/3.12.7/Python-3.12.7.tgz
tar xzf Python-3.12.7.tgz
cd Python-3.12.7
./configure --enable-optimizations
sudo make altinstall
cd ..
rm -rf Python-3.12.7 Python-3.12.7.tgz
sudo update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.12 1

# Install Quarto
echo "Installing Quarto..."
QUARTO_VERSION="1.5.57"
wget https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VERSION}/quarto-${QUARTO_VERSION}-linux-amd64.deb
sudo dpkg -i quarto-${QUARTO_VERSION}-linux-amd64.deb
rm quarto-${QUARTO_VERSION}-linux-amd64.deb

# Set up R environment
echo "Setting up R environment..."
# Install renv
R --vanilla -e 'install.packages("renv", repos="https://cloud.r-project.org")'
# Initialize and restore renv
R --vanilla -e 'renv::restore()'

# Install and set up Python virtual environment
echo "Setting up Python environment..."
python3.12 -m venv "$VIRTUAL_ENV"
source "$VIRTUAL_ENV/bin/activate"
pip install --upgrade pip
pip install jupyter jupyterlab pyyaml nbformat

# Install Julia
echo "Installing Julia..."
JULIA_VERSION="1.10.5"
JULIA_ARCHIVE="julia-${JULIA_VERSION}-linux-x86_64.tar.gz"
wget "https://julialang-s3.julialang.org/bin/linux/x64/${JULIA_VERSION%.*}/${JULIA_ARCHIVE}"
tar -xzf "$JULIA_ARCHIVE"
sudo mv julia-${JULIA_VERSION} /opt/julia
sudo ln -sf /opt/julia/bin/julia /usr/local/bin/julia
rm "$JULIA_ARCHIVE"

# Set up Julia environment
echo "Setting up Julia environment..."
julia --version
julia -e 'using Pkg; Pkg.activate("Code"); Pkg.instantiate()'

# Verify installations
echo "Verifying installations..."
echo "R version:"
R --version
echo "Julia version:"
julia --version
echo "Python version:"
python3.12 --version
echo "Jupyter version:"
jupyter --version
echo "Quarto version:"
quarto --version

echo "Setup complete! To activate the environment:"
echo "1. Use 'source $VIRTUAL_ENV/bin/activate' for Python"
echo "2. R and Julia are available system-wide"
echo "3. Your R packages are installed in $R_LIBS_USER"
echo "4. Julia packages are in $JULIA_DEPOT_PATH"