---
author:
- Elias Bouacida
- Renaud Foucart
date: "07/10/2024"
---


# Rituals of Reasons

This repository contains the experimental instructions, the data collected and the code processing for the research paper *Rituals of Reason: A Choice-Based Approach to the Acceptability of Lotteries in Allocation Problems* (previously circulated as *The Acceptability of Lotteries in Allocation Problems: A Choice-Based Approach*).

# Organisation of the Repository

- The folder `Experiments`  contains all the experimental code used to run the different experiments.
- The folder `Data` contains the data obtained from the experiment and the description of the data files.
- The folder `Code` contains the code used to anonymize the data, to produce the pre-registered analysis and the final paper (once the paper will be finished).

## Data

In the `Data/` folder are stored all the dataset we use in this experiment.
The `OriginalData/` subfolder contains all the original data from the experiment, except the data containing MTurk and Prolific identifiers, which are removed to anonymize the data.
The `Output/` subfolder contains all the output used in the paper.
The `Input/` subfolder contains cleaned data that is then used in the analysis, so the data there has seen some aggregation and treatment already.

The file names are normally explicit enough to tell what they are about.
`PilotN` refers to the first or the second pilot experiment.
`ExperimentN[_X|]` refers to the fact that this file is coming from the N^th^ experiment and the X session.
The file `CleanedData.csv` is the aggregated cleaned data used in the analysis after.
The files  `ExperimentN[_X|]RawData.csv` is the anonymized data from the experiment (without link with the WorkerID in Mturk).
The files  `ExperimentN[_X|]Comments.csv` contains our encoded comments.

## Code

In the `Codes/` folder are stored all the code files related to the treatment of the experiment.
We have used mainly three languages to run and exploit the results of our experiment: Python to run the experiment, and Julia and R to process the results.
All the steps to run a computational replication of the study is available in the [README_Replication](Code\README_Replication.md).

## Experiments

In the `Experiments\` folder, there is all the code needed to run the experiments.
All the experiments were run using Python and oTree Chen (2016).
The code has been written using Python 3.9 and oTree 5.10, and has last been tested with Python 3.12 and oTree 5.11.
A detailed description of the Experiment is available in the [README_Experiment](Experiments\README_Experiments.md).
The pilot is an exception: it has been written using oTree 2.5 and Python 3.7. 
There are breaking changes between oTree 2 and 5.

# Licence

In general, the data in this repository is available under the CC BY-NC-SA 4.0 licence, whereas the code is under GPLv3 licence.
More details are given in the [LICENSE](LICENSE).

# References

Chen, Daniel L., Martin Schonger, and Chris Wickens. 2016. "oTree—an Open-Source Platform for Laboratory, Online, and Field Experiments." *Journal of Behavioral and Experimental Finance* 9: 88–97. <https://doi.org/10.1016/j.jbef.2015.12.001> T