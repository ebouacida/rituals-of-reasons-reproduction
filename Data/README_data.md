---
title: "Data Code Book"
author: 
  - name: Elias Bouacida
    email: elias.bouacida@univ-paris8.fr
    orcid: 0000-0001-8656-6678
    affiliation:
      - name: University Paris 8
        city: Saint-Denis
        country: France
  - name: Renaud Foucart
    email: r.foucart@lancaster.ac.uk
    orcid:  0000-0003-0611-6146
    affiliation:
      - name: Lancaster University
        city: Lancaster
        country: United Kingdom
date: last-modified
date-format: long
format: html
---

We provide here the description of all the raw data created in the experiments.

The raw output from the experiment are not shared,  as they contain non-anonymous Prolific or AMT identifiers.
Once anonymized,  files from all the experiments are called `ExperimentN_XTYPEOFDATA.csv`.
`N` is the Experiment number in the paper. 
1 therefore corresponds to the experiments run on AMT, while 2 are the experiments run on Prolific.
X gives the different waves on AMT.
We also have data files called `Feedback_YYYY-MM-DD.csv` and `PagesTImes-YYYY-MM-DD.csv` which contains additional data exported from oTree.

Note:

- For Boolean values, `0`, `false` and `FALSE` are used interchangeably, and `1`, `true` and `TRUE` as well.
- There may be small variations in the variable names (`.` instead of `_` in particular).
We chose to keep oTree's output as raw as possible and not correct for the names before sharing.
- Sometimes empty values are empty, sometimes they are represented by `NA`.
- `n` in the variable names represent the round number.
- An "App" in oTree is a subdivision of the experiment.
An App is a cohesive block.
There was several App's in this experiment.

# Folder `OriginalData`

## `ExperimentN[|_X][RawData|Comments].csv` and `Feedback_YYYY-MM-DD.csv`

### Session Variables

Session variables have the same value for all the subjects in one session.

| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| session_code | Unique identifier for the session | string of 8 characters | There may be several `session_code` per experiment, as it corresponds to the session as launched in oTree, and not necessarily how we paid subjects. | 
| session_label | Name given to the session | empty | Not used in this experiment  |
| session_comment | comment on the session | empty | Not used in this experiment  |
| session_is_demo | Is the session a test one? | Boolean | Not used in this experiment  |
| session_config_criteria | Configuration particularity of the session | empty | Not used in this experiment   |
| session_config_real_world_currency_per_point | Conversion rate between experimental currency unit and money | integer | Value is always 1, there was no point in the experiment. |
| session_config_participation_fee | Value of the participation fee in the experiment (in \$) | integer | 0.8 in AMT (Experiment 1) and 1 in Prolific (Experiment 2) experiments |
| session.lottery_time | Which DC-5 lottery was used in the experiment | Time and Date | Each DC-5 lottery can be uniquely identified by the time and date it happened. |
| session.result_time | When will the results be available to subjects? | Time and Date | Typically a few days after the experiment is run. |

### Participant Variables

Participant variables may differ between different subjects in the experiment but remain constant in all the experiment.
When in the file  starting with  `Feedback`, the variables do not have the participant moniker.*
In the `ProlificRawData.csv`, they do.
It is the same variable.

| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| participant_id_in_session | Unique identifier for the participant in the session | Integer   |  |
| participant_code | Unique participant identifier,  in theory by session,  in practice over all the experiment | Random string of 8 letters and numbers | Used by subjects to see their results on the website |
| participant_is_bot | Is the participant a computer or not? | Boolean |  Not used in this experiment. |
| participant_index_in_pages | Which page number the row refers to in the experiment | integer |   |
| participant_max_page_index | The last page subjects reached in the experiment | integer |  |
| participant_current_app_name | Name of the App the subject is currently in for this row of data | string |   |
| participant_current_page_name | Name of the page the subject is currently in for this row of data | string |  |
| participant_time_started_utc | The date and time the subject entered oTree (which is slightly later than the AMT/Prolific entry time) | Time and data value following ISO conventions |   |
| participant_visited | Has the participant entered otree? | Boolean | Value is 1. |
| participant_payoff | Payment computed in oTree (in \$)| integer | Value is 0.1 or 0 in the AMT experiments, 0 in the Prolific one: the only payoff computed by oTree directly was the ambiguity aversion elicitation. |
| participant.criteria | Which procedure was the non-lottery one | string | Value is Guessing the Paintings, Rock, Paper, Scissors, Arrival Time or Time |
| participant.criteria_first | Was the non-lottery shown first? | Boolean | if false, the lottery was shown first |
| participant.criteria_choices | What choices subjects made in the non-lottery? | string that is a vector of figures | Repeat the value of `Algorithms_1_player_criteria_choiceX` in order. |
| participant.lottery_choices | What choices subjects made in the lottery? | string that is a vector of boolean | Repeat the value of `Algorithms_1_player_lottery_choiceX` in order. |
| participant.criteria_control  | Had subjects control on the non-loterry? | Boolean | Non-control on the non-lottery was only used in Experiment 1 C. The variable only exists for Experiment 1_C and 2, but is not used in the latter case. |
| participant.lottery_control or participant.control | Had subjects control on the lottery? | Boolean | lottery_control replaced control in Experiment 1_C.  |
| participant.mechanism | Which procedure subjects chose to allocate the reward? | string | The string is the name of the procedure |
| participant.arrival_code | Code created for subjects based on their arrival time | figure with 5 digits | Used for the Arrival time procedure, that is not in the paper, and to break ties if needed |
| participant.arrival_time | When did the subject enter the experiment | Time | Used for the  Arrival time procedure, may be redundant with `participant_time_started_utc` |
| participant.best_mechanism |  Did the subject think that they are more likely to win in the lottery or the non-lottery? |  Boolean | Only elicited in round 2 of the Belief app, so mirrors `Beliefs_2_player_best_mechanism `|
| participant.number | Number given to the participant in the Prolific experiment | integer between 0 and 99 | Used to group together subjects, as if they were chosen to implement the procedure, it only applied to a group of 100 participants. |

### "App" variables

Variables that may vary by page or block in the experiment and differ across subjects.
In the following table, APP can take different values (`Algorithms`, `Mechanisms`, `Beliefs`, `Questionnaire`, `Feedback` and so on). `n` can take value 1 or 2.
`n` is the round number if there are more than 1.

| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| APP_n_player_id_in_group | Identifier for the subject in the group for the given APP. | integer | Not used in this experiment  |
| APP_n_player_role | If we had groups, what role would the subject have in the group | empty | Not used in this experiment. |
| APP_n_player_payoff | Payment received in this APP | integer (in \$) | 0 for most App, except when ambiguity aversion is elicited. |
| Algorithms_1_player_criteria_choiceX | Actions in the non lottery. | integer | The actions X are chosen by the subject in the treatments with control and by oTree without control. X goes from 1 to 5, in order. |
| Algorithms_1_player_hours | The "hour" used to create the code in the Time algorithm | integer  |  |
| Algorithms_1_player_minutes | The "minute" used to create the code in the Time algorithm | integer  |  |
|Algorithms_1_player_seconds | The "seconds" used to create the code in the Time algorithm | integer  |  |
| Algorithms_1_player_lottery_choiceX | Actions in the lottery.  | Boolean | The actions X are chosen by the subject in the treatments with control and by oTree without control. X goes from 1 to 5, in order. | 
| APP_n_group_id_in_subsession | Identifier for the group in this APP. | empty |  Not used in this experiment |
| Algorithms_n_subsession_round_number | Round number in the APP. | integer |  |
| Mechanism_n_player_mechanism | Which procedure the subject chose? | Boolean | Each value correspond to the lottery or the non-lottery. |
| Mechanism.n.player.wta | What is the given WTA of the subjet to change his choice? | number | Value between \$0 and \$2. |
| Beliefs_n_player_belief | Did the subject think they will win or loose for the procedure given in round `n` | integer (value of -1 or 1) | -1: loosing, 1: winning |
| Beliefs_n_player_criteria | What procedure was elicited in round `n` | Boolean | Lottery if false, non lottery otherwise. |
| Beliefs_n_player_best_mechanism | Did the subject think that they are more likely to win in the lottery or the non-lottery? |  Boolean | Only elicited in round 2 of the Belief app, so is empty for `n=1`. |
| Questionnaire_1_player_age | Answer to the MCQ "How old are you?" | Values are "<25", "25-40", "40-55", ">55", "Prefer not to say" | Only in Experiment 1. |
| Questionnaire_1_player_gender_rev | What the gender question order reversed or not between Male and Female | Boolean | 1 means Female first, 0 Male first, Only in Experiment 1. |
| Questionnaire_1_player_gender | Answer to the MCQ "What is your gender?" | Values are "Male", "Female", "Prefer not to say", "Other" | Only in Experiment 1. |
| Questionnaire_1_player_employment | Answer to the MCQ "What is your occupation?" | Values are "Employed", "Self-employed", "Student", "Retired", "Other" | Only in Experiment 1. |
| Questionnaire_1_player_region | "What is your country of residence?" | String | Only in Experiment 1. |
|  Questionnaire_1_player_urn_rev | Not used in the experiment. | Boolean |  |
| Questionnaire_1_player_urn | What urn did the subject choose? | Values are "Urn Left" or "Urn Right" | The left one is the risky one, the right one the ambiguous one |
| Questionnaire_1_player_colour_rev | Not used in the experiment. | Boolean | It was supposed to invert Red and Black colour |
| Questionnaire_1_player_colour | Which colour did the subject choose? | "Red" or "Black" | |
| Questionnaire_1_player_colour_drawn | What colour did oTree draw when computing the ambiguity payments? | "Red" or "Black" | |
| Questionnaire_1_player_urn_winner | Did the subject win the in the ambiguity elicitation task? | Boolean | If 1, the subject earned \$0.10 |
| Questionnaire.n.player.probability | Choice between two statements  "I think I have more chances of winning in this mechanism." and "I prefer this mechanism, but for other reasons." | Boolean | First statement is true, second is false. |
| Questionnaire_1_player_reasons | Answer to the question "Why did you choose the CHOSEN PROCEDURE to get your reward?" | String | CHOSEN PROCEDURE was replaced by the procedure they chose in the experiment. |
| Questionnaire.1.player.merit | Likert scale answer to the question "PROCEDURE 1 is more meritocratic than PROCEDURE 2" | Likert scale values (integer) | Only in Experiment 2. |  
| Questionnaire.1.player.fair | Likert scale answer to the question "PROCEDURE 1 is fairer than PROCEDURE 2" | Likert scale values (integer) | Only in Experiment 2. Procedure 1 and 2 varied between participants. |  
| Questionnaire.1.player.easy | Likert scale answer to the question "PROCEDURE 1 is easier to understand than PROCEDURE 2" | Likert scale values (integer) | Only in Experiment 2|  
| Questionnaire.1.player.procedureX | Which procedure is the procedure X | string | Values are Lottery, Time or RPS |
| unique_id | Unique identifier (by session) created during the anonymization proces. | integer | Used in particular to make sure that an AMT participants did not participate more than once. |
| Feedback_n_player_completion_code | Completion code given to subjects to fill in AMT. Used to check double participation. | string | Random sequence of 10 digits and letters. |


### Prolific added variables

As a reminder, the Prolific experiment is Experiment 2.

| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| Started.at | When did the subject start the experiment on Prolific | Date and Time | |
| Completed.at | When did the subject finish the experiment on Prolific (i.e. entered the completion code) | Date and Time | |
| Reviewed.at | When did the experimenter checked the participation in the experiment on Prolific | Date and Time | Not used in this experiment. |
| Archived.at | When was the participation of the subject in the experiment archived on Prolific (i.e. entered the completion code) | Date and Time | Not used in this experiment. |
| Time.taken | How long did it take to complete the experiment | integer (seconds) | May differ from the time completed on oTree, as it starts and finishes on Prolific |
| Completion.code | What completion did subjects entered | string  | It should have been `CG1DN21H`, but sometimes subjects used their uniqueid, which is fine as well. Used for the payments, but not in the analysis. |
| Total.approvals | How many approvals did the subject received before on Prolific | integer | Not used in this experiment. |
| Fluent.languages | What language do subject speak fluently | string | Self-declared to Prolific. Not used in this experiment. |
| Age | How old is the subject | integer | Self-declared to Prolific |
| Sex | What gender? | string | Self-declared to Prolific |
| Ethnicity.simplified | What ethnicity does the  subject decare | string | Self-declared to Prolific |
| Country.of.birth | What country is the subject born in? | string | Self-declared to Prolific |
| Country.of.residence | What country is the subject living in? | string | Self-declared to Prolific and is USA. |
| Nationality  | What is the nationality of the subject? | string | Self-declared to Prolific. Not used in this experiment. |
| Language  |  What is the subject main language ? | string | Self-declared to Prolific. Not used in this experiment. |
| Student.status | Is the subject a student? | string | Self-declared to Prolific. |
| Employment.status |What is the subject's employment status? | string | Self-declared to Prolific. |

## `PageTimes-YYYY-MM-DD.csv` files

These files allow us to compute the time spend on each page.
Variables already introduced earlier are not repeated.



| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| page_index | Page number in the experiment | integer | Starts at 0 |
| app_name | The name of the App | string | Empty for the initial page, when subjets arrives in the experiment |
| page_name | Name given to the page in the experiment | string | The name is given in the oTree code. |
| epoch_time_completed | The time when the page is left | integer | Starts at the conventional epoch time for computer (January 1, 1970, midnight UTC). |
| round_number | Which round is it? | integer | At most 2 |
| timeout_happened | Did the subject go to the next page because of a time limit? | Bool | If 0, then no timeout. In general there should be not timeout |
| is_wait_page | Is the page a waiting page (i.e., a page where the subject is waiting for other subjects or an event) | Bool | 0 for everyone. Not used in this experiment. |


## `ExperimentN_XComments.csv`

In these files, we classified the literal comments provided by subjects as indicating that probability or preferences for a given procedure seem to matter more.
We did the classification ourselves, and it is obviously subject to our own subjective interpretation of the commentary for subjects.
We could not always classify the comments, so some subjects may belong to no or both classes.
The `participant_code` is the same as in the other files.

| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| probability | Does the subject state that probability matters more? | Bool |  |
| preference | Does the subject state that the prefer one procedure over the other, for a specific reason? | Bool |  |
| error | Do we think that stated preferences and behavior coincide? | Bool | True means that we think there is an inconsistency |


## Rock_Paper_Scissors_Raw_data.csv

Data is coming from players playing on \url{https://roshambo.me}. 
We thank Lasse Hassing for giving us access to their data.

| Variables | Description | Kind of values    | Notes | 
|:----------|:-----------|:----------|:----------------|
| game_id | Unique identifier for a game | integer |  |
| game_round_id | Unique identifier for the round in the game | integer | The identifier is unique at the round and game level. Players stay in the same round when they use the same strat. |
| player_one_throw | What did player 1 choose? | integer | 1 means Rock, 2 Paper and 3 Scissors. |  
| player_two_throw | What did player 2 choose? | integer | 1 means Rock, 2 Paper and 3 Scissors. |  


# Folder `Input`

In this folder, the data has been cleaned an pre-treated.
Some variables are repeated from before, others are new.
The treatment has been done by files `ExperimentN[|_X]CleaningData.ipynb`
`Experiment1CleanedData.csv` merges together all the data cleaning from the AMT experiment.
Its counterpart for the Prolific experiment is `Experiment2CleanedData.csv`

## `Experiment[N|N_X]CleanedData.csv`

| Variables | Description | Kind of values    | Notes | 
|:-------|:--------------|:----------|:----------------|
| `[participant_]code`  | Unique participant identifier, in theory by session,  in practice over all the experiment | Random string of 8 letters and numbers | Used by subjects to see their results on the website. Not used in the data analysis. |
| `[participant_]time_started_utc` | The date and time the subject entered oTree (which is slightly later than the AMT/Prolific entry time) | Time and data value following ISO conventions |   |
| criteria_choiceX | What X action did the subject choose for the non-lottery. In the treatment without control, oTree gave the choice to subjects | integer | X goes from 1 to 5, in order. |
| lottery_choiceX | What X action did the subject choose for the lottery. In the treatment without control, oTree gave the choice to subjects | Boolean | X goes from 1 to 5, in order. Value of 0 means odd, 1 means even. | 
| age or Age | How old is the subject | bracket or integer |  Values are "<25", "25-40", "40-55", ">55", "Prefer not to say" in AMT a integer value in Prolific | Self-declared  |
| employment | Answer to the MCQ "What is your occupation?" | Values are "Employed", "Self-employed", "Student", "Retired", "Other" | Not asked in Prolific |
| Employment_status | What is the subject's employment status? | string | Self-declared to Prolific. |
| Student_status | Is the subject a student? | string | Self-declared to Prolific. |
| Ethnicity.simplified | What ethnicity does the  subject decare | string | Self-declared to Prolific |
| colour | Which colour did the subject choose? | "Red" or "Black" | Not asked in Prolific |
| Questionnaire_1_player_colour_drawn | What colour did oTree draw when computing the ambiguity payments? | "Red" or "Black" | Not asked in Prolific |
| urn_winner | Did the subject win the in the ambiguity elicitation? | Boolean | If 1, the subject earned \$0.10, only AMT |
| reasons | Answer to the question "Why did you choose the CHOSEN PROCEDURE to get your reward?" | String | CHOSEN PROCEDURE was replaced by the procedure they chose in the experiment|
| unique_id | Unique identifier (by session) created during the anonymization proces. | integer | Used in particular to make sure that an AMT participants did not participate more than once. |
| time_in_experiment | Total time spent between landing on the Welcome Page and arriving to the last page of the experiment. |   |
| time_page_X | Total time spent on page X of the experiment | integer | The length (in pages) of the AMT and Prolific experiment differed. |
| criteria | Which procedure was the non-lottery one | string | Value is Guessing the Paintings, Rock, Paper, Scissors, Arrival Time or Time |
| criteria_first | Was the non-lottery shown first? | Boolean | if false, the lottery was shown first |
| criteria_choices | What choices subjects made in the non-lottery? | string that is a vector of figures | Repeat the value of `Algorithms_1_player_criteria_choiceX` in order. |
| lottery_choices | What choices subjects made in the lottery? | string that is a vector of boolean | Repeat the value of `Algorithms_1_player_lottery_choiceX` in order. |
| criteria_control | Had subjects control on the non-loterry? | Boolean | Non-control on the non-lottery was only introduced in Experiment 1_C |
| lottery_control or control | Had subjects control on the lottery? | Boolean |  |
| mechanism | Which procedure subjects chose to allocate the reward? | string | The string is the name of the procedure |
| arrival_code | Code created for subjects based on their arrival time | figure with 5 digits | Used for the Arrival time procedure, that is not in the paper, and to break ties if needed |
| arrival_codeX | Xth number in the code created for subjects based on their arrival time | integer | Used for the Arrival time procedure, that is not in the paper, and to break ties if needed. Together forms the `arrival_code` variable. |
| best_mechanism |  Did the subject think that they are more likely to win in the lottery or the non-lottery? |  Boolean | Only elicited in round 2 of the Belief app, so mirrors `Beliefs_2_player_best_mechanism `|
| criteria_belief | Did the subject think they will win or loose in the non-lottery? | Boolean | If true, the subject stated that they would win in that procedure. |
| lottery_belief | Did the subject think they will win or loose in the lottery? | Boolean | If true, the subject stated that they would win in the lottery. |
| female | Is the subject declaring she is a female? | Boolean |   |
| male | Is the subject declaring he is a male? | Boolean |  |
| other | Is the subject declaring nothing regarding their gender? | Boolean |  |
| ambiguity_averse | Did the subject choose the non-ambiguous urn? | Boolean | The measure does not elicit ambiguity neutral subjects, who could choose any urn (they are indifferent). |
| treatment | To which treatment does the subject belong? | string | Give the non-lottery procedure and the control given or not on the both the lottery and non-lottery one. |
| country | Normalized country names from their previous answers | string |  |
| prediction_payed | Which belief elicitation is used for payment? | string | Three possible values: lottery, criteria or best_mechanism |
| best_mechanism_winner | Did subjects correctly guessed their relative ranking between the lottery and non-lottery procedure | Boolean |  |
| payment | How much did subjects earn in the experiment (excluding show-up fee) | number |  |
| lottery_winner | Is the subject winning in the lottery procedure? | Boolean |  |
| lottery_ranks | What is the rank of the subject in the lottery procedure (in their group)? | number | Given between 0 and 1 (in percentage), to normalize because not all groups where exactly made of 100 subjects.  |
| lottery_score | How many odd/even guess were correct? | integer | |
| lottery_belief_winner | Is the subject correctly guessing if they won or not in the lottery? | Boolean | |
| criteria_winner | Is the subject winning in the non-lottery procedure? | Boolean |  |
| criteria_ranks | What is the rank of the subject in their non-lottery procedure (in their group)? | number | Given between 0 and 1 (in percentage), to normalize because not all groups where exactly made of 100 subjects.  |
| criteria_score | Score in the non-lottery procedure. | integer | The way the score is computed depends on the procedure. If there was an objective answer, it is the number of correct guesses (guessing the paintings), the number of wins (RPS) or the number of evens (Time). |
| criteria_belief_winner | Is the subject correctly guessing if they won or not in the lottery? | Boolean | |
| probability | Does the subject state that probability matters more? | Bool |  |
| preference | Does the subject state that the prefer one procedure over the other, for a specific reason? | Bool |  |
| error | Do we think that stated preferences and behavior coincide?Not used in the data analysis. | Bool | True means that we think there is an inconsistency |
| session | To which experiment did subjects belong | integer | values are 1, 2 or 3 |
| number | Number given to the subject in their group | integer between 0 and 99 | Used to determine which participant in the group is used for payment. |
| wta |  What is the given WTA of the subjet to change his choice? | number | Value between \$0 and \$2. |
| bonus | How much subjects received as bonus | integer | Always be 0, it was directly in their payments.  |
| group | In which group of participants the subject is | integer | Values go from 1 to 6 (or NA). All subjects in one group have the same procedure used to compute payments. Only used in Experiment 2. |
| expe | In which treatment were subjects | integer | 1 for RPS, 2 for Time. Only in Experiment 2. | 

